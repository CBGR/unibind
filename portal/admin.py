# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin

from .models import Factor, FactorData, Post


class FactorAdmin(admin.ModelAdmin):
	list_display = ('tf_name', 'folder')
	search_fields = ['tf_name', 'id', 'folder']
	list_filter = ('tf_name',)

admin.site.register(Factor, FactorAdmin)


class NewsAndUpdateAdmin(admin.ModelAdmin):
	list_display = ('title', 'author', 'category', 'date')
	search_fields = ['title', 'author', 'category']
	list_filter = ('category', 'author')

admin.site.register(Post, NewsAndUpdateAdmin)
