# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2018-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Make sure the following:
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desidered behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.

from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import User



# Gene, synonym and profile information permitting synonym searches.

# Gene information used to organise synonyms.

class Gene(models.Model):
    gene_id = models.IntegerField(primary_key=True)
    tax_id = models.IntegerField()
    symbol = models.TextField()

    class Meta:
        db_table = 'genes'

# Synonyms for each gene.

class Synonym(models.Model):
    id = models.IntegerField(primary_key=True)
    synonym = models.TextField()
    gene = models.ForeignKey(Gene, db_column='gene_id', related_name='synonyms', on_delete=models.CASCADE)

    class Meta:
        db_table = 'gene_synonyms'

# JASPAR profiles.

class Profile(models.Model):
    id = models.IntegerField(primary_key=True)
    jaspar_id = models.CharField(max_length=16)
    jaspar_version = models.CharField(max_length=1)
    name = models.CharField(max_length=255)
    genes = models.ManyToManyField(Gene, related_name='profiles', through='ProfileGene')

    class Meta:
        db_table = 'profiles'

# A one-to-many mapping from JASPAR profiles to genes.

class ProfileGene(models.Model):
    profile = models.ForeignKey(Profile, db_column='profile_id', related_name='profile_genes', on_delete=models.CASCADE)
    gene = models.ForeignKey(Gene, db_column='gene_id', related_name='gene_profiles', on_delete=models.CASCADE)

    class Meta:
        db_table = 'profile_genes'



# General taxonomy information.

class Species(models.Model):
    tax_id = models.IntegerField(db_column='tax_id', primary_key=True)
    name = models.CharField(db_column='scientific_name', max_length=250)
    image_name = models.CharField(db_column='image_name', max_length=255, blank=True, null=True)
    image_url = models.CharField(db_column='image_url', max_length=255, blank=True, null=True)

    def __str__(self):

        'Return a string representation, used as a form label.'

        return self.name

    class Meta:
        db_table = 'species'



# Cell type or tissue information.

class Cell(models.Model):
    cell_id = models.IntegerField(primary_key=True)
    title = models.CharField(max_length=255)
    cellosaurus_id = models.CharField(max_length=16, null=True)
    cell_ontology_id = models.CharField(max_length=16, null=True)
    exp_factor_ontology_id = models.CharField(max_length=16, null=True)
    uberon_id = models.CharField(max_length=16, null=True)
    source = models.CharField(max_length=16, null=True)
    source_id = models.CharField(max_length=16, null=True)
    cell_type_id = models.CharField(max_length=16, null=True)
    brenda_id = models.CharField(max_length=16, null=True)
    species = models.ForeignKey(Species, null=True, db_column='tax_id', related_name='cells', on_delete=models.CASCADE)

    def __str__(self):

        'Return a string representation, used as a form label.'

        return self.title

    class Meta:
        db_table = 'cell'



# Genome information.

class Genome(models.Model):
    species = models.OneToOneField(Species, primary_key=True, db_column='tax_id', related_name='genome', on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    ensembl_name = models.CharField(max_length=16)
    ucsc_name = models.CharField(max_length=16)
    ensembl_site = models.CharField(max_length=16)
    ucsc_special = models.CharField(max_length=16)

    class Meta:
        db_table = 'genome'



# Database reference sources.

class RefSource(models.Model):
    source_name = models.CharField(max_length=16, blank=False, primary_key=True)
    url_prefix = models.CharField(max_length=255, blank=False)

    def __str__(self):

        'Return a string representation, used as a form label.'

        return self.source_name

    class Meta:
        db_table = 'ref_db_sources'



# Essential transcription factor information.

# Transcription factors.

class Factor(models.Model):
    tf_name = models.CharField(max_length=255)
    folder = models.CharField(max_length=255, unique=True)
    total_peaks = models.CharField(max_length=16, blank=True)
    species = models.ForeignKey(Species, db_column='tax_id', related_name='factors', on_delete=models.CASCADE)

    def __str__(self):
        return self.tf_name

    class Meta:
        db_table = 'factor'

# Sample details related to a dataset.

class FactorSample(models.Model):
    biological_condition = models.CharField(max_length=255, blank=True, null=True)
    cell = models.ForeignKey(Cell, db_column='cell_id', related_name='samples', on_delete=models.CASCADE, null=True)
    folder = models.ForeignKey(Factor, to_field='folder', db_column='folder', related_name='samples', on_delete=models.CASCADE)

    class Meta:
        db_table = 'factor_sample'

# Prediction model result details.

class FactorData(models.Model):
    model_choices = (
        ('PWM', 'PWM'),
        ('DiMO','DiMO'),
        ('TFFM', 'TFFM'),
        ('DNAshaped', 'DNAshaped'),
        ('NRG', 'NRG')
    )

    peakcaller_choices = (
        ('MACS', 'MACS'),
        ('BCP', 'BCP'),
        ('HOMER', 'HOMER')
        )
    peak_caller = models.CharField(max_length=16, blank=False, choices=peakcaller_choices)
    prediction_model = models.CharField(max_length=16, blank=False, choices=model_choices)
    model_detail = models.CharField(max_length=16, null=True, blank=True)
    distance_threshold = models.CharField(max_length=16)
    score_threshold = models.CharField(max_length=16)
    adj_centrimo_pvalue = models.FloatField()
    total_tfbs = models.CharField(max_length=16, blank=True)

    # Multi-column profile reference.

    jaspar_id = models.CharField(max_length=16, blank=True)
    jaspar_version = models.CharField(max_length=1, blank=True)

    # Dataset collection reference.

    folder = models.ForeignKey(Factor, to_field='folder', db_column='folder', related_name='datasets', on_delete=models.CASCADE)

    # Single-column reference to a profile needed by the ORM.

    profile = models.ForeignKey(Profile, null=True, db_column='profile_id', related_name='factordata', on_delete=models.CASCADE)

    class Meta:
        db_table = 'factor_data'

# References to source data.

class FactorRef(models.Model):
    folder = models.ForeignKey(Factor, to_field='folder', db_column='folder', related_name='references', on_delete=models.CASCADE)
    ref_db = models.ForeignKey(RefSource, to_field='source_name', db_column='ref_db', related_name='references', on_delete=models.CASCADE)
    ref_id = models.CharField(max_length=16, blank=False)

    class Meta:
        db_table = 'factor_ref'



# Collections of factors.

class Collection(models.Model):
    name = models.CharField(max_length=16, primary_key=True)
    factors = models.ManyToManyField(Factor, related_name='factors', through='FactorCollection')
    species = models.ManyToManyField(Species, related_name='species', through='SpeciesCollection')

    class Meta:
        db_table = 'collection'

# Membership of collections for factors.

class FactorCollection(models.Model):
    collection = models.ForeignKey(Collection, to_field='name',db_column='collection', related_name='collection_factors', on_delete=models.CASCADE)
    folder = models.ForeignKey(Factor, to_field='folder', db_column='folder', related_name='factor_collections', on_delete=models.CASCADE)

    class Meta:
        db_table = 'factor_collection'

# Membership of collections for species, derived from factor membership.

class SpeciesCollection(models.Model):
    collection = models.ForeignKey(Collection, to_field='name',db_column='collection', related_name='collection_species', on_delete=models.CASCADE)
    species = models.ForeignKey(Species, db_column='tax_id', related_name='species_collections', on_delete=models.CASCADE)

    class Meta:
        db_table = 'species_collection'



# Download details.

class Download(models.Model):
    scope_choices = (
        ('model', 'Prediction model'),
        ('species', 'Species'),
    )
    presence_choices = (
        ('mandatory', 'Mandatory'),
        ('optional', 'Optional'),
    )
    basename = models.CharField(max_length=32)
    label = models.CharField(max_length=64)
    suffix = models.CharField(max_length=16)
    scope = models.CharField(max_length=16, choices=scope_choices)
    presence = models.CharField(max_length=16, choices=presence_choices)

    class Meta:
        db_table = 'downloads'

# Example details.

class Example(models.Model):
    label = models.CharField(max_length=64)
    url = models.CharField(max_length=256)
    example_type = models.CharField(max_length=16)

    class Meta:
        db_table = 'examples'

# General information.

class Info(models.Model):
    key = models.CharField(max_length=64)
    value = models.CharField(max_length=64)

    class Meta:
        db_table = 'info'



# Enrichment analysis jobs.

class EnrichmentJob(models.Model):
    status_choices = (
        ('Queued', 'Queued'),
        ('Running', 'Running'),
        ('Completed', 'Completed'),
        ('Stopped', 'Stopped'),
        ('Other', 'Other'),
    )
    enrichment_modules = (
        ('oneSetBg', 'Enrichment with a background'),
        ('twoSets', 'Differential enrichment'),
        ('oneSetNoBg', 'Enrichment when no background is provided'),
    )
    name = models.CharField(max_length=250)
    slug = models.SlugField(unique=True)
    analysis_type = models.CharField(max_length=250, choices=enrichment_modules)
    species = models.ForeignKey(Species, db_column='tax_id', related_name='jobs', on_delete=models.CASCADE)
    parameter = models.TextField()
    job_status = models.CharField(max_length=150, choices=status_choices)
    job_url = models.CharField(max_length=200, blank=True)
    email = models.CharField(max_length=200, blank=True)
    ip_address = models.CharField(max_length=250, blank=True)
    browser_info = models.CharField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'enrichment_jobs'

# TFBS extraction jobs.

class TFBSextractionJob(models.Model):
    status_choices = (
        ('Queued', 'Queued'),
        ('Running', 'Running'),
        ('Completed', 'Completed'),
        ('Stopped', 'Stopped'),
        ('Other', 'Other'),
    )
    name = models.CharField(max_length=250)
    slug = models.SlugField(unique=True)
    parameter = models.TextField()
    job_status = models.CharField(max_length=150, choices=status_choices)
    job_url = models.CharField(max_length=200, blank=True)
    email = models.CharField(max_length=200, blank=True)
    ip_address = models.CharField(max_length=250, blank=True)
    browser_info = models.CharField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'TFBS_extraction_jobs'

# Article posts.

class Post(models.Model):
    category_choices = (
        ('Update', 'Update'),
        ('Bug fix', 'Bug fix'),
        ('Announcement', 'Announcement'),
        ('Other', 'Other'),
    )
    title = models.CharField(max_length=100)
    content = models.TextField(blank=True)
    slug = models.SlugField(unique=True)
    category = models.CharField(max_length=150, choices=category_choices)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
