# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf.urls import url
from django.conf.urls import handler404, handler500
from . import views
from unibind import settings
from django.views.static import serve


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^search/?$', views.search, name='search'),
    url(r'^docs/$', views.documentation, name='documentation'),
    url(r'^contact-us/?$', views.contact_us, name='contact_us'),
    url(r'^about/$', views.about, name='about'),
    url(r'^faq/$', views.faq, name='faq'),
    url(r'^changelog/$', views.changelog, name='changelog'),
    url(r'^genome-tracks/$', views.genome_tracks, name='genome_tracks'),

    url(r'^api/$', views.api_documentation, name='api_documentation'),

    url(r'^factor/(?P<factor_id>[\w.\-]+)/$', views.factor_detail, name='factor_detail'),

    url(r'^enrichment/$', views.enrichment, name='enrichment'),
    url(r'^enrichment/(?P<enrichment_id>[\w.\-]+)/$', views.enrichment_results, name='enrichment_results'),

    url(r'^TFBS_extraction/$', views.TFBS_extraction, name='TFBS_extraction'),
    url(r'^TFBS_extraction/(?P<extraction_id>[\w.\-]+)/$', views.TFBS_extraction_results, name='TFBS_extraction_results'),

    url(r'^downloads/$', views.download_data, name='download_data'),

    #enable this url to create zip/txt files for downloads page
    #url(r'^downloads-internal/$', views.internal_download_data, name='internal_download_data'),

    url(r'^temp/(?P<path>.*)$', serve, {'document_root': settings.TEMP_DIR}),
    url(r'^download/(?P<path>.*)$', serve, {'document_root': settings.DOWNLOAD_DIR, 'show_indexes': True,}),

]

handler404 = 'portal.views.page_not_found'
handler500 = 'portal.views.server_error'
