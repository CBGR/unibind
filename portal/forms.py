# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django import forms
from django.http import QueryDict
from itertools import chain
from .models import Cell, Collection, Factor, FactorCollection, FactorData, \
                    FactorRef, FactorSample, RefSource, Species
from unibind.settings import SEARCH_MAX_CELL_LINES



# Field definitions.

class CollectionSelector:
    '''
    A class producing a suitable list of collection names.
    '''

    def __init__(self, allow_all=True):
        self.allow_all = allow_all

    def choices(self):
        label = 'All collections'

        # Prepend special value.

        return chain(self.allow_all and [('', label)] or [],
                     Collection.objects.all()
                     .values_list('name', 'name').order_by('name'))

class CellSelector:
    '''
    A class configured using field values to produce a suitable list of
    cell line/type names.
    '''

    def __init__(self, data=None, factors=None, species=None, cell_lines=None,
                 title_search=None, allow_all=True):
        '''
        Initialise the selection of cell lines/types using the given form data,
        restricted by any factor queryset, species identifier, cell line names,
        title search.

        There is also an option to allow all cell lines/types (suitable for
        general searches).
        '''

        data = data or QueryDict()
        self.factors = factors
        self.tax_id = data.get('species') or species
        self.title_search = title_search

        # External restriction on available values.

        self.cell_lines = cell_lines

        # Selected cell line/type, for showing regardless of eligibility.

        self.cell_id = data.getlist('cell_line_id')

        # Permit the selection of all possible values.

        self.allow_all = allow_all

    def choices(self):
        '''
        Return cell key and value details for the cell line/type choice field,
        inserting a default unselected value at the start of the list.
        '''

        label = 'Select a species'
        all_cells = Cell.objects.all()
        num_all_factors = Factor.objects.count()
        selected_factors = self.factors and self.factors.count() != num_all_factors and self.factors or None

        # Restrict by provided cell lines and species.

        if self.cell_lines is not None:
            cells = Cell.objects.filter(title__in=self.cell_lines)

            if self.tax_id:
                cells = cells.filter(species=self.tax_id)

        # Restrict by selected species or selected factors.

        elif self.tax_id or selected_factors:
            label = 'All cell lines'

            # Restrict by factors from query.

            if selected_factors:
                factors = selected_factors

            # Restrict by species.

            else:
                factors = Factor.objects.filter(species=self.tax_id)

            cells = all_cells.filter(cell_id__in=
                                     FactorSample.objects.filter(folder__in=factors)
                                     .values('cell'))
        else:
            cells = Cell.objects.none()

        # Filter according to any search criteria.

        if self.title_search:
            cells = cells.filter(title__icontains=self.title_search)

        # Show any selected cell type (even if it is inappropriate).

        if self.cell_id:
            label = 'All cell lines'
            cells |= all_cells.filter(cell_id__in=self.cell_id)

        # Include any "all cell lines" option if requested.

        return chain(self.allow_all and [('', label)] or [],
                     cells.values_list('cell_id', 'title')
                          .distinct()
                          .order_by('title'))

class SourceSelector:
    '''
    A class configured using field values to produce a suitable list of data
    source names.
    '''

    def __init__(self, data=None, factors=None):
        data = data or QueryDict()
        self.factors = factors

    def choices(self):
        '''
        Return source key and value details for the data source choice field,
        inserting a default unselected value at the start of the list.
        '''

        label = 'All sources'

        if self.factors:
            factors = self.factors
        else:
            factors = Factor.objects.all()

        sources = RefSource.objects.filter(source_name__in=
                                           FactorRef.objects.filter(folder__in=factors)
                                           .values('ref_db')) \
                                   .values_list('source_name', 'source_name') \
                                   .distinct() \
                                   .order_by('source_name')

        return chain([('', label)], sources)

class SpeciesSelector:
    '''
    A class configured using field values to produce a suitable list of species
    names.
    '''

    def __init__(self, data=None, factors=None, allow_all=True):
        data = data or QueryDict()
        self.factors = factors
        self.allow_all = allow_all
        self.collection = data.get('collection')
        self.tax_id = data.get('species')

    def choices(self):
        '''
        Return species key and value details for the species choice field,
        inserting a default unselected value at the start of the list.
        '''

        label = 'All species'

        species = self.values()

        # Show any selected species (even if it is inappropriate).

        if self.tax_id:
            species |= Species.objects.filter(tax_id=self.tax_id) \
                                      .values_list('tax_id', 'name')

        species = species.distinct().order_by('name')

        return chain(self.allow_all and [('', label)] or [],
                     species)

    def values(self):
        '''
        Return all non-null species values selectable by the species choice
        field.
        '''

        # Filter by indicated factors.

        if self.factors:
            factors = self.factors

        # Filter by collection.

        elif self.collection:
            factors = Factor.objects.filter(folder__in=
                          FactorCollection.objects.filter(collection__in=
                              Collection.objects.filter(name=self.collection)).values('folder'))
        else:
            factors = Factor.objects.all()

        return Species.objects.filter(tax_id__in=factors.values("species")) \
                                 .values_list('tax_id', 'name')

class TFNameSelector:
    '''
    A class configured using field values to produce a suitable list of TF
    names.
    '''

    def __init__(self, data=None, factors=None):
        data = data or QueryDict()
        self.factors = factors
        self.tax_id = data.get('species')
        self.cell_id = data.getlist('cell_line_id')
        self.tf_name = data.get('tf_name')

    def choices(self):
        '''
        Return TF name key and value details for the TF name choice field,
        inserting a default unselected value at the start of the list.
        '''

        label = 'Select a species'
        factors = Factor.objects
        num_all_factors = factors.count()
        selected_factors = self.factors and self.factors.count() != num_all_factors and self.factors or None

        # Restrict by selected species or selected factor species.

        if self.tax_id or selected_factors:
            label = 'All TFs'

            # Restrict by factors from query.

            if selected_factors:
                tf_names = selected_factors
            else:
                tf_names = factors.all()

            # Restrict by indicated species.

            if self.tax_id:
                tf_names &= factors.filter(species=self.tax_id)

            # Restrict by any selected cell types via the samples.

            if self.cell_id and len(self.cell_id) <= SEARCH_MAX_CELL_LINES:
                tf_names &= factors.filter(samples__in=
                        FactorSample.objects.filter(cell__in=self.cell_id))
        else:
            tf_names = factors.none()

        # Show any selected name (even if it is inappropriate).

        if self.tf_name:
            label = 'All TFs'
            tf_names |= factors.filter(tf_name=self.tf_name)

        return chain([('', label)],
                tf_names.values_list('tf_name', 'tf_name')
                .distinct().order_by('tf_name'))



# Form definitions.

class ContactForm(forms.Form):
    '''
    Form for contact us page
    '''

    #from_name = forms.CharField(label='Your name', required=True, max_length=100)
    from_email = forms.EmailField(required=True, label='Your email')
    subject = forms.CharField(label='Subject', required=False, max_length=100)
    message = forms.CharField(label='Your message/feedback', required=True, widget=forms.Textarea)

class EnrichmentForm(forms.Form):
    '''
    Form for enrichment page
    '''

    ANALYSIS_TYPES = [
        ('oneSetBg', 'Enrichment with a background'),
        ('twoSets', 'Differential enrichment'),
#        ('oneSetNoBg', 'Enrichment when no background is provided'),
        ]

    analysis_type = forms.ChoiceField(
                choices=ANALYSIS_TYPES,
                label='Select analysis type',
                widget=forms.RadioSelect)

    name = forms.CharField(
            required=False,
            max_length=25,
            label='Analysis title or name')

    email = forms.EmailField(
            required=False,
            label='E-mail address for notification')

    bed_file_1 = forms.FileField(
            label='BED file 1')

    bed_file_2 = forms.FileField(
            required=False,
            label='BED file 2',
            help_text='Required for differential enrichment.')

    bed_file_background = forms.FileField(
            required=False,
            label='BED file background',
            help_text='Required for enrichment with a background.')

    # All distinct species from the factor table.
    # The species may be influenced by the form.

    species = forms.ChoiceField(
            required=False,
            choices=SpeciesSelector(allow_all=False).choices,
            label='Species:')

    # Collection membership.
    # NOTE: This uses a specific collection as the initial choice. If this
    # NOTE: collection is not defined, no results will be shown.

    collection = forms.ChoiceField(
            required=False,
            choices=CollectionSelector(allow_all=False).choices,
            initial='Robust',
            label='Collection:')

    # Parameterisation.

    def update_requirements(self):
        '''
        Change the required state of fields based on the selected
        analysis type.
        '''

        analysis_type = self.data.get('analysis_type')
        background = analysis_type and analysis_type == 'oneSetBg'
        two_sets = analysis_type == 'twoSets'

        self.fields['bed_file_background'].required = background and True or False
        self.fields['bed_file_2'].required = two_sets and True or False

        # Produce the appropriate species values.

        self.fields['species'].choices = SpeciesSelector(self.data, allow_all=False).choices

class EnrichmentFilterForm(forms.Form):
    '''
    Enrichment results refinement form.
    '''

    title_search = forms.CharField(
            required=False,
            label='Search cell lines:')

    # Here, the cell types will be taken from the analysis results, with the
    # species being used to disambiguate cell types where the title information
    # occurs for multiple cell types belonging to different species.

    # Although the cell type is required for filtering, it is indicated as not
    # required to avoid irritating warnings in the Web page.

    cell_line_id = forms.MultipleChoiceField(
            required=False,
            choices=CellSelector().choices,
            label='Cell line/tissue:',
            widget=forms.CheckboxSelectMultiple)

    # Parameterisation.

    def update_inputs(self):
        '''
        Handle controls that act as inputs to the search activity.
        '''

        # Where the search term is to be discarded, clear the search field.

        if self.data.get('revert'):
            self.data['title_search'] = ''

        # Clear selection.

        if self.data.get('clear_all'):
            self.data.setlist('cell_line_id', [])

    def update_fields(self, species=None, cell_lines=None):
        '''
        Update controls from the search results.
        '''

        title_search = self.data.get('title_search')

        # Produce the appropriate cell line values.

        self.fields['cell_line_id'].choices = CellSelector(self.data,
                                                        species=species,
                                                        cell_lines=cell_lines,
                                                        title_search=title_search,
                                                        allow_all=False).choices

        # Select all shown values.

        if self.data.get('select_all'):
            self.select_all()

    def select_all(self):
        self.data.setlist('cell_line_id', [t[0] for t in self.fields['cell_line_id'].choices])

class TFBSextractionForm(forms.Form):
    '''
    Form for TFBS extraction page
    '''

    name = forms.CharField(
            required=False,
            max_length=25,
            label='Analysis title or name')

    email = forms.EmailField(
            required=False,
            label='E-mail address for notification')

    bed_file = forms.FileField(
            label='BED file (mandatory):', required=True)

    TFs_file = forms.FileField(
            label='TF list (optional):', required=False)

    experiments_file = forms.FileField(
            label='Experiment IDs list (optional):', required=False)

    # All distinct species from the factor table.
    # The species may be influenced by the form.

    species = forms.ChoiceField(
            required=False,
            choices=SpeciesSelector(allow_all=False).choices,
            label='Species:')

    # Collection membership.
    # NOTE: This uses a specific collection as the initial choice. If this
    # NOTE: collection is not defined, no results will be shown.

    collection = forms.ChoiceField(
            required=False,
            choices=CollectionSelector(allow_all=False).choices,
            initial='Robust',
            label='Collection:')

    # Parameterisation.

    # def update_requirements(self):
    #     '''
    #     Change the required state of fields based on the selected
    #     analysis type.
    #     '''
    #
    #     analysis_type = self.data.get('analysis_type')
    #     background = analysis_type and analysis_type == 'oneSetBg'
    #     two_sets = analysis_type == 'twoSets'
    #
    #     self.fields['bed_file_background'].required = background and True or False
    #     self.fields['bed_file_2'].required = two_sets and True or False
    #
    #     # Produce the appropriate species values.
    #
    #     self.fields['species'].choices = SpeciesSelector(self.data, allow_all=False).choices


class SearchForm(EnrichmentFilterForm):
    '''
    Form for advanced search options.
    '''

    # All distinct tf_name values from the factor table. Since distinct
    # names are selected and not model instances, a regular choice field is
    # employed.

    tf_name = forms.ChoiceField(
            required=False,
            choices=TFNameSelector().choices,
            label='TF name:')

    # All distinct species from the factor table.
    # The species may be influenced by the query.

    species = forms.ChoiceField(
            required=False,
            choices=SpeciesSelector().choices,
            label='Species:')

    # All distinct data sources from the factor references table.
    # The sources may be influenced by the query.

    source = forms.ChoiceField(
            required=False,
            choices=SourceSelector().choices,
            label='Data source:')

    # Centrality indicator.

    has_pvalue = forms.BooleanField(
            required=False,
            label='Has log(p-value) below defined centrality threshold')

    # Centrality threshold.

    threshold_pvalue = forms.DecimalField(
            required=False,
            max_value=0,
            decimal_places=2,
            initial=0,
            label='Centrality threshold:')

    # Collection membership.
    # NOTE: This uses a specific collection as the initial choice. If this
    # NOTE: collection is not defined, no results will be shown.

    collection = forms.ChoiceField(
            required=False,
            choices=CollectionSelector(allow_all=True).choices,
            initial='Robust',
            label='Collection:')

    # Query string.

    q = forms.CharField(required=False)

    # Page details.

    page = forms.IntegerField()
    page_size = forms.IntegerField()

    # Parameterisation.

    def update_inputs(self):
        '''
        Handle controls that act as inputs to the search activity.
        '''

        EnrichmentFilterForm.update_inputs(self)

        # Make sure that all cell lines are selected for the search, if this is
        # indicated by the form.

        if self.data.get('select_all'):
            self.update_cell_lines()

    def update_fields(self, queryset=None):
        '''
        Restrict various fields using selected field values. If the queryset is
        indicated, use the factors from the queryset to influence other fields.
        '''

        # Produce the appropriate data source values.

        self.fields['source'].choices = SourceSelector(self.data, queryset).choices

        # Produce the appropriate species values.

        self.fields['species'].choices = SpeciesSelector(self.data, queryset).choices

        # Produce the appropriate TF name values.
        # Without a selected species, retain any indicated TF name.

        self.fields['tf_name'].choices = TFNameSelector(self.data, queryset).choices

        # Provide the centrality threshold if absent.

        if not self.data.get('threshold_pvalue'):
            self.data['threshold_pvalue'] = 0

        self.update_cell_lines()

    def update_cell_lines(self):
        '''
        Produce the appropriate cell line values for the selector, selecting all
        items if indicated.
        '''

        species = SpeciesSelector(self.data).values()
        species = len(species) == 1 and species[0] or None

        EnrichmentFilterForm.update_fields(self, species, None)

# vim: tabstop=4 expandtab shiftwidth=4
