{% extends 'portal/base.html' %}

{% load static %}

{% block title %}
    Documentation
{% endblock %}

{% block content_header %}
 UniBind documentation
{% endblock %}

{% block breadcrumb %}
  <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
  <li class="active">Docs</li>
{% endblock %}

{% block content %}

<div class="row">
  <div class="col-md-8 col-lg-8 col-xs-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fas fa-book"></i> UniBind Documentation</h3>
        <p class="pull-right"><em>Last updated: July 13, 2020</em></p>
      </div>
      <!-- /.box-header -->
      <div class="box-body">

        <h4 id="UniBind"><i class="fas fa-question-circle"></i> What is UniBind?</h4>

        {% include "portal/summary.html" %}

        <h4 id="dataprocess"><i class="fas fa-database"></i> How was the data processed?</h4>

        <p>The entire collection of ChIP-seq data sets was processed by

        <a href="https://academic.oup.com/nar/article/46/D1/D267/4602873"
           target="_blank">ReMap</a>

        and

        <a href="http://gtrd.biouml.org/" target="_blank">GTRD</a>

        up to ChIP-seq peak calling and is available at its respective
        databases. The peaks were used as input for the ChIP-eat data
        processing pipeline. The complete pipeline is designed to uniformly
        process ChIP-seq data sets, from raw reads to the identification of
        direct TF-DNA binding events, and it was implemented in the ChIP-eat
        software with source code freely available at our

        <a href="https://bitbucket.org/CBGR/chip-eat/"
           target="_blank">BitBucket repository</a>.

        Specifically, ChIP-eat allows for: (i) aligning and filtering raw
        ChIP-seq data, (ii) calling ChIP-seq peaks, (iii) training the TFBS
        computational model and (iv) automatically defining the enrichment
        zone in the landscape

        <a href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4082612/"
           target="_blank">plots</a>

        to predict TFBSs. Only the ChIP-seq datasets for which a TF binding
        profile for the targeted TF was available in

        <a href="http://jaspar.genereg.net/" target="_blank">JASPAR</a>

        were used for TFBS predictions. For each peak, we computed the best
        subsequence as the one minimizing the distance to its peak summit and
        maximizing its score. With these subsequences, the enrichment zone
        containing high confidence direct TF-DNA interactions was
        automatically defined for each data set using an entropy-based
        algorithm. Next, we rescanned all peaks falling outside the enrichment
        zone to look for any subsequence different from the best that would
        still allow the peak to be within the enrichment zone.</p>

        <img class="img-responsive img-rounded"
             src="{% static 'img/chipeat.png' %}" width="100%">

        <p>Datasets in UniBind are separated into robust and permissive
        collections based on two quality control metrics. First, we filtered
        out datasets where the DAMO-optimized TF binding motif was not similar
        to the expected canonical motif. Second, we filtered out datasets
        where TFBSs are not enriched around their peak summits. Datasets
        satisfying both criteria were classified as part as the robust
        collection, while the rest were classified as part of the permissive
        collection.</p>

        <h4 id="UniBindhost"><i class="fas fa-server"></i> What data does UniBind host?</h4>

        <p>The UniBind database contains millions of transcription factor (TF)
        binding site (TFBS) predictions across the genome of nine different
        species. These predictions were derived from the uniform processing of
        9625 publicly available ChIP-seq datasets accounting for 837 distinct
        TFs in 1329 cell lines. For each ChIP-seq data set, the user can
        download the following: the set of predicted direct TF-DNA
        interactions in

        <a href="https://genome.ucsc.edu/FAQ/FAQformat.html">BED6</a>

        format, and in

        <a href="https://en.wikipedia.org/wiki/FASTA_format">FASTA</a>

        format, a visual representation of the delineated enrichment zone, and
        the trained prediction model used.</p>

        <h4 id="UniBindentry"><i class="fas fa-file"></i> What does each entry in UniBind contain?</h4>

        <p>When searching/clicking on one TF name or one ChIP-seq data set of
        interest, a page will be displayed with all the information available.
        A summary part is at the top of the page describing the metadata
        available for that dataset. The user is also provided with external
        links pointing for more details about this entry’s components.</p>

        <p>On the middle section of the page, all the JASPAR matrices used for
        this database entry are displayed in a tabbed layout. Download buttons
        are also available in this section from where the user can obtain the
        set of TF binding site predictions. At the bottom of this section, a
        plot consisting of four panels is displayed to give a visual
        representation of the data for the selected JASPAR matrix. Please see
        next section in the documentation on how to interpret these plots.</p>

        <p>Below this section, a summary of the statistics for all the
        prediction models used for this data set is displayed. Namely, the
        thresholds that define the set of direct TF-DNA interactions are
        displayed, as well as a

        <a href="http://meme-suite.org/doc/centrimo.html"
           target="_blank">CentriMo</a>

        p-value providing information about the centrality of the predictions
        with respect to the ChIP-seq peak summits. The lower the p-value the
        stronger the centrality. The closer it is to 0 the lower the quality
        of the data and/or performance of the prediction model is on this
        particular data set.</p>

        <p>Finally, at the bottom of the entry page, suggestions with other
        ChIP-seq data sets targeting the same TF are available.</p>

        <h4 id="download"><i class="fas fa-download"></i> How can I download the data?</h4>

        <p>The user can download the set of transcription factor binding sites
        (TFBSs) and the trained prediction model for each of the datasets
        individually by navigating to the UniBind entry of interest. If the
        user wants to download the available data in bulk, this can be done
        through the

        <a href="/downloads">Download</a>

        section of the UniBind database. For the TFBS bulk download, BED and
        FASTA files are available. In case of TFs with variants (e.g., TFAP2C,
        JUND), individual files are available for each variant. This section
        also allows the user to download the cis-regulatory modules derived
        using the entire set of TFBS predictions.</p>

        <h4 id="centrality"><i class="fas fa-align-center"></i> How to interpret the enrichment zone and centrality p-value?</h4>

        <img class="img-responsive img-rounded"
             src="{% static 'img/landscape_plot.png' %}" width="100%">

        <p>For each entry in UniBind, a visual representation of the results
        is available. It consists of a plot composed of four panels.  Each
        point in panel (A) represents the top scoring sequence in one ChIP-seq
        peak, that is the sequence within the ChIP-seq peak with the best
        score computed from the TFBS computation model used. On the Y-axis is
        the motif score, and on the X-axis is the distance to the ChIP-seq
        peak summit. The dashed lines represent the thresholds on the TFBS
        computational model scores and on the distance to the peak summits,
        delimiting the area that contains direct TF-DNA interactions.  Panel
        (B) is a heat map of the data from panel (A) to help in visualizing
        the density of the data and better understand the defined thresholds.
        The bottom panels show where the threshold on the motif score (C) and
        on the distance from the peak summit (D) was automatically defined by
        the entropy-based algorithm. In the legend of panel (D) the centrality
        p-value is provided. The closer it is to 0 the lower the quality of
        the data and/or performance of the prediction model is on this
        particular data set.</p>

        <h4 id="search"><i class="fas fa-search"></i> How to use advanced search?</h4>

        <p>The user can perform an advanced search of the data available in
        UniBind. Entering the Search page by clicking “Search” on the top of
        the page, an “Advanced Options” link is available under the search
        bar. Here, the user can select the TF of interest, the cell-line of
        interest, as well as the preferred species, data source, and
        collection. It is also possible to filter the results by centrality
        score (log10(p-value)) of the enrichment zone by checking the option
        P-value. This will display a text field where users can introduce a
        desired threshold to filter the search results, where only datasets
        with a centrality score lower than the introduced threshold will be
        shown. Once the desired options are selected, the user can click the
        search button, and come back to modify those options at any time
        during the browsing. The diagram below illustrates the use of advanced
        search:</p>

        <img class="img-responsive img-rounded"
             src="{% static 'img/advanced_search.png' %}" width="100%">

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>

  <div class="col-md-4 col-lg-4 col-xs-12">
    <div class="box box-success">
      <div class="box-header with-border">
        <h3 class="box-title"><i class="fas fa-list"></i> Table of contents</h3>

        </div>
      <!-- /.box-header -->
      <div class="box-body">
      <div id="TOC">
        <ul>

        <li><a href="#UniBind">What is UniBind?</a></li>
        <li><a href="#dataprocess">How was the data processed?</a></li>
        <li><a href="#UniBindhost">What data does UniBind host?</a></li>
        <li><a href="#UniBindentry">What does each entry in UniBind contain?</a></li>
        <li><a href="#download">How can I download the data?</a></li>
        <li><a href="#centrality">How to interpret the enrichment zone and centrality p-value?</a></li>
        <li><a href="#search">How to use advanced search?</a></li>
        <li><a href="/contact-us">Contact</a></li>
        </ul>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>

{% block javascript %}

<!-- add bootstrap table styles to pandoc tables -->
<script>
$(document).ready(function () {
  $('tr.header').parent('thead').parent('table').addClass('table table-condensed');
});
</script>

<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
  (function () {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src  = "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML";
    document.getElementsByTagName("head")[0].appendChild(script);
  })();
</script>

<script>
$(document).ready(function () {
  window.buildTabsets("TOC");
});
</script>

{% endblock %}

{% endblock %}
