# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import defaultdict
from itertools import groupby
from os.path import exists, isdir, join, split
from urllib.parse import urlencode
import datetime, os, re, shlex, tempfile, time

from shutil import rmtree

from django.core.files.storage import FileSystemStorage
from django.core.mail import BadHeaderError, EmailMessage
from django.core.paginator import Page, Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, Http404
from django.views.decorators.cache import cache_page
from django.views.decorators.clickjacking import xframe_options_exempt

# Application settings.

from unibind.settings import BASE_DIR, ENRICHMENT_DIR, TFBS_EXTRACTION_DIR, \
                            MEDIA_ROOT, \
                             LATEST_DATA_VERSION, \
                             SEARCH_MAX_CELL_LINES, \
                             SEND_TO_EMAIL, \
                             TEMP_DIR, TEMP_LIFE, \
                             MAX_PAGINATION_LIMIT, PAGINATION_DEFAULT

# Utilities.

from unibind.notify import send_email_notification
from unibind.search import get_search_queryset
from unibind.utils import Archiver, get_data_source

# Portal imports.

from .forms import ContactForm, SearchForm, TFBSextractionForm, EnrichmentForm, EnrichmentFilterForm
from .models import Cell, Collection, Factor, FactorData, FactorRef, \
                    FactorSample, Species, Genome, \
                    Download, Example, Info, \
                    EnrichmentJob, TFBSextractionJob, Post

# Definitions.

DAY_IN_SECONDS = 60 * 60 * 24
CACHE_TIMEOUT = 1 * DAY_IN_SECONDS

@cache_page(60)
def index(request):
    '''
    This loads the homepage
    '''

    setattr(request, 'view', 'index')

    form = SearchForm()

    # Obtain information specific to the version of the data.

    data_versions = list(map(lambda obj: obj.value, Info.objects.filter(key='data_version').order_by('-value')))
    examples = Example.objects.filter(example_type='search')
    tour_example = Example.objects.filter(example_type='tour_search').first()

    context = {
        'form': form,
        'examples': examples,
        'tour_example': tour_example,
        'data_version': data_versions[0],
        'previous_data_versions': data_versions[1:],
        'latest_data_version': LATEST_DATA_VERSION,
        }
    context.update(_get_statistics())

    return render(request, 'portal/index.html', context)

@cache_page(CACHE_TIMEOUT)
def search(request):
    '''
    This function returns the results based the on the search query,
    presented as a Web page.
    '''

    setattr(request, 'view', 'search')

    # Obtain form data from GET or POST requests, copying it for potential
    # modification.

    source = get_data_source(request)
    form = SearchForm(source.copy())

    # Convenience assignments for accessing the data.

    get = form.data.get
    getlist = form.data.getlist
    keys = form.data.keys

    # Update various controls.

    form.update_inputs()

    # Do not really care if the form is valid. Here, we just want the data.

    queryset = get_search_queryset(form.data)

    # Parameterise the field values based on existing choices.

    form.update_fields(queryset)

    # Handle too many parameters.

    cell_lines = getlist('cell_line')
    too_many_cell_lines = len(cell_lines) > SEARCH_MAX_CELL_LINES

    # Involved collections.

    selected_collection = get('collection')

    if selected_collection:
        collections = Collection.objects.filter(name=selected_collection)
    else:
        collections = Collection.objects.filter(factors__in=queryset)

    collections = collections.distinct().order_by('name')

    # Appropriate search examples.

    examples = Example.objects.filter(example_type='search')
    tour_example = Example.objects.filter(example_type='tour_detail').first()

    # Pagination of the queryset.

    page = get('page', 1)
    page_size = get('page_size', PAGINATION_DEFAULT)

    if not page_size or int(page_size) > MAX_PAGINATION_LIMIT:
        page_size = PAGINATION_DEFAULT

    paginator = SummaryPaginator(queryset, page_size)

    try:
        paginated = paginator.page(page)
    except PageNotAnInteger:
        paginated = paginator.page(1)
    except EmptyPage:
        paginated = paginator.page(paginator.num_pages)

    # Make parameters for navigation links.

    params = []
    for key in keys():
        for value in getlist(key):
            params.append((key, value))

    context = {
        'form': form,
        'factors': paginated,
        'collections': collections,
        'examples': examples,
        'tour_example': tour_example,
        'params': params,
        'q': get('q', ''),
        'too_many_cell_lines': too_many_cell_lines,
        }

    return render(request, 'portal/search.html', context)

@cache_page(CACHE_TIMEOUT)
def factor_detail(request, factor_id):
    '''
    This shows the details of a factor based on factor_id, showing a Web
    page.
    '''

    # Obtain the latest data version tag.

    data_tag = _get_data_tag()

    # Generate an archive for any indicated model name.

    model_name = request.GET.get('mtrain', None)

    if model_name:
        tar_file_name = 'UniBind_trained_model_%s_%s_%s.tar.gz' % (
                model_name, factor_id, _get_current_date())

        tar_file_path = join(TEMP_DIR, tar_file_name)
        target_path = join(BASE_DIR, 'static', 'data', data_tag, 'macs', model_name, factor_id)
        bed_pattern = join(target_path, "*.bed")
        fa_pattern = join(target_path, "*.fa")
        png_pattern = join(target_path, "*.png")

        # Create a tar archive excluding certain kinds of files.

        Archiver(tar_file_path, target_path, [".bed", ".fa", ".png"]).archive()

        # Redirect to the generated archive.

        return HttpResponseRedirect('/temp/%s' % tar_file_name)

    # Display the details otherwise.

    folder_name = factor_id
    factor = get_object_or_404(Factor, folder=folder_name)
    factor_details = FactorData.objects.filter(folder=folder_name)

    models = factor_details.values_list('prediction_model', flat=True) \
            .distinct().order_by('prediction_model')
    models = _pwm_first(models)

    collections = Collection.objects.filter(factors__in=[factor])

    # Configuration of download details.

    have_peaks = exists(join(BASE_DIR, 'static', 'data', data_tag, 'peaks', 'macs', factor_id))

    # Tour configuration using appropriate data.

    tour_example = Example.objects.filter(example_type='tour_search').first()

    # Similar/related datasets.

    similar_factors = _factor_summaries(
        Factor.objects.filter(tf_name=factor.tf_name)
        .exclude(folder=folder_name)
        .order_by('tf_name'))

    context = {
        'biological_conditions': _biological_conditions(factor.samples),
        'cell_lines': _cell_line_titles(factor.samples),
        'collections': collections,
        'data_tag': data_tag,
        'factor': factor,
        'factor_details': factor_details,
        'factors': similar_factors,
        'folder_name': folder_name,
        'have_peaks': have_peaks,
        'models': models,
        'peak_caller': 'MACS',
        'profile_ids': _profile_ids(factor_details),
        'references': _references(factor.references),
        'sources': _sources(factor.references),
        'tour_example': tour_example,
        }

    return render(request, 'portal/factor_detail.html', context)

@cache_page(CACHE_TIMEOUT)
def enrichment(request):
    '''
    UniBind enrichment analysis
    '''
    setattr(request, 'view', 'enrichment')

    # Delete older temporary files.

    _delete_temp_files(temp_path=TEMP_DIR, media_path=MEDIA_ROOT, days=TEMP_LIFE)

    # Show the enrichment front page if no form data is provided.

    if request.method != 'POST':
        return render(request, 'portal/enrichment.html', {'form': EnrichmentForm()})

    # Populate the form with POST data including files.

    form = EnrichmentForm(request.POST, request.FILES)

    # Adjust the required state of fields based on the selected analysis
    # type.

    form.update_requirements()

    # Rely on the form validation mechanism to handle input file
    # requirements. Where files are required but not provided, the form will
    # be invalid.

    if form.is_valid():
        get = form.cleaned_data.get

        # Obtain file inputs.

        bed_file_1 = get('bed_file_1')
        bed_file_2 = get('bed_file_2')
        bed_file_background = get('bed_file_background')

        fs = FileSystemStorage()
        filename = fs.save(bed_file_1.name, bed_file_1)
        file_a = fs.path(filename)
        file_b = None
        file_bg = None

        analysis_type = get('analysis_type')

        # Get second bed file differential analysis.

        if analysis_type == 'twoSets' and bed_file_2:
            filename = fs.save(bed_file_2.name, bed_file_2)
            file_b = fs.path(filename)

        # Get any background file.

        if analysis_type == 'oneSetBg' and bed_file_background:
            filename = fs.save(bed_file_background.name, bed_file_background)
            file_bg = fs.path(filename)

        # Create a temporary directory for the results.

        output_dir_path = tempfile.mkdtemp(
                    suffix='_%s' % _get_current_date(),
                    prefix='UniBind_%s_' % analysis_type,
                    dir=TEMP_DIR)

        if not exists(output_dir_path):
            os.makedirs(output_dir_path)

        # Obtain parameters for the enrichment process.

        collection = get('collection')
        tax_id = get('species')
        email = get('email')
        analysis_name = get("name") or dict(EnrichmentForm.ANALYSIS_TYPES)[analysis_type]

        # Obtain the actual species.

        species = Species.objects.get(tax_id=tax_id)

        # Get the command and run the analysis, save the results to the database.
        # Store the error output.

        cmd = unibind_enrichment_analysis(analysis_type, output_dir_path,
                                          collection, species.name,
                                          file_a, file_b, file_bg)

        if cmd:
            job = start_job(cmd, output_dir_path, analysis_name, analysis_type,
                            species, email, request)

            if job.id:
                return HttpResponseRedirect('/enrichment/%s' % job.slug)
            else:
                message_type = 'error'
                message = '''
Something went wrong with your job. Please try again and make sure your input
files are in BED format. You may <a href="/contact-us">contact us</a> with your
job identifier: %s
''' % job.id

        # Handle incompatible parameters.

        else:
            message_type = 'error'
            message = 'Something went wrong, please try again and make sure your input files are in BED format.'

        return render(request, 'portal/enrichment.html', {
            'form': form,
            'message_type': message_type,
            'message': message,
            })
    else:
        return render(request, 'portal/enrichment.html', {'form': form})

@xframe_options_exempt
def enrichment_results(request, enrichment_id):
    '''
    UniBind enrichment analysis results, presented as a Web page offering a
    download for the results.
    '''
    setattr(request, 'view', 'enrichment')

    results_path = join(TEMP_DIR, enrichment_id)

    # Test for the existence of the results directory. Its presence
    # indicates that the processing has at least been started or attempted.
    try:
        job = get_object_or_404(EnrichmentJob, slug=enrichment_id)
    except Http404:
        return render(request, 'portal/enrichment.html', {
            'results_path': results_path,
            'enrichment_id': enrichment_id,
            'message_type': "error",
            'message': "The results were not found!",
            'form': EnrichmentForm(),
            })

    # Show an absence of results, either due to the use of an invalid
    # identifier or to a failure in initiating result processing.

    if not job:
        return render(request, 'portal/enrichment.html', {
            'message_type': "error",
            'message': "Results not found!!",
            'form': EnrichmentForm(),
            })

    # Test for the presence of actual result files, finalising the job and
    # communicating completion to the template.

    processing = False
    empty_filtered_results = False

    # Check for a special success file.

    if exists(join(results_path, "success")):
        set_job_completed(job, request, results_path)

        # Note the presence of zero-size filtered results. This means that
        # filtering produced no results to work with, so any charts will be
        # showing the unfiltered results instead.

        empty_filtered_results = exists(join(results_path, "allEnrichments.tsv.filtered"))

    # Check for a special failure file.

    elif exists(join(results_path, "failed")):
        set_job_stopped(job, request)

    # The job is presumably still running.

    else:
        processing = True

    # Show either continuing progress or completion with a form.

    message = None
    message_type = None

    # The form uses a copied request dictionary so that it can be modified.

    form = EnrichmentFilterForm(request.POST.copy())

    # Update various controls.

    form.update_inputs()

    # Update the cell lines in the form, either using species information or
    # supplying cell line details from existing results.

    cell_lines = _read_values(join(results_path, "cell_summary"))

    # Determine which cell lines will be shown. These will be a combination of
    # selected cell lines and search results.

    form.update_fields(job.species, cell_lines)

    # Only invoke filtering if the appropriate form control was used to submit
    # the form. Otherwise, the cell lines will be updated and the page shown
    # again.

    if request.POST.get('performFiltering') and form.is_valid():
        cell_lines = form.cleaned_data.get('cell_line')

        # Without any cell lines, no filtering will be performed.

        if not cell_lines:
            message = 'No cell lines specified. Please select items in the list before submitting the form.'
            message_type = 'error'

        # Convert to actual objects. A ModelMultipleChoiceField would probably
        # do this automatically in a form, but a different kind of field is used
        # instead.

        else:
            cells = Cell.objects.filter(cell_id__in=cell_lines)

            # Create a temporary directory for the results. This uses a new
            # identifier, distinct from the identifier used for the existing
            # results.

            output_dir_path = tempfile.mkdtemp(
                        suffix='_%s' % _get_current_date(),
                        prefix='UniBind_%s_' % job.analysis_type,
                        dir=TEMP_DIR)

            if not exists(output_dir_path):
                os.makedirs(output_dir_path)

            # Attempt to start a new filtering job.
            # Since the form ultimately yields a cell object, access the title of
            # the cell to send to the enrichment script for filtering.

            cell_titles = [cell.title for cell in cells]

            cmd = unibind_enrichment_filtering(cell_titles, results_path,
                                               output_dir_path)

            job = start_job(cmd, output_dir_path, job.name, job.analysis_type,
                            job.species, job.email, request)

            if job.id:
                return HttpResponseRedirect('/enrichment/%s/' % job.slug)

            # Handle job failure, showing the form again.

            else:
                message_type = 'error'
                message = 'Something went wrong, please try again.'

    return render(request, 'portal/enrichment_result.html', {
        'results_path': results_path,
        'enrichment_id': enrichment_id,
        'processing': processing,
        'message': message,
        'message_type': message_type,
        'empty_filtered_results': empty_filtered_results,
        'form': form,
        'job': job,
        'days': TEMP_LIFE,
        })

def unibind_enrichment_analysis(analysis_type, output_dir_path, collection,
                                species, file_a, file_b=None, file_bg=None):
    '''
    This function performs the enrichment analysis of the given type, with
    output to the given directory, involving the indicated files. A command
    is returned with a false value indicating error.
    '''

    # Convert the collection name to a directory name.

    collection_dir = "LOLA_databases_%s" % collection

    # Convert the species name to a directory name.

    species_dir = species.replace(" ", "_")

    # Obtain the latest data version tag.

    data_tag = _get_data_tag()

    # Obtain the program and database locations.

    program = join(ENRICHMENT_DIR, "bin", "UniBind_enrich.sh")
    loladb = join(ENRICHMENT_DIR, "data", data_tag, collection_dir, species_dir, "UniBind_LOLA.RDS")
    lolauniverse = join(ENRICHMENT_DIR, "data", data_tag, collection_dir, species_dir, "UniBind_LOLA_universe.RDS")

    # Test the existence of the databases. Some databases may only exist in
    # certain collections.

    if not exists(loladb) or not exists(lolauniverse):

        # Permit usage of the bundled enrichment data for human data.

        if species != "Homo sapiens":
            return None

        loladb = join(ENRICHMENT_DIR, "data", "20190423_UniBind_LOLAdb.RDS")
        lolauniverse = join(ENRICHMENT_DIR, "data", "20190423_UniBind_LOLAuniverse.RDS")

        if not exists(loladb) or not exists(lolauniverse):
            return None

    # Enrichment against a given background set of genomic regions.
    # UniBind_enrich.sh oneSetBg <LOLA db> <S bed> <B bed> <output dir>

    if analysis_type == "oneSetBg" and file_a and file_bg:
        args = [file_a, file_bg]

    # Differential enrichment.
    # UniBind_enrich.sh twoSets <LOLA db> <S1 bed> <S2 bed> <output dir>

    elif analysis_type == "twoSets" and file_a and file_b:
        args = [file_a, file_b]

    # Enrichment when no background is provided.
    # UniBind_enrich.sh oneSetNoBg <LOLA db> <S bed> <output dir>

    elif analysis_type == "oneSetNoBg" and file_a:
        args = [lolauniverse, file_a]

    # Invalid type or missing requirements.

    else:
        return None

    return [program, analysis_type, loladb] + args + [output_dir_path]

def unibind_enrichment_filtering(cell_line_names, results_path, output_dir_path):
    '''
    Return command details for filtering using the given cell line name, the
    location of existing results, and the location for filtered results.
    '''

    program = join(ENRICHMENT_DIR, "bin", "UniBind_filter.sh")

    return [program, output_dir_path, results_path] + cell_line_names

def start_job(cmd, output_dir_path, name, analysis_type, species, email, request):
    '''
    Start a process using the given command details, establishing a job having
    the details provided.
    '''

    # Employ the directory leafname to label the results.

    output_dir_name = split(output_dir_path)[1]

    # Add details to the database.

    job_status = "Queued"

    job = EnrichmentJob(
        name=name,
        slug=output_dir_name,
        parameter=_quote_command(cmd),
        analysis_type=analysis_type,
        species=species,
        job_status=job_status,
        job_url=request.build_absolute_uri("/enrichment/%s" % output_dir_name),
        email=email,
        ip_address=_get_user_ip(request),
        browser_info=request.META['HTTP_USER_AGENT'])
    job.save()

    # Notify any identified initiator if the job is established.

    if job.id and email:
        send_email_notification(email, job.job_status, job.job_url)

    # Return, having queued the job for running elsewhere.

    return job

def set_job_completed(job, request, results_path):
    '''
    Set the given job to completed status, performing finalisation on the
    results and notifying the initiator.
    '''

    enrichment_id = job.slug
    output_filename = join(TEMP_DIR, "%s.tar.gz" % enrichment_id)

    # Produce any output archive.

    if not exists(output_filename) and \
       exists(join(results_path, "allEnrichments_swarm.pdf.png")) and \
       exists(join(results_path, "Unibind_enrichment_interactive_ranking.html")):

        Archiver(output_filename, TEMP_DIR).archive(enrichment_id)

    # Update the job status.
    # Send a notification if a mail address is provided.

    if job.job_status == 'Running':
        job.job_status = "Completed"
        job.save()
        if job.email:
            send_email_notification(job.email, job.job_status, job.job_url)

def set_job_stopped(job, request):
    '''
    Set the given job to stopped status, notifying the initiator.
    '''

    enrichment_id = job.slug

    if job.job_status == 'Running':
        job.job_status = "Stopped"
        job.save()
        if job.email:
            send_email_notification(job.email, job.job_status, job.job_url)

@cache_page(CACHE_TIMEOUT)
def TFBS_extraction(request):
    '''
    This loads the TFBS extraction page.
    '''

    setattr(request, 'view', 'TFBS_extraction')

    # Delete older temporary files.

    _delete_temp_files(temp_path=TEMP_DIR, media_path=MEDIA_ROOT, days=TEMP_LIFE)

    # Show the enrichment front page if no form data is provided.

    if request.method != 'POST':
        return render(request, 'portal/TFBS_extraction.html', {'form': TFBSextractionForm()})

    # Populate the form with POST data including files.

    form = TFBSextractionForm(request.POST, request.FILES)

    # Adjust the required state of fields based on the selected analysis
    # type.

    # form.update_requirements()

    # Rely on the form validation mechanism to handle input file
    # requirements. Where files are required but not provided, the form will
    # be invalid.

    if form.is_valid():
        get = form.cleaned_data.get

        # Obtain file inputs.

        fs = FileSystemStorage()

        bed_file = get('bed_file')

        filename = fs.save(bed_file.name, bed_file)
        file_a = fs.path(filename)

        # Check for presence of optional files (TF and experiment lists)

        file_TFs = None
        file_experiments = None

        TFs_file = get('TFs_file')

        if TFs_file:
            filename = fs.save(TFs_file.name, TFs_file)
            file_TFs = fs.path(filename)

        experiments_file = get('experiments_file')

        if experiments_file:
            filename = fs.save(experiments_file.name, experiments_file)
            file_experiments = fs.path(filename)

        # Create a temporary directory for the results.

        output_dir_path = tempfile.mkdtemp(
                    suffix='_%s' % _get_current_date(),
                    dir=TEMP_DIR)

        if not exists(output_dir_path):
            os.makedirs(output_dir_path)


        # Obtain parameters for the enrichment process.

        tax_id = get('species')
        collection = get('collection')
        email = get('email')
        analysis_name = get('name')

        # Obtain the actual species.

        species = Species.objects.get(tax_id=tax_id)

        genome = Genome.objects.get(species=tax_id)

        # Get the command and run the analysis, save the results to the database.
        # Store the error output.

        cmd = unibind_TFBS_extraction(genome.name, collection, species.name, output_dir_path, file_a, file_TFs, file_experiments)
        # cmd = unibind_TFBS_extraction(genome.name, collection, species.name, output_dir_path, file_a, file_TFs, file_experiments)

        if cmd:

            job = start_TFBS_extraction_job(cmd, output_dir_path, analysis_name, genome.name, email, request)

            if job.id:
                return HttpResponseRedirect('/TFBS_extraction/%s' % job.slug)
            else:
                message_type = 'error'
                message = '''
Something went wrong with your job. Please try again and make sure your input
files are in BED format. You may <a href="/contact-us">contact us</a> with your
job identifier: %s
''' % job.id

        # Handle incompatible parameters.

        else:
            message_type = 'error'
            message = 'Something went wrong, please try again and make sure your input files are in BED format.'

        return render(request, 'portal/TFBS_extraction.html', {
            'form': form,
            'message_type': message_type,
            'message': message,
            })
    else:
        return render(request, 'portal/TFBS_extraction.html', {'form': form})

    # return render(request, 'portal/TFBS_extraction.html', {})

@xframe_options_exempt
def TFBS_extraction_results(request, extraction_id):
    '''
    UniBind TFBS extraction results, presented as a Web page offering a
    download for the results.
    '''

    setattr(request, 'view', 'TFBS_extraction')

    results_path = join(TEMP_DIR, extraction_id)

    # Test for the existence of the results directory. Its presence
    # indicates that the processing has at least been started or attempted.
    try:
        job = get_object_or_404(TFBSextractionJob, slug=extraction_id)
    except Http404:
        return render(request, 'portal/TFBS_extraction.html', {
            'message_type': "error",
            'message': "The results were not found!",
            'form': TFBSextractionForm(),
            })

    # Show an absence of results, either due to the use of an invalid
    # identifier or to a failure in initiating result processing.

    if not job:
        return render(request, 'portal/TFBS_extraction.html', {
            'message_type': "error",
            'message': "Results not found!!",
            'form': TFBSextractionForm(),
            })

    # Test for the presence of actual result files, finalising the job and
    # communicating completion to the template.

    processing = False
    empty_filtered_results = False

    # Check for a special success file.

    if exists(join(results_path, "success")):
        set_TFBS_job_completed(job, request, results_path)

    # Check for a special failure file.

    elif exists(join(results_path, "failed")):
        set_job_stopped(job, request)

    # The job is presumably still running.

    else:
        processing = True

    # Show either continuing progress or completion with a form.

    message = None
    message_type = None

    return render(request, 'portal/TFBS_extraction_result.html', {
        'extraction_id': extraction_id,
        'processing': processing,
        'job': job,
        'days': TEMP_LIFE,
        })


def unibind_TFBS_extraction(genome, collection, species, output_dir_path, bed_file, file_TFs, file_experiments):
    '''
    This function performs the TFBS extracion with the input BED file and the
    selected TFBSs, with output to the given directory. A command is returned
    with a false value indicating error.
    '''

    # Convert the species name to a TFBS file path

    TFBSs_file = "%s_compressed_TFBSs.bed.gz" % genome

    # Obtain the latest data version tag.

    data_tag = _get_data_tag()

    # Obtain the program and database locations.

    program = join(TFBS_EXTRACTION_DIR, "bin", "extract_TFBSs_bedtools_gzip.sh")
    TFBSs_file = join(TFBS_EXTRACTION_DIR, "data", data_tag, "bulk_%s" % collection, species.replace(' ', '_'), TFBSs_file)

    # Check if optional files are provided and add to command if true

    opt_args = []

    if file_TFs is not None:
        opt_args = opt_args + ['-t', file_TFs]

    if file_experiments is not None:
        opt_args = opt_args + ['-e', file_experiments]


    # extract_TFBSs_bedtools_gzip.sh -i INPUT BED -b GZIPPED BED [-t TFs] [-e EXPERIMENT IDs]

    if exists(TFBSs_file) and exists(bed_file):
        args = [program, '-i', bed_file, '-b', TFBSs_file, '-o', output_dir_path] + opt_args
        return args

    # Invalid type or missing requirements.

    else:
        return None

def start_TFBS_extraction_job(cmd, output_dir_path, name, genome, email, request):
    '''
    Start a process using the given command details, establishing a job having
    the details provided.
    '''

    # Employ the directory leafname to label the results.

    output_dir_name = split(output_dir_path)[1]

    # Add details to the database.

    job_status = "Queued"

    job = TFBSextractionJob(
        name=name,
        slug=output_dir_name,
        parameter=_quote_command(cmd),
        job_status=job_status,
        job_url=request.build_absolute_uri("/TFBS_extraction/%s" % output_dir_name),
        email=email,
        ip_address=_get_user_ip(request),
        browser_info=request.META['HTTP_USER_AGENT'])

    job.save()

    # Notify any identified initiator if the job is established.

    if job.id and email:

        send_email_notification(email, job.job_status, job.job_url)

    # Return, having queued the job for running elsewhere.

    return job

def set_TFBS_job_completed(job, request, results_path):
    '''
    Set the given job to completed status, performing finalisation on the
    results and notifying the initiator.
    '''

    extraction_id = job.slug
    output_filename = join(TEMP_DIR, "%s" % extraction_id, "extraction_results.bed")

    # Produce any output archive.

    # if not exists(output_filename) and \
    #    exists(join(results_path, "allEnrichments_swarm.pdf.png")) and \
    #    exists(join(results_path, "Unibind_enrichment_interactive_ranking.html")):
    #
    #     Archiver(output_filename, TEMP_DIR).archive(enrichment_id)

    # Update the job status.
    # Send a notification if a mail address is provided.

    if job.job_status == 'Running':
        job.job_status = "Completed"
        job.save()
        if job.email:
            send_email_notification(job.email, job.job_status, job.job_url)

def _get_current_date():
    '''
    Return this instant's date in the form YYYYMMDD.
    '''

    return datetime.datetime.now().strftime("%Y%m%d")

@cache_page(CACHE_TIMEOUT)
def documentation(request):
    '''
    This shows the documentation page.
    '''
    setattr(request, 'view', 'docs')

    return render(request, 'portal/documentation.html')

@cache_page(CACHE_TIMEOUT)
def download_data(request):
    '''
    This shows the downloads page.
    '''
    setattr(request, 'view', 'downloads')

    models = []

    for model_name in FactorData.objects.values_list('prediction_model', flat=True) \
                                        .distinct().order_by('prediction_model'):
        model = {
            'name': model_name,
            'prefix': model_name.lower(),
            }
        models.append(model)

    collections = []

    for collection in Collection.objects.all().order_by('name'):
        organisms = []

        # Collect together organism and genome details for use in linking to
        # appropriately named files.

        for species in collection.species.order_by('name'):

            # Produce a suitable organism definition.

            organism = {
                'name': species.name,
                'genome': species.genome.name,
                'genome_prefix': '%s_' % species.genome.name,
                'suffix': 'tar.gz',
                'bed_suffix': 'bed',
                'compressed_bed_suffix': 'bed.gz',
                'filename': species.name.replace(' ', '_'),
                'id': species.tax_id,
                }

            organisms.append(organism)

            # Override the downloads for species, removing any entries for files
            # that are optional and not present.

            organism['downloads'] = _get_downloads('species', collection, organism)
            _set_organism_models(collection, organism, models)

        # Add a common entry for all organisms if more than one are present.

        if len(organisms) > 1:

            organism = {
                'name': 'All',
                'genome': '',
                'genome_prefix': '',
                'suffix': 'tar',
                'bed_suffix': 'tar.gz',
                'compressed_bed_suffix': 'tar.gz',
                'filename': 'All',
                'id': 'All',
                }

            organisms.insert(0, organism)
            organism['downloads'] = _get_downloads('species', collection, organism)
            _set_organism_models(collection, organism, models)

        details = {
            'name': collection.name,
            'organisms': organisms,
            }

        # NOTE: Handling the robust collection in a special way.

        if collection.name == 'Robust':
            collections.insert(0, details)
        else:
            collections.append(details)

    # Fallback downloads list.

    downloads = Download.objects.all().order_by('id')

    # Test for peak data in order to configure the interface.

    data_tag = _get_data_tag()
    have_peaks = exists(join(BASE_DIR, 'static', 'data', data_tag, 'peaks'))

    context = {
        'collections': collections,
        'data_tag': data_tag,
        'downloads': downloads,
        'have_peaks': have_peaks,
        'models': models,
        'peak_caller': 'MACS',
        }

    return render(request, 'portal/downloads.html', context)

def _set_organism_models(collection, organism, models):
    '''
    In the given collection, add model details including downloads to the given
    organism dictionary.
    '''

    organism['models'] = organism_models = []

    # Provide model download details within each organism.

    for model in models:
        organism_model = {
            'downloads' : _get_downloads('model', collection, organism, model),
            }
        organism_model.update(model)
        organism_models.append(organism_model)

def _get_downloads(scope, collection, organism, model=None):
    '''
    Return download records for the given scope, collection and organism,
    omitting records for unavailable, optional files.
    '''

    data_tag = _get_data_tag()
    downloads = []

    for download in Download.objects.filter(scope=scope).order_by('id'):

        # Test file presence only when the download is optional.

        if download.presence == 'optional':

            # Employ the appropriate suffix.

            if download.suffix == 'bed':
                suffix = organism['bed_suffix']
            elif download.suffix == 'compressed_bed':
                suffix = organism['compressed_bed_suffix']
            else:
                suffix = organism['suffix']

            # Add any necessary model prefix.

            model_prefix = scope == 'model' and model and ('%s_' % model['prefix']) or ''

            # Construct the filename to test.

            filename = join(BASE_DIR, 'static', 'data', data_tag,
                            'bulk_%s' % collection.name,
                            organism['filename'],
                            '%s%s%s.%s' % (model_prefix,
                                           organism['genome_prefix'],
                                           download.basename,
                                           suffix))

            if exists(filename):
                downloads.append(download)
        else:
            downloads.append(download)

    return downloads

@cache_page(CACHE_TIMEOUT)
def api_documentation(request):
    '''
    This shows the API documentation page.
    '''
    setattr(request, 'view', 'api-home')

    return render(request, 'restapi/api_home.html')

@cache_page(CACHE_TIMEOUT)
def contact_us(request):
    '''
    Contact and feedback page to send mail to the site administrators.
    '''
    setattr(request, 'view', 'contact_us')

    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            #from_name = form.cleaned_data['from_name']
            message = form.cleaned_data['message']

            email = EmailMessage(
                subject,
                'From: '+from_email+'\n\nMessage: '+message,
                from_email,
                SEND_TO_EMAIL,
                reply_to=[from_email],
                )
            try:
                email.send()
            except BadHeaderError:
                context ={'message': 'Invalid header found. Your message did not go through.', 'message_type': 'error', }
                return render(request, 'portal/contact_us.html', context)

            context = {'message': 'Thank you! Your message has been sent successfully. We will get back to you shortly.', 'message_type': 'success'}

            return render(request, 'portal/contact_us.html', context)

    return render(request, 'portal/contact_us.html', {'form': form})

@cache_page(CACHE_TIMEOUT)
def faq(request):
    '''
    This shows the FAQ page.
    '''

    setattr(request, 'view', 'faq')

    return render(request, 'portal/faq.html')

@cache_page(CACHE_TIMEOUT)
def changelog(request):
    '''
    This shows the changelog page.
    '''

    setattr(request, 'view', 'changelog')

    return render(request, 'portal/changelog.html')

@cache_page(CACHE_TIMEOUT)
def tour_video(request):
    '''
    This shows the tour video page.
    '''

    setattr(request, 'view', 'tour')

    return render(request, 'portal/tour_video.html')

@cache_page(CACHE_TIMEOUT)
def about(request):
    '''
    This shows the about page.
    '''
    setattr(request, 'view', 'about')

    context = _get_statistics()

    return render(request, 'portal/about.html', context)

@cache_page(CACHE_TIMEOUT)
def genome_tracks(request):

    setattr(request, 'view', 'tracks')

    collections = []

    for collection in Collection.objects.all().order_by('name'):
        organisms = []

        for organism in collection.species.order_by('name'):
            d = {
                'name': organism.name,
                'quoted_name': organism.name.replace(' ', '_'),
                'genome' : organism.genome,
                }
            organisms.append(d)

        details = {
            'name': collection.name,
            'organisms': organisms,
            }

        # NOTE: Handling the robust collection in a special way.

        if collection.name == 'Robust':
            collections.insert(0, details)
        else:
            collections.append(details)

    context = {
        'collections': collections,
        'data_tag': _get_data_tag(),
        }

    return render(request, 'portal/genome_tracks.html', context)

@cache_page(CACHE_TIMEOUT)
def post_details(request, year, month, day, slug):
    '''
    Show individual news/updates.
    '''
    post = get_object_or_404(Post, slug=slug)

    posts = Post.objects.all().order_by('-date')[:5]

    return render(request, 'portal/blog_single.html', {
        'post': post,
        'posts': posts,
        })

@cache_page(CACHE_TIMEOUT)
def post_list(request):
    '''
    List all news/updates.
    '''
    posts = Post.objects.all().order_by('-date')

    return render(request, 'portal/blog.html', {
        'posts': posts,
        })

def page_not_found(request, exception):
    '''
    Return custom 404 error page.
    '''

    return render(request, '404.html', status=404)

def server_error(request):
    '''
    Return custom 500 error page.
    '''

    return render(request, '500.html', status=500)

def _delete_temp_files(temp_path=TEMP_DIR, media_path=MEDIA_ROOT, days=TEMP_LIFE):
    '''
    Delete temporary files in the given temporary files directory (TEMP_DIR by
    default) as well as uploads in the given media directory (MEDIA_ROOT by
    default), selecting files with an age greater than the indicated number of
    days (TEMP_LIFE by default).

    The default values can be modified in the unibind.settings files, with the
    TEMP_LIFE setting being most useful.
    '''

    today = datetime.datetime.today()
    jobs = EnrichmentJob.objects.filter(created_at__lt=today - datetime.timedelta(days=days), deleted_at=None)
    if jobs:
        for job in jobs:
            archive_basename = join(temp_path, job.slug)
            if isdir(archive_basename):
                rmtree(archive_basename)
            archive_filename = "%s.tar.gz" % archive_basename
            if exists(archive_filename):
                os.remove(archive_filename)
            job.deleted_at = today
            job.save()

    # Delete uploaded BED files.

    current_time = time.time()
    for filename in os.listdir(media_path):
        filename = join(media_path, filename)
        if os.stat(filename).st_mtime < current_time - days * DAY_IN_SECONDS:
            if isdir(filename):
                rmtree(filename)
            else:
                os.remove(filename)

# Statistical overview.

def _get_statistics():
    '''
    Return a dictionary containing statistical information.
    '''

    cell_lines = _cell_lines(FactorSample.objects)

    datasets = Factor.objects.all().count()

    available_species = Factor.objects.all().values('species').distinct()

    featured_species = Species.objects.filter(image_name__isnull=False) \
                                      .filter(tax_id__in=available_species) \
                                      .order_by('name')

    tf_count = Factor.objects.all().values('species', 'tf_name') \
            .distinct().count()

    context = {
        'datasets': datasets,
        'tf_count': tf_count,
        'featured_species': featured_species,
        'cell_lines': cell_lines,
        }

    return context

# Search query processing.

def _get_matrix_term(term):
    '''
    Obtain the base identifier from any matrix identifier of the
    form "<base>.<version>", where <base> is of the form "MA<num>".
    '''

    field, value = term

    if field not in (None, 'tf'):
        return term

    t = value.split('.', 1)
    if len(t) == 2 and t[0][0:2] == 'MA':
        return field, t[0]
    else:
        return term

def _get_query_terms(query_string):
    '''
    Return a list of (field, value) terms.
    '''

    terms = []

    for token in _get_query_tokens(query_string):
        t = token.split(':', 1)
        field = len(t) > 1 and t[0] or None
        value = t[-1]
        terms.append((field, value))

    return terms

def _get_query_tokens(query_string):
    '''
    Return a list of tokens, with quotation marks being used to group
    characters into single tokens.
    '''

    pattern = re.compile('"(.*?)"')
    unquoted = True
    tokens = []

    # Split the query using quoted blocks as separators. These blocks will
    # be single tokens, with the surrounding text being split on whitespace
    # to produce individual tokens.

    for value in pattern.split(query_string):
        if unquoted:
            tokens += value.split()
        else:
            tokens.append(value)
        unquoted = not unquoted

    return tokens

# Search result preparation.

class SummaryPaginator(Paginator):
    def _get_page(self, *args, **kwargs):
        return SummaryPage(*args, **kwargs)

class SummaryPage(Page):
    def __getitem__(self, index):
        return _factor_summary(Page.__getitem__(self, index))

def _factor_summaries(factors):
    '''
    Return a list of dictionaries that can be used to present search results
    and the list of similar/related factors in the detail view.
    '''

    results = []
    for factor in factors:
        results.append(_factor_summary(factor))
    return results

def _factor_summary(factor, simple=False):
    collections = Collection.objects.filter(factors__in=[factor])
    return {
        'biological_conditions': _biological_conditions(factor.samples),
        'cell_lines': _cell_line_titles(factor.samples),
        'collections': collections,
        'folder': factor.folder,
        'profile_ids': _profile_ids(factor.datasets),
        'references': _references(factor.references, simple),
        'sources': _sources(factor.references),
        'species': simple and factor.species.name or factor.species,
        'tf_name': factor.tf_name,
        }

def _biological_conditions(factor_samples):
    '''
    Return all distinct biological conditions, ordered, from a query
    involving samples. Blank values are removed.
    '''

    return list(filter(None,
               factor_samples.values_list('biological_condition', flat=True)
               .distinct().order_by('biological_condition')))

def _cell_lines(factor_samples):
    '''
    Return all distinct cell types, ordered, from a query involving samples.
    '''

    return Cell.objects.filter(cell_id__in=factor_samples.values('cell')) \
                       .distinct()

def _cell_line_titles(factor_samples):
    '''
    Return all distinct cell type titles, ordered, from a query involving
    samples.
    '''

    return list(_cell_lines(factor_samples).values_list('title', flat=True) \
                                           .order_by('title'))

def _profile_ids(factor_details):
    '''
    Return all distinct JASPAR profile identifiers, ordered, from a query
    involving factor data/details.
    '''

    profile_ids = []

    for jaspar_id, jaspar_version in factor_details.values_list(
        'jaspar_id', 'jaspar_version').distinct().order_by(
        'jaspar_id', 'jaspar_version'):

        profile_id = '%s.%s' % (jaspar_id, jaspar_version)
        profile_ids.append(profile_id)

    return profile_ids

def _pwm_first(models):
    '''
    Reorder the supplied collection of models, putting "PWM" first.
    '''

    models = list(models)
    try:
        i = models.index('PWM')
        del models[i]
        models.insert(0, 'PWM')
    except ValueError:
        pass
    return models

def _references(factor_references, simple=False):
    '''
    Return all reference details, ordered by database and identifier, from a
    query involving factor references. Each item in the returned list is a
    FactorRef instance with ref_db providing access to a RefSource instance.
    '''

    # A simple representation involves a list of "<db>:<id>" values.

    if simple:
        return ["%s:%s" % (ref_db, ref_id) for (ref_db, ref_id) in
                factor_references.values_list('ref_db', 'ref_id')
                                 .order_by('ref_db', 'ref_id')]

    # Aggregate references using the data source name, producing a list of
    # (source_name, references) elements.

    aggregated = []

    for source_name, references in groupby(factor_references.all()
                                           .order_by('ref_db', 'ref_id'),
                                           lambda obj: obj.ref_db.source_name):

        aggregated.append((source_name, list(references)))

    return aggregated

def _source_ids(factor_references):
    '''
    Return all database identifiers, ordered, from a query involving
    factor references.
    '''

    return factor_references.values_list('ref_id', flat=True) \
        .order_by('ref_id')

def _sources(factor_references):
    '''
    Return all database reference details, ordered, from a query involving
    factor references.
    '''

    return factor_references.values_list('ref_db', flat=True) \
        .distinct().order_by('ref_db')

def _get_user_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def _quote_command(cmd):
    '''
    Return a safely quoted string for the given command token list.
    This is provided by shlex.join in Python 3.8.
    '''

    return " ".join(map(shlex.quote, cmd))

def _read_values(filename):
    '''
    Read values from the given filename, returning None if the file does not
    exist.
    '''

    if not exists(filename):
        return None

    with open(filename) as f:
        values = set()
        for line in f.readlines():
            value = line.strip()
            if value:
                values.add(value)
        return values

def _get_data_tag():
    '''
    Return the data tag: a version indicator for the data to be accessed.
    '''

    return Info.objects.filter(key='data_tag').order_by('-value').first().value

# vim: tabstop=4 expandtab shiftwidth=4
