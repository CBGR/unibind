{% extends "restapi/api.html" %}
{% load static %}
{% load i18n %}

{% block title %}
RESTful API Overview
{% endblock %}

{# Replace the default REST framework content with the following content. #}

{% block content %}
<div class="content-main" role="main"  aria-label="{% trans "main content" %}">
  <div class="page-header">
    <h1>{% block content_header %}API Overview{% endblock %}</h1>
  </div>

  <div class="row">
    <div class="col-sm-2">
      <nav id="toc" data-spy="affix" data-toggle="toc"></nav>
    </div>

    <div class="col-md-10 col-lg-10 col-xs-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><i class="fas fa-info-circle"> </i> API Overview</h3>
        </div>

        <div class="box-body" data-spy="scroll" data-target="#toc">
          <p>The UniBind RESTful API provides programmatic and human-browsable
          access to the UniBind database. This is implemented in Python using the
          Django REST Framework.</p>

          <h3 class="page-header">API Versioning</h3>
          <p>Currently, the UniBind API is at version 1, being available at
          <b>{{ request.get_api_host }}</b>. New versions will be released here
          with new URLs and there will be a prior notice before obsoleting any
          older API version.</p>

          <h3 class="page-header">Throttling</h3>
          <p>Our API is using throttling in order to control the rate of requests
          that clients can make to the API. We allow <b>25</b> requests per second
          from the <b>same IP address</b>, but no limit on the total number of
          requests.</p>

          <p>Please feel free to write to us if you need higher request.</p>

          <h3 class="page-header">Pagination</h3>
          <p>To provide a faster response to each request and to prevent larger
          accidental downloads, the API provides pagination. Users can increase or
          decrease the number of records per pages by setting
          <code>page_size</code>. By default <code>page_size=10</code>, which can
          be increased up to <b>1000</b>.</p>

          <pre>{{ request.get_api_host }}datasets/?page=1&amp;page_size=25</pre>

          <p>To jump from one page to another, modify the <code>page</code>
          parameter:</p>

          <pre>{{ request.get_api_host }}datasets/?page=2</pre>

          <h3 class="page-header">Ordering</h3>
          <p>This supports simple query parameter controlled ordering of results.
          The query parameter is named <code>order</code>.</p>

          <p>For example, to order datasets by transcription factor name:</p>

          <pre>{{ request.get_api_host }}datasets/?order=tf_name</pre>

          <p>The client may also specify reverse order by prefixing the field name
          with '-', like this:</p>

          <pre>{{ request.get_api_host }}datasets/?order=-tf_name</pre>

          <p>Multiple orderings may also be specified:</p>

          <pre>{{ request.get_api_host }}datasets/?order=tf_name,version</pre>

          <h3 class="page-header">Output formats</h3>
          <p>The REST API provides several data renderer types that allow you to
          return responses with various media types. The query parameter is named
          <code>format</code>.</p>

          <p>Currently, available data formats are <code>json</code>,
	  <code>jsonp</code>, <code>yaml</code>, <code>api</code>.</p>

          <p>For example, to return all the datasets in JSON format:</p>

          <pre>{{ request.get_api_host }}datasets/?format=json</pre>

          <p>You can set the output format type in three different ways:</p>

          <ol>
            <li>By setting <code>?format=format</code> url parameter. For
              example:
              <ul>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=json">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=json</a></li>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=jsonp">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=jsonp</a></li>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=yaml">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=yaml</a></li>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=api">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3/?format=api</a></li>
              </ul>
            </li>
          
            <li>By adding <code>.format</code> suffix. For example:
              <ul>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.json">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.json</a></li>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.jsonp">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.jsonp</a></li>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.yaml">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.yaml</a></li>
                <li><a href="{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.api">{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3.api</a></li>
              </ul>
            </li>

            <li><p>By using the <a href="http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html">Accept headers</a>.
              For example:</p>

<pre class="prettyprint">
curl '{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3' -H 'Accept: application/json'
curl '{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3' -H 'Accept: application/javascript'
curl '{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3' -H 'Accept: application/yaml'
curl '{{ request.get_api_host }}datasets/GSE38103.lx2_tgfb1.SMAD3' -H 'Accept: text/html'
</pre>

              <p>If there is a conflict between the Accept headers and the format
              parameter, the following error message will be returned:</p>

<pre class="prettyprint">
curl '{{ request.get_api_host }}datasets/?format=jsonp' -H 'Accept: application/json'
{
"detail": "Could not satisfy the request's Accept header"
}
</pre>
            </li>
          </ol>

          <h3 class="page-header">CORS requests</h3>
          <p>UniBind API also supports
          <a href="https://en.wikipedia.org/wiki/Cross-origin_resource_sharing"
             target="_blank">Cross-Origin Resource Sharing (CORS)</a>, which
          enables users to make cross-origin API requests directly from their
          web application.</p>

          <h3 class="page-header">Caching</h3>
          <p>Setting up the cache is very useful to enhance the performance of
          the API. For this API, we are using <a href="http://memcached.org/"
          target="_blank">Memcached</a>, which is a fast, efficient and entirely
          memory-based cache server. It is used by sites such as Facebook and
          Wikipedia to reduce database access and dramatically increase site
          performance. Memcached runs as a daemon and is allotted a specified
          amount of RAM. All it does is provide a fast interface for adding,
          retrieving and deleting data in the cache. All data is stored directly
          in memory, so there’s no overhead of database or filesystem usage.

          We are using a Python Memcached binding
          <a href="https://pypi.python.org/pypi/python-memcached"
             target="_blank"> python-memcached</a>, which is running on localhost
          (127.0.0.1) port 11211.</p>
        </div>
      </div>
    </div>
  </div>
</div>
{% endblock %}

{# Add a stylesheet to make the table-of-contents highlighting work. #}

{% block style %}
{{ block.super }}
<link rel="stylesheet" type="text/css" href="{% static "restapi/css/bootstrap-toc.min.css" %}"/>
{% endblock %}

{# Scripts used by the REST framework appear at the end of the page body. Here,
   a table-of-contents script is added. In addition, although the main document
   element employs data-spy and data-target elements, it appears that they may
   be evaluated before the table is built, causing its initialisation to fail
   (and thus the current section is not highlighted when scrolling). So, an
   extra script is used to reinitialise the "ScrollSpy" mechanism after the
   document has loaded. #}

{% block script %}
{{ block.super }}
<script src="{% static "restapi/js/bootstrap-toc.min.js" %}"></script>
<script src="{% static "restapi/js/toc.js" %}"></script>
{% endblock %}
