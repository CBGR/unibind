#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Label the wrapper script.

ENRICH="$BASEDIR/bin/UniBind_enrichment.py"

semanage fcontext -a -t bin_t "$ENRICH"
restorecon "$ENRICH"
