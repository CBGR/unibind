#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain the Python version and amend the path configuration.

PYTHON=`"$THISDIR/tools/python_version"`
export PYTHONPATH="$BASEDIR/lib/usr/local/lib/$PYTHON/site-packages:$BASEDIR/lib/usr/local/lib64/$PYTHON/site-packages"

# Initialise static content.

"$PYTHON" "$BASEDIR/manage.py" collectstatic --no-input

# Create a secret key.

if [ ! -s "$BASEDIR/secret.txt" ] ; then
    "$THISDIR/tools/make_secret.py" > "$BASEDIR/secret.txt"
fi
