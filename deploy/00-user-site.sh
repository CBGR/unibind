#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain any site identifier and use that instead of the default.

SITE=
SITE_DEFAULT="test-unibind.uio.no"

while getopts s: NAME ; do
    case "$NAME" in
        s) SITE=$OPTARG ;;
        *) echo "Option ignored." 1>&2 ;;
    esac
done

shift $((OPTIND - 1))

# Record the site details for other operations.

if [ ! -e "$THISDIR/work" ] ; then
    mkdir -p "$THISDIR/work"
fi

SITE_FILE="$THISDIR/work/site.txt"

if [ "$SITE" ] || [ ! -e "$SITE_FILE" ] ; then
    echo ${SITE:-$SITE_DEFAULT} > "$SITE_FILE"
fi
