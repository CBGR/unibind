#!/bin/sh

# SPDX-FileCopyrightText: 2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

cat <<EOF > "$THISDIR/conf/crontab"
MAILTO=""
EOF

# Generate an entry for each version of the application.

for FILENAME in "$BASEDIR/unibind/settings"*".py" ; do

    # Stop if no settings files are found.

    if [ ! -e "$FILENAME" ] ; then
        break
    fi

    # Obtain the module name. This contains the data tag for each version.

    MODULE=`basename "$FILENAME" .py`

    # Skip generic modules.

    if [ "$MODULE" = 'settings' ] || [ "$MODULE" = 'settings_template' ] ; then
        continue
    fi

    # Append to the crontab for each configuration.

    cat <<EOF >> "$THISDIR/conf/crontab"
* * * * * /var/www/apps/unibind/bin/UniBind_enrichment.py unibind.${MODULE} first 1
* * * * * /var/www/apps/unibind/bin/UniBind_TFBS_extraction.py unibind.${MODULE} first 1
EOF
done
