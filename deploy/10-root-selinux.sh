#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Initialise directories.

for DIRNAME in media temp ; do
    DIRPATH="$BASEDIR/$DIRNAME"

    # Set a label for SELinux, apply the labelling for SELinux.

    semanage fcontext -a -t httpd_sys_rw_content_t "$DIRPATH(/.*)?"
    restorecon -R "$DIRPATH"
done
