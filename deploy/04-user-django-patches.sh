#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# A simplistic substitution for Python version adjustments.

make_updated()
{
    sed "s/$1/$2/g" < "$3"
}

# Obtain the Python version for parameterising files.

PYTHON=`"$THISDIR/tools/python_version"`

# Patch Django to work correctly with SELinux.

  make_updated "python3.7" "$PYTHON" "$THISDIR/patches/patch-django-file-move-selinux.diff" \
| patch -N -d "$BASEDIR" -p0
