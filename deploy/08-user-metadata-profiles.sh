#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Import the metadata into the database from each versioned metadata directory.

for WORKDIR in "$THISDIR/work/metadata/"* ; do

    # Ignore any backups.

    if basename "$WORKDIR" | grep -q 'backup' ; then
        continue
    fi

    if [ -d "$WORKDIR" ] ; then

        # Employ a version-specific database.

        TAG=`basename "$WORKDIR"`
        DATABASE="$BASEDIR/UniBind${TAG}.sqlite3"

        # Update each database.

        "$THISDIR/tools/update_matrix_profiles" `realpath "$WORKDIR"` "$DATABASE"
    fi
done
