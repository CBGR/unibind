#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Obtain the existing crontab, removing any existing UniBind-related entries.
# The MAILTO setting is also removed because it will be re-added below.

  crontab -u apache -l \
| grep -v UniBind \
| grep -v MAILTO \
> "$THISDIR/work/crontab.old"

# Concatenate the old crontab to the one installed here.

  cat "$THISDIR/work/crontab.old" "$THISDIR/conf/crontab" \
> "$THISDIR/work/crontab"

# Make sure we really have the MAILTO. Otherwise, mails get sent, sometimes to
# all kinds of people who didn't expect them.

if ! grep -q "MAILTO" "$THISDIR/work/crontab" ; then
    echo "MAILTO missing from crontab in $THISDIR/work/crontab - refusing to install!" 1>&2
    exit 1
fi

# Install the crontab for enrichment job processing.

crontab -u apache "$THISDIR/work/crontab"

# Install access control rule.

if [ ! -e "/etc/security/access.d" ] ; then
    mkdir -p "/etc/security/access.d"
fi

# Update the configuration if necessary.

if ! cmp -s "$THISDIR/conf/90-apache-cron.conf" "/etc/security/access.d/" ; then
    cp "$THISDIR/conf/90-apache-cron.conf" "/etc/security/access.d/"
fi

# vim: tabstop=4 expandtab shiftwidth=4
