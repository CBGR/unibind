#!/bin/sh

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

for DATABASE in "$BASEDIR/UniBind"*".sqlite3" ; do
    BASENAME=`basename "$DATABASE" .sqlite3`
    TAG=${BASENAME#UniBind}

    # Obtain a data version tag from each database file, using it to configure
    # the static data.

    if [ ! "$TAG" ] ; then
        continue
    fi

    "$THISDIR/tools/fix_bulk_metadata" "$TAG"
    "$THISDIR/tools/fix_hub_data" "$TAG"
done
