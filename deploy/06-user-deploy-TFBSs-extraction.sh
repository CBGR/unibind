#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

EXTRACTION="/var/www/apps/unibind_tfbs_extraction"

# Detect enrichment.

if [ ! -e "$EXTRACTION" ] ; then
    cat 1>&2 <<EOF
Please install the UniBind TFBS extraction repository at the following location:

$ENRICHMENT
EOF
    exit 1
fi

# Run the deployment script to initialise required data.

"$EXTRACTION/deploy.sh"
