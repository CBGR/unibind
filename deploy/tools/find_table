#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

DB=$1

if [ ! "$DB" ] || [ ! -e "$DB" ] ; then
    cat 1>&2 <<EOF
Usage: $0 <database filename> <database column> <database table>...

Find the appropriate table having column in the specified database.
EOF
    exit 1
fi

COLUMN=$2

shift 2

# Choose the table appropriate for the schema version.

TABLE=

for NAME in $* ; do
    if sqlite3 "$DB" "select $COLUMN from $NAME limit 1" > /dev/null 2>&1 ; then
        TABLE=$NAME
        break
    fi
done

if [ "$TABLE" ] ; then
    echo "$TABLE"
else
    exit 1
fi
