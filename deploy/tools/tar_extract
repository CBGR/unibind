#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`

FILENAME=$1
UNPACKDIR=$2

# Create the directory if necessary.

if [ ! -e "$UNPACKDIR" ] ; then
    mkdir -p "$UNPACKDIR"
fi

# Attempt to unpack, removing leading path components that indicate the
# data origin. For archives only containing files, no components are
# removed.

FLAG=`"$THISDIR/tar_flag" "$FILENAME"`

if tar "$FLAG"tf "$FILENAME" | head -n 1 | grep -q '^\./' ; then
    STRIP=2
elif tar "$FLAG"tf "$FILENAME" | head -n 1 | grep -q '\/' ; then
    STRIP=1
else
    STRIP=0
fi

tar "$FLAG"xf "$FILENAME" -C "$UNPACKDIR" --strip-components="$STRIP"
