#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.management.utils import get_random_secret_key

print(get_random_secret_key())
