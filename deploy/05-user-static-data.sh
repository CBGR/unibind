#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`
NOW=`date +%Y%m%d`

# Permit deployment of the latest files.

if [ "$1" = '--latest' ] ; then
    LATEST=$1
    shift 1
else
    LATEST=
fi

# Define a common storage location.

STATIC="$BASEDIR/static"

# Find the source data.

#SOURCEDIR="/var/tmp/unibind-data"
SOURCEDIR="/div/pythagoras/u4/rafaelri/unibind-data"

if [ ! -e "$SOURCEDIR" ] ; then
    echo "UniBind data directory missing: $SOURCEDIR" 1>&2
    exit 1
fi

# Unpack the data into the output directory.

for FILENAME in `find "$SOURCEDIR" -maxdepth 1 -name '*.tar*'` ; do

    # Only unpack the latest file of a given type.

    if [ "$LATEST" ] && ! "$THISDIR/tools/latest_file" "$FILENAME" > /dev/null ; then
        echo "Ignoring older file: $FILENAME" 1>&2
        continue
    fi

    # Identify the file type.

    FILETYPE=`"$THISDIR/tools/file_type" "$FILENAME"`
    TAG=`"$THISDIR/tools/file_tag" "$FILENAME"`

    # Obtain a suitable distinguishing name based on the archive name.

    FILETYPENAME=`"$THISDIR/tools/file_type" "$FILENAME" --name`

    # Set the versioned data root.

    DATADIR="$STATIC/data/$TAG"

    # Choose a suitable directory for unpacking.

    UNPACKTEST=

    # Bulk downloads, peak data and genome browser track hubs are unpacked
    # directly.

    if [ "$FILETYPE" = 'bulk' ] ; then
        UNPACKDIR="$DATADIR/$FILETYPENAME"
    elif [ "$FILETYPE" = 'peaks' ] ; then
        UNPACKDIR="$DATADIR/$FILETYPENAME"
    elif [ "$FILETYPE" = 'UniBind_hubs' ] ; then
        UNPACKDIR="$DATADIR/$FILETYPENAME"

    # TFBS data is unpacked directly, with a specific directory name used to
    # determine the presence of existing data.

    elif [ "$FILETYPE" = 'tfbs' ] ; then
        UNPACKDIR="$DATADIR"
        UNPACKTEST="$UNPACKDIR/macs"

    # Metadata is written to release-specific directories.

    elif [ "$FILETYPE" = 'tfbs_metadata' ] ; then
        UNPACKDIR="$THISDIR/work/metadata/$TAG"

    # Synonyms are derived from the most recent JASPAR release and are applied
    # to all UniBind releases.

    elif [ "$FILETYPE" = 'matrix_synonyms' ] ; then
        UNPACKDIR="$THISDIR/work/synonyms/all"

    else
        echo "Ignoring unrecognised file: $FILENAME" 1>&2
        continue
    fi

    UNPACKTEST=${UNPACKTEST:-$UNPACKDIR}

    # Unpack the archive if apparently necessary.

    if [ "$FILENAME" -nt "$UNPACKTEST" ] ; then

        echo "Processing file: $FILENAME" 1>&2

        # Back up any existing data.

        if [ -e "$UNPACKTEST" ] ; then
            mv -f "$UNPACKTEST" "$UNPACKTEST.backup-$NOW"
        fi

        "$THISDIR/tools/tar_extract" "$FILENAME" "$UNPACKDIR"
        touch "$UNPACKTEST"
    fi
done
