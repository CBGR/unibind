#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Initialise directories.

for DIRNAME in media temp ; do
    DIRPATH="$BASEDIR/$DIRNAME"

    # Create the directory, set an ACL for Apache.

    mkdir "$DIRPATH"
    setfacl -m u:apache:rwx "$DIRPATH"
done
