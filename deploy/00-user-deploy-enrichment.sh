#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

ENRICHMENT="/var/www/apps/unibind_enrichment"

# Detect enrichment.

if [ ! -e "$ENRICHMENT" ] ; then
    cat 1>&2 <<EOF
Please install the UniBind enrichment repository at the following location:

$ENRICHMENT
EOF
    exit 1
fi

# Run the deployment script to initialise required data.

"$ENRICHMENT/deploy.sh"
