#!/bin/sh

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

THISDIR=`dirname "$0"`
BASEDIR=`realpath "$THISDIR/.."`

# Set permissions on the application directory for SQLite write access.
# See: https://stackoverflow.com/questions/3319112/sqlite-error-attempt-to-write-a-readonly-database-during-insert

setfacl -m u:apache:rwx "$BASEDIR"

# Set permissions for each database.

for DATABASE in "$BASEDIR/UniBind"*".sqlite3" ; do
    setfacl -m u:apache:rw "$DATABASE"
done
