Upgrading or Introducing Data
=============================

The deployment_ process may be used to introduce data to the deployed
application. Where existing data is to be upgraded or replaced, data archives
will be unpacked and processed, backups of existing databases and files
created, and the new data will be made available transparently.

Where new data is to be introduced, new database and filesystem directories
will be created. These resources need to be referenced by the application, and
where a settings module does not exist for the data, it will be created by the
deployment scripts. For example, data tagged as ``20201216`` will be
referenced by a settings file found at ``unibind/settings20201216.py``
appropriately configured to use the ``UniBind20201216.sqlite3`` database.

The WSGI script supporting a given UniBind release may need updating to refer
to the specific data version. For example, ``unibind/wsgi2021.py`` when
referring to, say, ``20200918`` will need updating to refer to ``20201216``
following an upgrade as shown above.

The ``crontab`` used for enrichment job monitoring and e-mail notifications
may also need updating to use the current data versions. For example,
``settings20200918`` will need updating to refer to ``settings20201216`` in
the program invocations used by the ``crontab``.

.. _deployment: Deployment.rst
