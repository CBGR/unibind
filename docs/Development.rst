Development
===========

The following development topics are covered by this documentation:

- `Application settings and configuration <Settings.rst>`_
- `Configuration of additional functionality <Configuration.rst>`_
- `Database schema details <Schema.rst>`_
- `Database usage <Database.rst>`_
- `Deployment of the software <Deployment.rst>`_
- `Documentation generation <Documentation.rst>`_
- `System architecture <Architecture.rst>`_
- `System testing <Testing.rst>`_
- `Upgrading and introducing data <Upgrading.rst>`_
