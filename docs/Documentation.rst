Documentation
=============

The documentation provided in this software distribution is in the
`reStructuredText`_ format with document filenames having the ``.rst`` suffix.
This provides a way of writing something similar to plain text but with
relatively simple adornments to indicate headings and to communicate things
like emphasis, these demonstrating the appropriate formatting effects when the
documents are processed by a conversion tool to produce output in formats like
HTML.

Alongside textual content, images are also used to provide diagrams showing
database or architectural details. Such diagrams are typically defined using
the `Graphviz`_ DOT format, with diagram filenames having the ``.dot`` suffix,
and are then transformed into SVG images for display in HTML documents.


Software Requirements
---------------------

To build the documentation files (like this one), the Docutils, Pygments and
Graphviz tools are needed. On Fedora and Red Hat systems, as well as on Debian
systems, the ``python3-docutils``, ``python3-pygments`` and ``graphviz``
packages provide such software.


Building the Documentation
--------------------------

To make the preparation of documents and images relatively convenient, a
script called ``make_docs.sh`` is provided in the ``docs`` directory of the
distribution. To produce a collection of output documents and images in HTML
and SVG formats alongside the original input files, the following command may
be used from the top level of the distribution:

.. code:: shell

   docs/make_docs.sh

It is usually more convenient to generate output documents and images in a
separate directory, such as one called ``html``. This can be done as follows:

.. code:: shell

   docs/make_docs.sh html

For viewing as static Web pages, it is also helpful to generate symbolic links
for the original document filenames, redirecting links in the documents to the
output files. This is done as follows:

.. code:: shell

   docs/make_docs.sh --links html

For updating the repository on Bitbucket, the SVG files need to be updated but
the textual documents do not, since Bitbucket can display the original
documents as HTML. For such repository updates, use the following:

.. code:: shell

   docs/make_docs.sh --diagrams-only

.. _reStructuredText: https://docutils.sourceforge.io/rst.html
.. _Graphviz: https://graphviz.gitlab.io/
