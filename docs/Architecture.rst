System Architecture
===================

This document seeks to describe the architecture and general organisation of
the UniBind Web application. Paths to files are given relative to the top
level of this source distribution.

.. contents::


Site Map
--------

As expected with a Django_ application, the general site structure of the
application is defined using ``urls.py`` files, known as URLconfs_ (URL
configurations). The following are defined in this Web application:

========================= ==================================================
URLconf                   Description
========================= ==================================================
``portal/urls.py``        Browsable and navigable resources
``restapi/v1/urls.py``    Version-specific REST interface (API) resources
``unibind/urls.py``       Site top-level description
========================= ==================================================

The ``unibind/urls.py`` file incorporates the other URLconfs by using the
include_ function.

Notable URLs
++++++++++++

The following are URLs reachable via the menubar provided on each page by the
``portal/templates/portal/header.html`` template:

===================== =================== ==================================
Menubar Item          URL                 Target Page Details
===================== =================== ==================================
UniBind               ``/``               Main page of UniBind
Home                  ``/``               Main page of UniBind
Search                ``/search``         Search dialogue
Download              ``/downloads``      Download bulk data
Genome tracks         ``/genome-tracks``  Genome browser tracks
Doc                   ``/docs``           Documentation and help
About                 ``/about``          Background information
Changelog             ``/changelog``      Changelog details
Enrichment Analysis   ``/enrichment``     TFBS set enrichment analysis
Contact               ``/contact-us``     Contact details
===================== =================== ==================================

Transcription factor details are provided via the ``/factor/`` prefix, with
the following URL component indicating the identifier. For example:

::

  /factor/ENCSR000EFY.K562.ARID3A/

Enrichment
''''''''''

Enrichment analysis results are provided via the ``/enrichment/`` prefix, with
the following URL component indicating the result identifier. Such identifiers
are the directory names generated in the ``enrichment`` view function for file
generation. For example:

::

  /enrichment/UniBind_twoSets_9vx0n22v_20200205/

Visiting such a URL will yield a page showing a generic progress indicator if
result processing is still ongoing. Otherwise, a page appears showing the
results and offering download links.

Since the progress indicator page polls the server periodically, as soon as
the ``allEnrichments_swarm.pdf.png`` file appears in the results directory
(created inside ``temp`` and bearing the result identifier as its name), the
next request polling the URL will cause the page to change from the progress
page to the results page.

Note that enrichment jobs are actually only recorded in the database by the
Web application in the ``enrichment_jobs`` table. A separate program,
``UniBind_enrichment.py``, found in the ``bin`` directory, is invoked
periodically by a scheduled ``cron`` job and checks for queued jobs in the
database. Where no jobs are running and at least one is queued, a queued job
will be started. This arrangement is intended to limit the load on the server
from enrichment jobs and to provide a framework for e-mail notifications and
simple job control.

Generating a Site Map
+++++++++++++++++++++

A crude sitemap can be generated by installing the ``django-extensions``
package (using ``pip``), adding ``'django_extensions'`` to ``INSTALLED_APPS``
in the settings_, and then running the following:

.. code-block:: bash

  python manage.py show_urls

This will omit the URLs served under ``/static``, but these are described
below.


Static Files
------------

Static files and resources are served for the application using the specific
``django.contrib.staticfiles`` application, defined in ``INSTALLED_APPS`` in
the settings_. They are served under ``/static`` within the application.

Bundled Files
+++++++++++++

The ``portal/static`` and ``restapi/static`` directories in this source
distribution contain bundled files that are served statically. These consist
mostly of stylesheets, scripts and images.

These files are deployed to the ``static`` directory found at the top
level of this distribution through the use of the ``collectstatic`` operation
of the Django ``manage.py`` tool. Here, they will be augmented by other static
data that is not supplied in this distribution.

Pre-Generated Files
+++++++++++++++++++

The ``/static/data`` area is supposed to contain pre-generated resources for
serving statically such as...

============================= ==============================================
URL Prefix                    Resources
============================= ==============================================
``/static/data/bulk/``        Bulk data for the "Download data" page
``/static/data/macs/``        Model output artefacts for each dataset
``/static/data/peaks/macs/``  Peak information for each dataset
``/static/UniBind_hubs/``     Genome Browser resources
============================= ==============================================

The ``/static/data`` resources are produced by the ChIP-eat pipeline and are
then installed into the appropriate location.

In a production environment, a dedicated Web server such as Apache will employ
an ``Alias`` configuration or equivalent to serve these files at the indicated
URL prefix. In the test environment, the files will need to be copied to the
``static/data`` directory within this software distribution. In both cases, the
organisation of the ``static`` directory hierarchy corresponds directly to the
URL hierarchy.

Bulk Data Downloads
'''''''''''''''''''

The ``portal/templates/portal/downloads.html`` template links to the different
files providing bulk data.

Model Output Artefacts
''''''''''''''''''''''

The ``portal/templates/portal/factor_detail.html`` template links to the
different model output artefact files.

This data is provided by files with names of the following form::

  static/data/macs/<model>/<dataset>/<dataset>.<matrix_id>.<model_variant>.<extension>

- ``model`` is one of ``BEM``, ``DNAshaped``, ``PWM``, ``TFFM``
- ``dataset`` corresponds to ``factor_id`` in program code, ``folder`` in the
  database schema
- ``matrix_id`` is the JASPAR matrix profile identifier
- ``model_variant`` is one of ``bem``, ``DNAshaped4bits``, ``DNAshapedPSSM``,
  ``DNAshapedTFFM``

The ``extension`` corresponds to resources as follows:

=================== ========================================================
Extension           Description
=================== ========================================================
``bed``             TFBS download in BED format
``classifier.pkl``  DNAshaped-specific classifier state details
``fa``              TFBS download in FASTA format
``png``             Chart for display for each model on factor details pages
``svg``             TFFM-specific sequence logos
``thr``             Threshold-related properties
``xml``             TFFM-specific GHMM_ state details
=================== ========================================================

For example::

  static/data/macs/BEM/ENCSR000AQU.DND41.CTCF/ENCSR000AQU.DND41.CTCF.MA0139.1.bem.bed

Of note are the charts for transcription factor details pages which are
provided in PNG format. For example::

  static/data/macs/BEM/ENCSR000AQU.DND41.CTCF/ENCSR000AQU.DND41.CTCF.MA0139.1.bem.png

Trained model downloads are provided from the factor detail page by archiving
files from a given dataset directory.

.. _GHMM: http://ghmm.sourceforge.net/

Peak Information
''''''''''''''''

The ``portal/templates/portal/factor_detail.html`` template provides a
"Download ReMap Peaks" link for each dataset (independent of the chosen model
in the interface) referencing a file with a name of the following form::

  static/data/peaks/macs/<dataset>/<dataset>.narrowPeak

For example::

  static/data/peaks/macs/ENCSR000AQU.DND41.CTCF/ENCSR000AQU.DND41.CTCF.narrowPeak

Generated Files
+++++++++++++++

Generated files are also served statically under the following URL prefixes:

========================= ==================================================
URL Prefix                Resources
========================= ==================================================
``/download/``            (Not defined in the application)
``/temp/``                Enrichment analysis results, trained model files
========================= ==================================================

The filesystem locations of these prefixes are defined by the ``DOWNLOAD_DIR``
and ``TEMP_DIR`` settings_.  They will correspond directly to directories
positioned at the top-level of this software distribution when running in test
mode.

The ``portal/views.py`` file contains the file generation actions:

======================= ====================================================
View Function           Details
======================= ====================================================
``factor_detail``       Generates model from pre-generated resources
``enrichment``          Creates a directory and initiates output generation
``enrichment_results``  Generates an archive from the enrichment results
======================= ====================================================


Models
------

Django accesses databases_ via models comprising a particular schema_.

.. _databases: Database.rst
.. _schema: Schema.rst


Templates
---------

The output generated for Web pages and accessed resources is described by a
number of templates, defined in the following locations:

===================================== ======================================
Template Collection                   Purpose
===================================== ======================================
``templates/admin``                   Administration interface
``templates/rest_framework``          REST API access
``templates/rest_framework_docs``     REST API documentation
``templates/rest_framework_swagger``  "Live API" interface to the REST API
``portal/templates/portal``           Portal interface
===================================== ======================================


.. _Django: https://docs.djangoproject.com/en/2.2/
.. _include: https://docs.djangoproject.com/en/2.2/ref/urls/#django.urls.include
.. _settings: Settings.rst
.. _URLconfs: https://docs.djangoproject.com/en/2.2/topics/http/urls/
