Schema Investigations
=====================

Some investigations into the properties of the schema were performed to
establish the basis of various entities and their relationships. Although some
of the queries are still valid for the current schema, others are now no
longer applicable. For example, data source and identifier details have now
been relocated to a separate database references table.

Queries
-------

Folder plus number of factors per folder:

.. code:: sql

  select folder, count(*) as factors from factor group by folder;

Expanding the above, giving the distribution of factor involvement per folder
(number of factors per folder, number of folders having a collection of this
size):

.. code:: sql

  select factors, count(*) from (
    select folder, count(*) as factors from factor group by folder
    ) as factors_per_folder
  group by factors order by factors;

This should indicate that there will only ever be one factor record per
folder. (Indeed, the schema should state that ``folder`` is a unique column in
the ``factor`` table.)

Folder plus number of factor data records per folder:

.. code:: sql

  select folder, count(*) as factors from factor_data group by folder;

Expanding the above, giving the distribution of data records per folder
(number of records per folder, number of folders having a collection of this
size):

.. code:: sql

  select records, count(*) from (
    select folder, count(*) as records from factor_data group by folder
    ) as records_per_folder
  group by records order by records;

Experiment Correspondence
+++++++++++++++++++++++++

The ``identifier`` column provides experiment values. Meanwhile, the
``folder`` combines the ``identifier``, ``cell_line`` and ``tf_name``. The
correspondence between experiments and records in the ``factor`` table can be
measured as follows:

.. code:: sql

  select num_folders, count(*) from (
    select identifier, count(distinct folder) as num_folders
    from factor
    group by identifier) as folders_per_experiment
  group by num_folders order by num_folders;

This indicates that most experiments provide only one dataset employing a
given cell line and transcription factor. However, many experiments appear to
provide several datasets. For example, GSE80151 from GEO provides a number of
folders:

::

  GSE80151.BE2C.MYCN
  GSE80151.BE2C.TWIST1
  GSE80151.KELLY.MYCN
  GSE80151.NGP.MYCN
  GSE80151.SHEP21.MYCN
  GSE80151.SHEP21.TWIST1
  GSE80151.SHEP21_dox_0h.MYCN
  GSE80151.SHEP21_dox_0h.TWIST1
  GSE80151.SHEP21_dox_24h.MYCN
  GSE80151.SHEP21_dox_24h.TWIST1

These correspond to some, but not all, sample records in the GEO source
record.

Protein Name Correspondence
+++++++++++++++++++++++++++

JASPAR maps profiles to proteins. With a simple mapping table exported from
JASPAR having ``base_id``, ``version`` and ``uniprot_ac`` columns, the
following query investigates the correspondence between UniBind transcription
factor names and proteins:

.. code:: sql

  select proteins, min(tf_name), count(*) from (
    select tf_name, count(distinct uniprot_ac) as proteins
    from factor inner join jaspar_proteins
      on jaspar_id = base_id and jaspar_version = version
    group by tf_name
    ) as proteins_per_name
  group by proteins order by proteins;

This will largely show that most names correspond to one protein. However, a
small number of names correspond to two or three proteins, through either name
or profile ambiguity.
