Deployment
==========

The deployment of this application involves the installation and configuration
of software, mostly conducted using deployment scripts. Once this has been
done, various system services need to be enabled and started.

The latter system configuration activities are not automated here since the
discretion of the system administrator may need to be applied with regard to
integrating the application with its environment.

Some additional configuration_ may be required alongside this deployment
activity.


.. contents::


Prerequisites for Deployment
----------------------------

The rest of this document assumes that a number of prerequisites are
satisfied.

Firstly, the software must be retrieved and placed in a suitable location. The
following location is recommended:

::

  /var/www/apps/unibind

Use of the above location will require that the parent directory
``/var/www/apps`` exists and is writable for the user performing the
deployment.

The ``git`` tool must be available, although it may already have been used
when obtaining the software.

The ``sudo`` command must be available to the user performing the deployment.

Filesystem support for POSIX access control lists (ACLs) is required, this
being provided through the use of the ``getfacl`` and ``setfacl`` commands.

On Red Hat distributions such as Fedora, CentOS and Red Hat Enterprise Linux,
SELinux will be used.

The Apache Web server is used to deploy the software, and where secure
protocols are to be used, the `Let's Encrypt`_ certificate service is accessed
using the appropriate system tools. See below for more information. Before
deployment, make sure that the ``certbot`` tool is configured, performing
registration with the service if necessary:

.. code:: shell

  sudo certbot register

Familiarisation with the operation of ``certbot`` and the certificate renewal
process is recommended.


Deployment Data
---------------

Before the software is deployed, it is recommended that the data used by the
software be available, consisting of a number of archives described in the
UniBind data processing software documentation. These archives should then be
placed in the following location:

::

  /var/tmp/unibind-data

Where an alternative location is desired, this can be specified to the main
deployment script as described below.

UniBind data archives will have names resembling the following:

::

  bulk_Permissive_20210421.tar
  bulk_Robust_20210421.tar
  tfbs_20210421.tar.gz
  tfbs_metadata_20210421.tar.gz
  UniBind_hubs_Permissive_20210421.tar.gz
  UniBind_hubs_Robust_20210421.tar.gz

Together with UniBind data, a synonym data archive from the JASPAR data
preparation activity needs to be provided. It will also reside in the
indicated data location and have a name of the following form:

::

  matrix_synonyms_2022.tar.gz

Here, ``2022`` may be replaced by a different, potentially revised or more
recent indicator.


Application Branding and Customisations
---------------------------------------

Various images are used in UniBind as icons or buttons to refer to other online
resources, but these images are not freely redistributable. In addition, the
logos of partner organisations may be shown on the official UniBind site, but
any agreements with those organisations will not apply to unofficial
deployments of the site or of this software more generally. Other resources
related to analytics are usually deployed for official instances of the site,
but these will involve specific account details or credentials that are not
shared in this distribution.

To provide such resources, a directory called ``unibind-branding`` must reside
alongside ``unibind`` in the filesystem. Without such a directory, generic
images are used for various icons or buttons, partner logos will be absent,
and analytics scripts will not be deployed. See the section below for further
information. For official UniBind deployment, please contact the maintainers
for a suitable archive containing the appropriate resources.


Deployment Scripts
------------------

A suite of deployment scripts has been provided to automate deployment. The
suite is run using the ``deploy.sh`` script which will then invoke the
different scripts found in the ``deploy`` directory in turn to attempt to
prepare the application environment.

Where scripts are marked as ``root`` they will need to be run with elevated
privileges. To see what the ``deploy.sh`` script will do with regard to script
invocation, see the output of running it with the ``-n`` option:

.. code:: shell

  ./deploy.sh -n

A Web site name can be indicated to indicate a particular Web site
configuration. For example:

.. code:: shell

  ./deploy.sh -- -s testunibind.uio.no

Here, the additional ``--`` is necessary since the ``-s`` option is passed to
various more specific deployment scripts as an extra argument or option.

Where the data to be deployed will reside in an alternative location, this can
be indicated as follows (using the location ``/tmp`` as an example):

.. code:: shell

  ./deploy.sh -- -d /tmp

The above configuration options can also be combined.

Additional Web Server Tasks
+++++++++++++++++++++++++++

Once deployed, the Web server may need additional configuration. For example,
a default Web site may be configured, and this default site may need removing.
On Debian systems, this can be done as follows:

.. code:: shell

  a2dissite 000-default.conf

Once deployed, the Web server may need restarting. This can currently be done
in various GNU/Linux environments using the following command:

.. code:: shell

  systemctl restart httpd

On Debian systems, ``apache2`` will be the service name.


Deployment Script Descriptions
------------------------------

Some details of the individual scripts are given below.

System Package Installation
+++++++++++++++++++++++++++

The ``00-root-packages.sh`` script installs the packages listed in one of the
following files:

- ``requirements-sys.txt`` for Fedora and Red Hat systems
- ``requirements-sys-debian.txt`` for Debian systems

Some of these packages are actually needed to build other kinds of packages,
such as those provided by Python.

The ``00-user-deploy-enrichment.sh`` script attempts to invoke the deployment
process for the UniBind Enrichment software distribution in order to make that
software available for the deployed UniBind application. The
``unibind/settings.py`` file employs the ``ENRICHMENT_DIR`` setting to
indicate the location of this software.

The ``00-user-site.sh`` script records the server name of the site to be
deployed. This name, plus variations derived from the different data versions,
is used to configure the Web server and to obtain secure site certificates.
The site name can be specified as an extra argument to the ``deploy.sh`` script
as described above, along with an option to indicate the deployment of a secure
site (one using SSL/TLS). This option ensures that in the ``deploy/conf``
directory, a symbolic link to the secure site template is established for the
indicated site. Such links are used as a means of remembering which sites
require deployment as secure sites. The script also records the location of
data for deployment which can be overridden through the use of an option.

Other Package Installation
++++++++++++++++++++++++++

The ``01-user-dependencies.sh`` script installs non-system packages as an
unprivileged user in a directory local to the application. These packages are
listed in the following files:

- ``requirements.txt`` for Python packages with no version constraints
- ``requirements-python3.5.txt`` for packages suitable for Python 3.5

Related Data
++++++++++++

The ``02-user-branding.sh`` script installs logos and other resources that
customise the site for its official deployment. If a sibling directory of this
distribution's directory exists with the name ``unibind-branding``, such
resources will be copied from this other directory into the site resources,
making various partner organisation logos appear in the Web site footer, and
links to other resources will bear the appropriate logos. Without such a
sibling directory, the partner organisation logos will be absent, and linked
resources will bear various generic logos. (This exercise is performed to
avoid the accidental usage of logos by anyone deploying this software
independently, and it also aims to avoid issues with the redistribution of
logo images where the terms of redistribution are not generally clear or where
such redistribution is forbidden.)

Directory Creation
++++++++++++++++++

The ``02-user-directories.sh`` script creates directories for use by the
application in its top-level directory, setting permissions appropriately.
Typically, the ``media`` and ``temp`` directories are used by Django
applications and these will need to be accessible by the Web server. Such
access can be conferred by setting an ACL (access control list) rule.

Django-Related Initialisation
+++++++++++++++++++++++++++++

The ``03-user-django.sh`` script performs administrative tasks related to the
initialisation of the Django-based Web application such as collecting static
resources and making sure that the database migrations have been applied.

Django Modifications
++++++++++++++++++++

The ``04-user-django-patches.sh`` script applies a patch to fix the behaviour
of file moving operations in Django. Without this patch, file attribute
modifications are attempted that SELinux rejects, and various file
manipulation operations will be broken.

Data Provision
++++++++++++++

The ``05-user-static-data.sh`` script unpacks data archives and populates the
``static/data`` directory with generated data providing charts/plots,
sequences, models and other details related to individual records. In
addition, bulk data archives are copied to the appropriate directory within
``static/data``. Data is organised into separate versioned hierarchies in this
directory to permit multiple sites (each corresponding to a specific release)
to be served from the same repository.

The ``06-user-metadata.sh`` script populates version-specific databases with
metadata, thus providing the navigable structure of the data presented in each
version-specific site. The version details are obtained from metadata archives
unpacked by the ``05-user-static-data.sh`` script.

The ``07-user-data.sh`` script populates various tables with common and
configuration data such as information describing the different download
resources, genome browser resource details, URLs for linked databases, and
species information.

The ``07-user-static-data-fix.sh`` script introduces symbolic links to the
unpacked static data so that the Web application can find resources in the
appropriate places, principally ``PWM`` resources in the ``DiMO`` hierarchy.

The ``08-user-metadata-profiles.sh`` script fixes profile references in each
database.

Permissions and SELinux Labelling
+++++++++++++++++++++++++++++++++

The ``06-user-sqlite-apache.sh`` script fixes various permissions for SQLite
to work with Apache.

The ``10-root-selinux.sh`` script applies SELinux labelling to the ``media``
and ``temp`` directories so that the Web server may update them.

The ``10-root-selinux-sqlite.sh`` script applies SELinux labelling to the
SQLite database and its parent directory so that the database can be updated.

The ``11-root-selinux-bin.sh`` script labels the enrichment job monitoring and
initiation script so that it may be run periodically.

Web Server Configuration
++++++++++++++++++++++++

The ``11-root-httpd.sh`` script copies a Web server configuration file from the
``conf`` directory into the appropriate location. A secure site certificate is
also retrieved and employed if this is needed for the indicated Web site.

Scheduled Job Configuration
+++++++++++++++++++++++++++

The ``07-user-crontab.sh`` script defines the scheduled jobs needed to support
the enrichment functionality.

The ``11-root-crontab.sh`` script installs the generated ``crontab`` for the
configured instances of the application so that enrichment jobs may be run.

Web Server Requirements
-----------------------

The application is deployed using the Apache Web server and mod_wsgi server
module, informed by the `WSGI deployment documentation`_. The mod_wsgi `quick
configuration guide`_ provides relatively approachable documentation about
appropriate deployment techniques.

On Fedora or Red Hat systems, the ``python3-mod_wsgi`` package provides
mod_wsgi, and this package is listed in the system requirements file,
``requirements-sys.txt``. It will cause the ``httpd`` package to be installed
for Apache.

On Debian systems, the ``libapache2-mod-wsgi-py3`` package provides mod_wsgi,
and this packages is listed in the system requirements file,
``requirements-sys-debian.txt``. It will cause the ``apache2`` package to be
installed for Apache.

The Let's Encrypt ``certbot`` and ``python3-certbot-apache`` packages are used
to interact with the certificate service and to administer secure site
certificates.


.. _configuration: Configuration.rst

.. _`Let's Encrypt`: https://letsencrypt.org/

.. _`WSGI deployment documentation`:
   https://docs.djangoproject.com/en/2.2/howto/deployment/wsgi/modwsgi/

.. _`quick configuration guide`:
   https://modwsgi.readthedocs.io/en/develop/user-guides/quick-configuration-guide.html
