Database Usage
==============

The ``DATABASES`` mapping, found in the settings_, indicates how the UniBind
data is to be accessed within the application. Currently, the following
methods are defined:

=========== ================================================================
Method      Details
=========== ================================================================
SQLite_     Use a SQLite database file, for testing purposes only
MySQL       Use a MySQL-compatible database deployed separately
=========== ================================================================

Currently, only SQLite_ is used in deployed instances of the software.

The ``ENGINE`` and ``NAME`` settings within the ``DATABASES`` mapping indicate
the database access class and the database location or name respectively.

Data Version Selection
----------------------

For a particular instance of the UniBind Web site, a handler program will be
invoked by the Web server to provide the site content and behaviour. This
program selects settings_ specific to the site instance in order to display
data from a particular database providing data for a specific version of
UniBind. To select the appropriate database, the version-specific settings
file replaces the generic ``NAME`` setting in the ``DATABASES`` mapping,
indicating the database location.

.. _settings: Settings.rst
.. _SQLite: https://www.sqlite.org/
