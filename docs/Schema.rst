Schema
======

This document describes the form of the database schema.

Django_ accesses databases_ via models, and such model definitions are found
in the ``portal/models.py`` file. A more conventional schema can be inspected
by entering a database environment directly using an appropriate database
file. For example:

.. code:: shell

  sqlite3 UniBind20200918.sqlite3

In the SQLite_ command environment, the following command can be used to
display the entire database schema:

.. code:: sqlite3

  .schema

.. _databases: Database.rst
.. _Django: https://docs.djangoproject.com/en/2.2/
.. _SQLite: https://www.sqlite.org/


.. contents::


Overview
--------

The following diagram attempts to summarise the entities in the schema and
their relationships.

.. image:: Schema.svg

Of particular interest is the many-to-many relationship between genes and
profiles moderated by the ``ProfileGene`` entity.

Natural and Surrogate Keys
++++++++++++++++++++++++++

Note that Django employs surrogate keys due to limitations with its
object-relational mapper. In a more sophisticated system, compound primary
keys might be employed. This surrogate key (often called ``id``) provides a
means of referencing each record and its values may be used by other tables to
reference records in the tables concerned.


Entity Descriptions
-------------------

Cell or Tissue Type
+++++++++++++++++++

Table
  ``cell``
Model class
  ``Cell``

Each record in the ``cell`` table provides details of a cell or tissue type.
Such records are referenced by the sample information records for experimental
data.

Collection
++++++++++

Table
  ``collection``
Model class
  ``Collection``
Natural key
  ``name``

Each record in the ``collection`` table provides details of a data collection.
Transcription factors are organised into collections such as ``Permissive`` or
``Robust``, potentially multiple collections. As a consequence, species can
also be regarded as being members of collections depending on whether they are
associated with transcription factors that are members of those collections.

Factor
++++++

Table
  ``factor``
Model class
  ``Factor``
Natural key
  ``folder``

Each record in the ``factor`` table represents a dataset providing a
transcription factor occurrence in experimental data. Thus, potentially many
records in this table may correspond to a single ``profile`` record.

The transcription factor name (``tf_name``) column potentially identifies an
unambiguous protein. However, investigation using JASPAR protein information
suggests that since profiles may correspond to multiple proteins (for example,
MA0006.1), a single name may therefore be associated with multiple proteins.
Meanwhile, general naming ambiguity may lead to names being used with multiple
profiles (for example, POU5F1), and even with a one-to-one profile-to-protein
correspondence, a name may be associated with multiple proteins as a result.

The ``species`` field (``tax_id`` column) associates a transcription factor
with a species (defined in the ``species`` table). This is distinct from any
species defined for an experiment or sample.

Factor Data or Dataset
++++++++++++++++++++++

Table
  ``factor_data``
Model class
  ``FactorData``

Each record in the ``factor_data`` table represents the application of a
prediction model to experimental data. Thus, potentially many records in this
table may correspond to a single ``factor`` record.

Factor Database Reference
+++++++++++++++++++++++++

Table
  ``factor_ref``
Model class
  ``FactorRef``

Each record in the ``factor_ref`` table provides a database reference related
to experimental data. Thus, potentially many records in this table may
correspond to a single ``factor`` record.

Factor Sample Details
+++++++++++++++++++++

Table
  ``factor_sample``
Model class
  ``FactorSample``

Each record in the ``factor_sample`` table provides biological information for
experimental data. Thus, potentially many records in this table may correspond
to a single ``factor`` record.

Gene
++++

Table
  ``genes``
Model class
  ``Gene``
Natural key
  ``gene_id``

Each record in the ``genes`` table describes a gene from the Entrez Gene
database.

Gene Synonym
++++++++++++

Table
  ``gene_synonyms``
Model class
  ``GeneSynonym``
Natural key
  (``gene_id``, ``synonym``)

Each record in the ``gene_synonyms`` table provides a synonym recorded for the
indicated gene from the Entrez Gene database.

Profile
+++++++

Table
  ``profiles``
Model class
  ``Profile``
Natural key
  (``jaspar_id``, ``jaspar_version``)

Each record in the ``profiles`` table represents a profile denoted by a
distinct versioned JASPAR identifier (a combination of ``jaspar_id`` and
``jaspar_version``).

Profile Gene
++++++++++++

Table
  ``profile_genes``
Model class
  ``ProfileGene``

This table has been introduced to map profiles to genes. Each profile may be
associated with multiple proteins, and each protein may be associated with
multiple genes. The involvement of proteins has been omitted from this schema.

Sources
+++++++

Table
  ``ref_db_sources``
Model class
  ``RefSource``
Natural key
  ``source_name``

This table provides the details of database sources from which the
experimental data originated.

Species
+++++++

Table
  ``species``
Model class
  ``Species``
Natural key
  ``tax_id``

This table provides additional information about individual species associated
with factors. It is referenced by the ``tax_id`` column of the
``factor`` table.


Additional Entities
-------------------

A number of additional entities are present in the database to configure the
appearance of the Web application. The following diagram shows these entities.

.. image:: Schema-site.svg

Download
++++++++

Table
  ``downloads``
Model class
  ``Download``

This table provides details of download files for the bulk downloads page.
Populated using data from the ``data/downloads.txt`` file in the software
distribution, it describes the downloads available for different data
collections at the level of species or prediction model.

Example
+++++++

Table
  ``examples``
Model class
  ``Example``

This table provides details of example searches used in the Web site
interface.

Info
++++

Table
  ``info``
Model class
  ``Info``

This table provides version information for the data shown in the Web site,
plus knowledge of previous versions of the data. The ``key`` column indicates
the nature of a particular piece of information stored in the ``value``
column. Currently, the following keys are employed:

=================== ==========================================================
Key                 Description
=================== ==========================================================
``data_tag``        A precise version indicator for data stored in a schema
``data_version``    A version indicator corresponding to a known release
=================== ==========================================================

The latest value corresponding to each of these keys indicates the details of
the data stored in the schema of a particular database. Earlier releases of
the data can be obtained by retrieving other ``data_version`` values and then
directing the user to a database (provided by a site) for which that version
is the latest known value.

An approach combining the ``data_tag`` and ``data_version`` in each record can
be envisaged. This would allow the representation of relationships between
tags and versions and potentially allow navigation of precise data versions
within releases. However, a workable approach involving a general key-value
table was chosen with the possibility of using this table for other
miscellaneous details that might need to be conveniently stored in the
database.
