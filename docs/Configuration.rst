Configuration
=============

Some additional configuration files may need to be defined in the deployment_
environment for certain functionality.

.. _deployment: Deployment.rst

Mail Sending
------------

The enrichment job initiation and monitoring program may be configured to send
e-mail messages when jobs are initiated and completed. The module supporting
this functionality will attempt to read from a file situated in the base
directory of this software distribution called ``smtp_settings.txt``. This
file should contain definitions in JSON format resembling the following:

.. code:: json

   {"host" : "smtp.uio.no",
    "user" : "unibindmail",
    "password" : "<password>",
    "port" : 465,
    "ssl" : true}

Here, the ``password`` would need to be set appropriately for the indicated
user recognised by the given SMTP host or server.

Settings Usage
--------------

Upon deploying data, the WSGI handler files may need to be adjusted to employ
settings_ corresponding to the "tags" employed by the available data.
Moreover, the generic ``unibind/settings.py`` file may need to have the
default database name updated to reflect the most recent tag.

.. _settings: Settings.rst
