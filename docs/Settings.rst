Settings
========

The Web application's core settings are found in the ``unibind/settings.py``
file. In order to support different sites providing access to different
versions of UniBind data, all within the same repository of program code and
associated resources, specific settings files import the core settings and
indicate different database details:

=================================== ====================================
File                                Description
=================================== ====================================
``unibind/settings20190717.py``     UniBind 2018 data settings
``unibind/settings20210421.py``     UniBind 2021 data settings
=================================== ====================================

The naming of these files is not significant, although it is helpful to name
them in a descriptive way, indicating which database each of them references.

These settings files are employed by the WSGI handler programs as follows:

=================================== ====================================
File                                Description
=================================== ====================================
``unibind/wsgi.py``                 Handler for ``unibind.uio.no``
``unibind/wsgi2018.py``             Handler for ``unibind2018.uio.no``
``unibind/wsgi2021.py``             Handler for ``unibind2021.uio.no``
=================================== ====================================

Here, a convention is employed where the file naming reflects the site being
provided by the handler. This convention is adopted by the deployment scripts
to generate the site configuration files for the Web server.

The following diagram summarises the relationships between these files, the
sites and the databases.

.. image:: Settings.svg


Versions and Version Tags
-------------------------

To distinguish between general releases and precise versions of the data, a
version such as ``2018`` or ``2021`` is used to denote a general UniBind
release that is then published as a versioned Web site. More precise versions
of the data are denoted by a tag such as ``20190717`` or ``20210421``. These
tags indicate a more specific collection of data that supports a given
release, where further revision of the data published as part of a release may
lead to a new tag being adopted.

Currently, each dataset incorporates the appropriate version and tag details
together with any preceding details in order to allow navigation to earlier
releases of the data. The `Info`_ table in the database schema is consulted to
obtain the latest details, using them to parameterise the published site and
the construction of filesystem paths for the retrieval of stored data.

.. _`Info`: Schema.html#info


Versioned Data Resources
------------------------

Since different versioned collections of resources coexist within a single
directory hierarchy, a site providing data corresponding to the tag
``20210421`` will need to construct paths to resources incorporating this tag.
For example:

::

  static/data/20210421/macs/DAMO/ENCSR000AHD.MCF7_Invasive_ductal_breast_carcinoma.CTCF

Here, the ``static/data/20210421`` directory contains resources for datasets
belonging to that particular versioned collection. In contrast, for the tag
``20190717``, the following path would be generated for the corresponding
dataset from this older version of the data:

::

  static/data/20190717/macs/PWM/ENCSR000AHD.MCF7.CTCF
