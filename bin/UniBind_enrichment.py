#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Run another program in the appropriate environment for a given enrichment job.
# This takes the command for the program invocation from the job database, with
# either a specific job number being indicated or "first" indicating the first
# queued job. A limited number of jobs may be active at once, as indicated by
# the following setting:

job_limit_default = 1

# Application settings default value.

settings_default = "unibind.settings"



# Set up the module search path referencing this project's top-level directory
# plus library directories in the lib directory set up after deploying the
# software.

from os.path import abspath, join, split
import sys

this_dir, progname = split(sys.argv[0])
base_dir = abspath(split(this_dir)[0])
python_lib = "python%s.%s" % (sys.version_info.major, sys.version_info.minor)

for lib in ["lib", "lib64"]:
    sys.path.insert(0, join(base_dir, "lib", "usr", "local", lib, python_lib, "site-packages"))

sys.path.insert(0, base_dir)

# Handle Django initialisation issues when run separately.

import django.core.exceptions
import os

# Override the environment variable to access different databases.

settings = (sys.argv[1:] or [None])[0]

if settings or not os.environ.get("DJANGO_SETTINGS_MODULE"):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", settings or settings_default)

# Test whether Django can start, invoking setup if not.

try:
    import portal.models
except django.core.exceptions.AppRegistryNotReady:
    import django
    django.setup(set_prefix=False)



# Actual imports.

from portal.models import EnrichmentJob
from unibind.notify import send_email_notification
from unibind.settings import TEMP_DIR
import shlex

# Support invocation as a program.

if __name__ == "__main__":
    import subprocess

    try:
        job_id = sys.argv[2]
    except IndexError:
        print("Usage: %s <settings identifier> <job selector> <job limit>" % progname, file=sys.stderr)
        sys.exit(1)

    try:
        job_limit = int(sys.argv[3])
    except (IndexError, ValueError):
        job_limit = job_limit_default

    # Take the first queued job and its command from the database.

    if job_id == "first":

        # Test running jobs and only run another if the limit is not reached.

        running = EnrichmentJob.objects.filter(job_status="Running").count()
        if running >= job_limit:
            print("Not running a new job until existing jobs are finished.", file=sys.stderr)
            sys.exit(0)

        job = EnrichmentJob.objects.filter(job_status="Queued").order_by("created_at").first()
        if not job:
            print("No queued enrichment jobs.", file=sys.stderr)
            sys.exit(0)

    # Otherwise, process the indicated job, taking any command from the program
    # arguments.

    else:
        try:
            job = EnrichmentJob.objects.get(id=job_id)
        except EnrichmentJob.DoesNotExist:
            print("Enrichment job %s not found." % job_id, file=sys.stderr)
            sys.exit(1)

    cmd = shlex.split(job.parameter)

    # Invoke any indicated or retrieved command, setting the job state before
    # and after, also notifying the submitter.

    job.job_status = "Running"
    job.save()

    # Invoke the command with standard error redirection to a file.

    stderr_log = join(TEMP_DIR, job.slug, "unibind_stderr_log.txt")

    with open(stderr_log, "w+") as f:
        if cmd:
            proc = subprocess.Popen(cmd, stderr=f, universal_newlines=True)
            status = proc.wait()
        else:
            print("Missing command for job!", file=f)
            status = 1

    if not status:
        job.job_status = "Completed"
    else:
        job.job_status = "Stopped"

    job.save()

    # Notify the submitter.

    send_email_notification(job.email, job.job_status, job.job_url)

# vim: tabstop=4 expandtab shiftwidth=4
