# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import defaultdict
from io import BytesIO
from rest_framework import renderers
from os import remove
from os.path import join, split
from portal.models import Info
from unibind.settings import BASE_DIR, TEMP_DIR
from unibind.utils import get_data_source

import tarfile, tempfile, time



# Result rendering.

class CompleteRenderer(renderers.BaseRenderer):
    '''
    Support rendering without pagination.
    '''
    def render(self, data, media_type=None, renderer_context=None):
        '''
        Render the data as tab-separated fields, discarding pagination artefacts
        to render the results.
        '''

        if 'count' in data and 'results' in data:
            return self._render(data['results'], renderer_context)
        else:
            return self._render(data, renderer_context)

    def _set_filename(self, filename, renderer_context):
        '''
        Set a filename on the response, if possible.
        '''

        response = renderer_context and renderer_context.get('response')
        if response:
            response['Content-Disposition'] = 'attachment; filename="%s"' % filename

class ArchivingRenderer(CompleteRenderer):
    '''
    A renderer that archives results and produces archive file data for those
    results. All records are archived and the response can be rather large.

    Unfortunately, streaming is not readily supported by renderers, and so the
    output is written to a buffer and then read back. Ideally, it would be
    possible to either write the output directly to the response or to provide
    an iterator that accesses the output incrementally.
    '''

    media_type = 'application/x-gtar'
    format = 'tgz'

    def __init__(self):

        # Obtain the latest data version tag.

        self.data_tag = Info.objects.filter(key='data_tag').order_by('-value').first().value
        self.data_dir = join(BASE_DIR, 'static', 'data', self.data_tag)

    def get_filename(self):
        '''
        Generate a filename purely for the response.
        '''

        return tempfile.mktemp(suffix='.tar.gz',
                               prefix='UniBind_search_',
                               dir='')

    def _render(self, data, renderer_context):
        '''
        Obtain filename references in records, adding each referenced file to
        the archive.
        '''

        # Obtain specific file type details, if available.

        request = renderer_context and renderer_context.get('request')
        source = get_data_source(request)
        filetypes = source and source.getlist('filetype') or None

        # Set a filename on the response, if possible.

        self._set_filename(split(self.get_filename())[-1], renderer_context)

        # Add referenced files to an archive.

        out_buffer = BytesIO()

        with tarfile.open(fileobj=out_buffer, mode="w|gz", encoding="utf-8") as tf:

            # Produce a table of serialised object details.

            metadata = "".join(get_records_as_table(data)).encode("utf-8")
            tarinfo = tarfile.TarInfo("metadata.tsv")
            tarinfo.size = len(metadata)
            tarinfo.mtime = time.time()

            tf.addfile(tarinfo, BytesIO(metadata))

            # Write the archive content after the first member.

            yield out_buffer.getvalue()

            # Start filling the buffer from the beginning.

            out_buffer.truncate(0)
            out_buffer.seek(0)

            # Inspect each serialised object and generate file content.

            for obj in data:
                current, keys = get_fields(obj)

                # Find filename references, adding files relative to the data
                # directory.

                for key in keys:
                    if key.endswith("_filename"):

                        # Determine the nature of the referenced file and
                        # select files of the requested types.

                        filetype = key[:-len("_filename")]

                        if not filetypes or filetype in filetypes:
                            for filename in current[key]:

                                # Employ the leafname of each filename within the
                                # archive.

                                arcname = split(filename)[-1]

                                # Need to encode the filename and set the encoding.
                                # Despite Python 3 supposedly "handling Unicode",
                                # without a byte-encoded filename, there is a call
                                # to os.lstat that fails with UnicodeEncodeError.

                                pathname = join(self.data_dir, filename).encode("utf-8")

                                tf.add(pathname, arcname)

                # Write the archive content after each record.

                yield out_buffer.getvalue()

                # Start filling the buffer from the beginning.

                out_buffer.truncate(0)
                out_buffer.seek(0)

        # Write any remaining data produced during archive closure.

        yield out_buffer.getvalue()

class TabSeparatedRenderer(CompleteRenderer):
    '''
    Return records in tab-separated values format. Since tab-separated files are
    only meant to hold data (and column metadata), not additional metadata,
    pagination is not supported: all records are returned.
    '''
    media_type = 'text/tab-separated-values'
    format = 'tsv'

    def get_filename(self):
        '''
        Generate a filename purely for the response.
        '''

        return tempfile.mktemp(suffix='.tsv',
                               prefix='UniBind_search_',
                               dir='')

    def _render(self, data, renderer_context):
        '''
        Render the data as tab-separated fields.
        '''

        # Set a filename on the response, if possible.

        self._set_filename(split(self.get_filename())[-1], renderer_context)

        # Get a list of tab-separated record strings.

        return get_records_as_table(data)



# Serialised object processing and rendering.

def get_fields(obj, current=None, keys=None):
    '''
    Collect fields from the given object, adding them to any supplied
    current record mapping and noting the keys in order of appearance.

    The resulting mapping should only contain keys providing simple values, not
    objects, but since the serialisation of objects involves lists of objects,
    the structure is traversed until simple values are found and then their keys
    are used to populate this mapping.
    
    One complication arises where an empty list appears. Such a list might be a
    list of objects whose key would not be desired in this mapping. However, an
    empty list of simple values would be desired in this mapping to ensure that
    any tabular data retains its structure.

    Consequently, None is used as a special value by serialisers to indicate
    empty value lists, with empty lists indicating empty collections of objects.
    '''

    if current is None:
        current = defaultdict(list)
    if keys is None:
        keys = []

    for key, value in obj.items():

        # Descend into structures to obtain values.

        if hasattr(value, 'items'):
            get_fields(value, current, keys)

        # Handle lists of objects or values.

        elif isinstance(value, (list, tuple)):
            for v in value:
                if hasattr(v, 'items'):
                    get_fields(v, current, keys)
                else:
                    get_field_value(current, key, v, keys)

        # Add values to the current mapping, noting the key involved.

        else:
            get_field_value(current, key, value, keys)

    return current, keys

def get_field_value(current, key, value, keys):
    '''
    Add values to the current mapping, noting the key involved.
    Here, None represents an empty list of values that should be
    reproduced in the output.
    '''

    if value is not None:
        current[key].append(str(value))
    if key not in keys:
        keys.append(key)

def get_records_as_table(data):
    '''
    Process objects in the given data, returning a list of tab-separated
    strings, one per record.
    '''

    # Collect records and establish which columns/fields are involved.

    records = []
    keys = None

    # For unpaginated data or paginated results, collect values.

    for obj in data:
        current, _keys = get_fields(obj)

        # Use a consistent field order between records, although the
        # serialiser will most likely achieve this normally.
        # Write column headings.

        if keys is None:
            keys = _keys
            records.append('\t'.join(keys) + '\n')

        # Traverse the keys in order of discovery. Multiple values are
        # pipe-separated.

        record = []
        for key in keys:
            values = current[key]

            # Special case: preserve only basenames.

            if key.endswith("_filename"):
                values = [split(value)[-1] for value in values]

            record.append('|'.join(values))

        # Tab-separate the fields and add a newline.

        records.append('\t'.join(record) + '\n')

    return records

# vim: tabstop=4 expandtab shiftwidth=4
