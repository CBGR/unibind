# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from rest_framework import serializers
from portal.models import Cell, Factor, FactorData, FactorRef, \
                          FactorSample, Gene, Profile, Species, Synonym

from .linking import *



# Model/table instance serialisation.

class CellSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Cell
        fields = ('cell_id', 'title')

class FactorSerializer(serializers.HyperlinkedModelSerializer):

    url = serializers.SerializerMethodField()

    class Meta:
        model = Factor
        fields = ('tf_name', 'total_peaks', 'url')
        ordering = ['tf_name', 'total_peaks']

    def get_url(self, obj):
        request = self.context.get('request')
        return request and get_folder_url(request, obj.folder)

class FactorDataSerializer(serializers.HyperlinkedModelSerializer):

    jaspar_id = serializers.SerializerMethodField()

    class Meta:
        model = FactorData
        fields = ('folder', 'peak_caller', 'distance_threshold',
                  'score_threshold', 'adj_centrimo_pvalue', 'total_tfbs',
                  'jaspar_id')

    def get_jaspar_id(self, obj):
        return '%s.%s' % (obj.jaspar_id, obj.jaspar_version)

class FactorRefSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = FactorRef
        fields = ('ref_db', 'ref_id')

class FactorSampleSerializer(serializers.HyperlinkedModelSerializer):

    cell = serializers.SerializerMethodField()

    class Meta:
        model = FactorSample
        fields = ('biological_condition', 'cell')

    def get_cell(self, obj):
        return obj.cell.cell_id



# Factor summary serialisation for output resembling the Web interface search
# results.

class CellSummarySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Cell
        fields = ('title',)

class SpeciesSummarySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Species
        fields = ('name',)

class FactorDataSummarySerializer(serializers.HyperlinkedModelSerializer):

    bed_filename = serializers.SerializerMethodField()
    fasta_filename = serializers.SerializerMethodField()
    jaspar_id = serializers.SerializerMethodField()
    profile_name = serializers.SerializerMethodField()
    synonyms = serializers.SerializerMethodField()

    class Meta:
        model = FactorData
        fields = ('jaspar_id', 'bed_filename', 'fasta_filename', 'profile_name', 'synonyms')

    def get_jaspar_id(self, obj):
        return '%s.%s' % (obj.jaspar_id, obj.jaspar_version)

    def get_bed_filename(self, obj):
        return self.get_filename(obj, 'bed')

    def get_fasta_filename(self, obj):
        return self.get_filename(obj, 'fa')

    def get_filename(self, obj, suffix):
        return get_filename(obj.peak_caller, obj.prediction_model, obj.model_detail,
                            obj.folder.folder, obj.jaspar_id, obj.jaspar_version,
                            suffix)

    def get_profile_name(self, obj):
        return obj.profile.name

    def get_synonyms(self, obj):
        synonyms = []

        # Obtain gene-related synonyms.

        for gene in obj.profile.genes.all():
            for synonym in gene.synonyms.all():
                synonyms.append(synonym.synonym)

        # Return None where no synonyms were found. This is interpreted
        # specially when data is serialised as a table.

        synonyms.sort()

        return synonyms or None

class FactorRefSummarySerializer(serializers.HyperlinkedModelSerializer):

    identifier = serializers.SerializerMethodField()

    class Meta:
        model = FactorRef
        fields = ('identifier',)

    def get_identifier(self, obj):
        return '%s:%s' % (obj.ref_db, obj.ref_id)

class FactorSampleSummarySerializer(serializers.HyperlinkedModelSerializer):

    biological_condition = serializers.SerializerMethodField()
    cell = CellSummarySerializer()

    class Meta:
        model = FactorSample
        fields = ('cell', 'biological_condition')

    def get_biological_condition(self, obj):
        return obj.biological_condition or ''

class SpecificFactorSummarySerializer(serializers.HyperlinkedModelSerializer):

    samples = FactorSampleSummarySerializer(many=True)
    references = FactorRefSummarySerializer(many=True)
    species = SpeciesSummarySerializer()

    class Meta:
        model = Factor
        fields = ('folder', 'tf_name', 'samples', 'references', 'species')

class SpecificFactorDataSummarySerializer(FactorDataSummarySerializer):

    folder = SpecificFactorSummarySerializer()

    class Meta:
        model = FactorData
        fields = ('folder', 'jaspar_id', 'bed_filename', 'fasta_filename', 'profile_name', 'synonyms')

class FactorSummarySerializer(SpecificFactorSummarySerializer):

    datasets = FactorDataSummarySerializer(many=True)

    class Meta:
        model = Factor
        fields = ('folder', 'tf_name', 'samples', 'references', 'species', 'datasets')
