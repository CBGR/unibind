# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from os.path import join
from urllib.parse import quote

# URL generation.

def get_api_root_url(request):
    return request.build_absolute_uri(location='/api/v1/')

def get_host_name(request):
    return request.build_absolute_uri(location='/')

# Resource-specific URL generation.

def get_cellline_url(request, cell_line):
    path = '/api/v1/datasets'
    return request.build_absolute_uri(location=path) + '?cell_line=%s' % quote(cell_line)

def get_collection_url(request, collection):
    path = '/api/v1/datasets'
    return request.build_absolute_uri(location=path) + '?collection=%s' % quote(collection)

def get_folder_url(request, folder):
    path = '/api/v1/datasets/%s/' % folder
    return request.build_absolute_uri(location=path)

def get_tfbs_url(request, data_tag, factor_id, model_name, model_detail, jaspar_id, jaspar_version, file_extension):
    model_detail = model_detail and '%s%s' % (model_name, model_detail) or model_name

    if model_name != 'DNAshaped':
        model_detail = model_detail.lower()

    path = '/static/data/%s/macs/%s/%s/%s.%s.%s.%s.%s' % (
        data_tag, model_name, factor_id, factor_id, jaspar_id, jaspar_version,
        model_detail, file_extension)
    return request.build_absolute_uri(location=path)

def get_species_url(request, name):
    path = '/api/v1/datasets'
    return request.build_absolute_uri(location=path) + '?species=%s' % quote(name)

def get_tf_url(request, tf_name):
    path = '/api/v1/datasets'
    return request.build_absolute_uri(location=path) + '?tf_name=%s' % quote(tf_name)

# Filename generation.

def get_filename(peak_caller, prediction_model, model_detail, folder, jaspar_id, jaspar_version, suffix):

    # Special cases apply here.

    if prediction_model != 'DNAshaped':
        prediction_model_suffix = prediction_model.lower()
        model_detail_suffix = ''
    else:
        prediction_model_suffix = prediction_model
        model_detail_suffix = model_detail

    return join(peak_caller.lower(), prediction_model, folder,
                '%s.%s.%s.%s%s.%s' % (folder, jaspar_id, jaspar_version,
                                      prediction_model_suffix, model_detail_suffix, suffix))
