# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.db.models import Q, QuerySet
from django.http import Http404, StreamingHttpResponse
from django.shortcuts import render

from rest_framework import renderers
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.throttling import UserRateThrottle
from rest_framework.views import APIView

from rest_framework_jsonp.renderers import JSONPRenderer
from rest_framework_yaml.renderers import YAMLRenderer
from rest_framework_yaml.parsers import YAMLParser

from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
    BaseFilterBackend,
    )
from rest_framework.generics import (
    ListAPIView,
    )
from rest_framework.pagination import (
    PageNumberPagination,
    )

# Live API imports.

from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer
from rest_framework.decorators import api_view, renderer_classes
from rest_framework import response, schemas

# Portal and common imports.

from portal.models import Cell, Collection, Factor, FactorData, Info, Species
from portal.views import _biological_conditions, _cell_line_titles, _profile_ids, _source_ids
from unibind.search import get_schema_fields, get_search_queryset
from unibind.utils import get_data_source

# REST API imports.

from .linking import *
from .renderers import ArchivingRenderer, TabSeparatedRenderer
from .serializers import FactorSerializer, FactorSummarySerializer, \
                         FactorDataSerializer, SpecificFactorDataSummarySerializer

# YAML rendering fixes.

import yaml.representer

YAMLRenderer.encoder.add_representer(QuerySet, yaml.representer.SafeRepresenter.represent_list)
YAMLRenderer.encoder.add_representer(map, yaml.representer.SafeRepresenter.represent_list)

# Renderer customisations.
# Since dedicated templates are used, the renderers are overridden to reference
# them.

class BrowsableAPIRenderer(renderers.BrowsableAPIRenderer):
    template = 'restapi/api.html'

class LiveAPIRenderer(SwaggerUIRenderer):
    template = 'restapi/api_live.html'



# Pagination.

class FactorResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 500



# Filter classes.

class UniBindFilterBackend(BaseFilterBackend):
    """
    General filtering for endpoints.
    """

    def get_schema_fields(self, view):
        return get_schema_fields()

    def filter_queryset(self, request, queryset, view):
        source = get_data_source(request)
        queryset = get_search_queryset(source, queryset)
        return queryset.order_by('tf_name')

class UniBindFactorDataFilterBackend(BaseFilterBackend):
    """
    General filtering for endpoints.
    """

    def get_schema_fields(self, view):
        return get_schema_fields()

    def filter_queryset(self, request, queryset, view):
        source = get_data_source(request)
        queryset = get_search_queryset(source, queryset).order_by('tf_name')
        return FactorData.objects.filter(folder__in=queryset)

# View classes.

class DatasetDetailsViewSet(APIView):
    """
    API endpoint that returns the tfbs detail information.
    """

    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    # Fields from FactorData instances for TFBS results.

    tfbs_fields = [
        'adj_centrimo_pvalue', 'distance_threshold', 'jaspar_id',
        'jaspar_version', 'score_threshold', 'total_tfbs',
        'model_detail',
        ]

    # URL fields for TFBS results: keys and corresponding data types.

    tfbs_url_fields = [
        ("bed_url", "bed"), ("fasta_url", "fa"), ("summary_plot_url", "png")
        ]

    def get(self, request, tf_id, format=None):
        """
        Return TFBS detail (dataset) information.
        """

        setattr(request, 'view', 'api-browsable')

        try:
            factor = Factor.objects.get(folder=tf_id)
        except Factor.DoesNotExist:
            raise Http404

        # Obtain the latest data version tag.

        data_tag = Info.objects.filter(key='data_tag').order_by('-value').first().value

        data_for_folder = FactorData.objects.filter(folder=tf_id)
        prediction_models = data_for_folder.values_list('prediction_model', flat=True).distinct()

        all_tfbs = []
        for model_name in prediction_models:
            model_tfbs = []

            # Select records for the given model from the selected dataset.
            # Dictionaries containing a selection of fields are obtained.

            for tfbs in data_for_folder.filter(prediction_model=model_name) \
                .values(*self.tfbs_fields):

                # Augment the stored data with URLs appropriate for this
                # deployment of the software.

                for key, datatype in self.tfbs_url_fields:
                    tfbs[key] = get_tfbs_url(request, data_tag, tf_id, model_name,
                        tfbs['model_detail'],
                        tfbs['jaspar_id'], tfbs['jaspar_version'], datatype)

                model_tfbs.append(tfbs)

            all_tfbs.append({model_name: model_tfbs})

        data_dict = {
            'biological_condition': _biological_conditions(factor.samples),
            'cell_line': _cell_line_titles(factor.samples),
            'identifier': _source_ids(factor.references),
            'jaspar_id': _profile_ids(factor.datasets),
            'prediction_models': prediction_models,
            'tf_id': factor.folder,
            'tf_name': factor.tf_name,
            'tfbs': all_tfbs,
            'total_peaks': factor.total_peaks,
            }

        return Response(data_dict)

class TranscriptionFactorsListViewSet(APIView):
    """
    API endpoint that returns a list of all transcription factors.
    """

    throttle_classes = (UserRateThrottle,)
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request, format=None):
        """
        List all the transcription factors available in UniBind.
        """

        setattr(self.request, 'view', 'api-browsable')

        queryset = Factor.objects.values_list('tf_name', flat=True).distinct()

        results = []
        for tf_name in queryset:
            results.append({
                'tf_name': tf_name,
                'url': get_tf_url(request, tf_name)
                })

        data_dict = {
            'count': queryset.count(),
            'results': results
            }

        return Response(data_dict)

class DatasetsListViewSet(ListAPIView):
    """
    REST API endpoint that returns a list of all the datasets used.
    """

    pagination_class = FactorResultsSetPagination
    throttle_classes = (UserRateThrottle,)
    filter_backends = [SearchFilter, OrderingFilter, UniBindFilterBackend]
    parser_classes = [FormParser, MultiPartParser, YAMLParser]
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, TabSeparatedRenderer,
                        ArchivingRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def post(self, request, *args, **kwargs):
        '''
        Support POST requests for search result downloads. This requires the
        FormParser and MultiPartParser classes to be enabled.
        '''
        return self.list(request, *args, **kwargs)

    def list(self, request, *args, **kwargs):
        '''
        From rest_framework.mixins.ListModelMixin, but with the streaming
        response.
        '''

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        # Customise the response, also setting a filename.

        renderer = request.accepted_renderer

        # Use the presence of get_filename to determine whether the streaming
        # response can be introduced, this being provided by custom renderers.

        if not hasattr(renderer, 'get_filename'):
            return Response(serializer.data)

        filename = renderer.get_filename()

        output = renderer.render(serializer.data, renderer_context={
            'request' : request,
            })

        response = StreamingHttpResponse(output, content_type=renderer.media_type)
        response['Content-Disposition'] = 'attachment; filename="%s"' % filename
        return response

    def get_queryset(self):
        """
        List all datasets.
        """

        setattr(self.request, 'view', 'api-browsable')
        return Factor.objects.all().order_by('tf_name')

    def get_serializer_class(self):
        """
        Return the serialiser depending on the query parameters, selecting a
        summary serialiser if requested.
        """

        source = get_data_source(self.request)

        if source.get('summary') not in ('false', None):
            return FactorSummarySerializer
        else:
            return FactorSerializer

    def paginate_queryset(self, queryset):
        """
        Override pagination if requested.
        """

        source = get_data_source(self.request)

        if source.get('all') not in ('false', None):
            return None
        else:
            return ListAPIView.paginate_queryset(self, queryset)

class SpecificDatasetsListViewSet(DatasetsListViewSet):
    """
    REST API endpoint that returns a list of profile-specific datasets.
    """

    filter_backends = [SearchFilter, OrderingFilter, UniBindFactorDataFilterBackend]

    def get_serializer_class(self):
        """
        Return the serialiser depending on the query parameters, selecting a
        summary serialiser if requested.
        """

        source = get_data_source(self.request)

        if source.get('summary') not in ('false', None):
            return SpecificFactorDataSummarySerializer
        else:
            return FactorDataSerializer

class CellTypesListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all cell-lines/tissue names.
    """

    filter_backends = [SearchFilter,]
    throttle_classes = (UserRateThrottle,)
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request):
        """
        List all the cell-lines/tissue names available in UniBind.
        """

        setattr(self.request, 'view', 'api-browsable')

        # Filter the results using the generic search parameter as a cell line
        # substring.

        source = get_data_source(request)
        get = source.get
        query_string = get('search', None)

        queryset = Cell.objects.all()

        if query_string:
            queryset = queryset.filter(title__icontains=query_string)

        # Order and flatten the query results for annotation.

        queryset = queryset.order_by('title')

        results = []
        for cell in queryset:
            results.append({
                'name': cell.title,
                'url': get_cellline_url(self.request, cell.title)
                })

        data_dict = {
            'count': queryset.count(),
            'results': results
            }

        return Response(data_dict)

class CollectionsListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all collections.
    """

    filter_backends = [SearchFilter,]
    throttle_classes = (UserRateThrottle,)
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request):
        """
        List all the collections available in UniBind.
        """

        setattr(self.request, 'view', 'api-browsable')

        # Filter the results using the generic search parameter as a cell line
        # substring.

        source = get_data_source(request)
        get = source.get
        query_string = get('search', None)

        queryset = Collection.objects.all()

        if query_string:
            queryset = queryset.filter(title__icontains=query_string)

        # Order and flatten the query results for annotation.

        queryset = queryset.order_by('name')

        results = []
        for collection in queryset:
            results.append({
                'name': collection.name,
                'url': get_collection_url(self.request, collection.name)
                })

        data_dict = {
            'count': queryset.count(),
            'results': results
            }

        return Response(data_dict)

class SpeciesListViewSet(ListAPIView):
    """
    API endpoint that returns a list of all species names.
    """

    filter_backends = [SearchFilter,]
    throttle_classes = (UserRateThrottle,)
    parser_classes = (YAMLParser,)
    renderer_classes = [renderers.JSONRenderer, JSONPRenderer, YAMLRenderer, BrowsableAPIRenderer]

    def get(self, request):
        """
        List all the species names available in UniBind.
        """

        setattr(self.request, 'view', 'api-browsable')

        # Filter the results using the generic search parameter as a name
        # substring.

        source = get_data_source(request)
        get = source.get
        query_string = get('search', None)

        queryset = Species.objects.filter(tax_id__in=Factor.objects.values('species').distinct())

        if query_string:
            queryset = queryset.filter(name__icontains=query_string)

        # Order and flatten the query results for annotation.

        queryset = queryset.order_by('name').values_list('name', flat=True)

        results = []
        for name in queryset:
            results.append({
                'name': name,
                'url': get_species_url(self.request, name)
                })

        data_dict = {
            'count': queryset.count(),
            'results': results
            }

        return Response(data_dict)

class APIRoot(APIView):
    """
    This is the root of the UniBind RESTful API v1. Please read the documentation for more details.
    """
    permission_classes = (AllowAny,)

    def get(self, request, format=format):
        setattr(request, 'view', 'api-browsable')
        return Response({
            'tfs': reverse('v1:tf-list', request=request),
            'datasets': reverse('v1:dataset-list', request=request),
            'specific': reverse('v1:specific-list', request=request),
            'celltypes': reverse('v1:cellline-list', request=request),
            })

def api_homepage(request):

    setattr(request, 'view', 'api-home')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_home.html')

def api_docs(request):

    setattr(request, 'view', 'apidocs')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_docs.html')

def api_contactus(request):

    setattr(request, 'view', 'contactus')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_contact.html')

def api_overview(request):

    setattr(request, 'view', 'overview')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_overview.html')

def api_clients(request):

    setattr(request, 'view', 'clients')
    setattr(request, 'get_api_host', get_api_root_url(request))
    setattr(request, 'get_host', get_host_name(request))

    return render(request, 'restapi/api_clients.html')

# Live API customisations.
# This is needed to permit overriding of both the renderer classes and the URL
# patterns, which the get_swagger_view function does not permit.

def api_live(patterns):
    class LiveAPIView(APIView):
        permission_classes = [AllowAny]
        renderer_classes = [OpenAPIRenderer, LiveAPIRenderer]

        def get(self, request):
            setattr(request, 'view', 'apilive')
            setattr(request, 'get_api_host', get_api_root_url(request))
            setattr(request, 'get_host', get_host_name(request))

            generator = schemas.SchemaGenerator(title='UniBind Live API',
                                                patterns=patterns)
            return response.Response(generator.get_schema(request=request))

    return LiveAPIView.as_view()

# vim: tabstop=4 expandtab shiftwidth=4
