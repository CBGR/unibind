# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from django.views.decorators.cache import cache_page

from . import views

# Cache timeout in seconds.

CACHE_TIMEOUT = 60 * 60 * 24 * 1

def view(obj):
    return cache_page(CACHE_TIMEOUT)(obj.as_view())

# Application details and structure.

app_name = "restapi.v1"

# Resources exposed by the live API.

urlpatterns_api = [
    url(r'^v1/celltypes/?$', view(views.CellTypesListViewSet), name='cellline-list'),
    url(r'^v1/collections/?$', view(views.CollectionsListViewSet), name='collection-list'),
    url(r'^v1/datasets/?$', view(views.DatasetsListViewSet), name='dataset-list'),
    url(r'^v1/datasets/(?P<tf_id>.+)/$', view(views.DatasetDetailsViewSet), name='dataset-detail'),
    url(r'^v1/specific/?$', view(views.SpecificDatasetsListViewSet), name='specific-list'),
    url(r'^v1/species/?$', view(views.SpeciesListViewSet), name='species-list'),
    url(r'^v1/tfs/?$', view(views.TranscriptionFactorsListViewSet), name='tf-list'),
    ]

urlpatterns_api = format_suffix_patterns(urlpatterns_api, allowed=['bed','json','jsonp','tgz','tsv','yaml','api'])

# Prefix the API resources to configure the live API properly.

urlpatterns_api_prefixed = [url("^api/", include(urlpatterns_api))]

# All resources.

urlpatterns = urlpatterns_api + [
    url(r'^clients/?$', views.api_clients, name='api-clients'),
    url(r'^contact-us/?$', views.api_contactus, name='api-contactus'),
    url(r'^home/?$', views.api_homepage, name='api-homepage'),
    url(r'^overview/?$', views.api_overview, name='api-overview'),
    url(r'^v1/?$', views.api_homepage, name='api-homepage'),
    url(r'^v1/live-api/?$', views.api_live(urlpatterns_api_prefixed), name='api-live'),
    ]
