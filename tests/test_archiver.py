#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest import main, TestCase
from os import chdir, getcwd, makedirs
from os.path import abspath, join, split
from shutil import rmtree
from tempfile import mkdtemp
import sys

# Access the distribution's local packages.

thisdir = split(abspath(__file__))[0]
sys.path.append(split(thisdir)[0])

from unibind.utils import Archiver

class ArchivingTest(TestCase):

    factor = "ENCSR000BSA.HCT166.JUND"

    def _get_filenames(self):

        "Return a collection of filenames for testing."

        filenames = []
        for suffix in [".pwm", ".fa", ".png", ".bed"]:
            filenames.append("%s%s" % (self.factor, suffix))
        return filenames

    def setUp(self):

        "Set up temporary directories and files."

        self.curdir = getcwd()

        self.output_dir = abspath(mkdtemp(prefix="output"))
        self.data_dir = abspath(mkdtemp(prefix="data"))

        self.model_dir = join(self.data_dir, 'static', 'data', 'macs', 'PWM')
        self.model_dir_relative = join('static', 'data', 'macs', 'PWM')
        self.record_dir = join(self.model_dir, self.factor)
        self.record_dir_relative = join(self.model_dir_relative, self.factor)

        makedirs(self.record_dir)

        for filename in self._get_filenames():
            with open(join(self.record_dir, filename), "w") as f:
                f.write("Some content.")

    def tearDown(self):

        "Remove temporary directories and files."

        rmtree(self.data_dir)
        rmtree(self.output_dir)

    def test_flat_absolute(self):

        "Test archive creation using absolute paths with no internal directory."

        target_path = self.record_dir
        tar_file_path = join(self.output_dir, 'flat_absolute.tar.gz')
        Archiver(tar_file_path, target_path, [".bed", ".fa", ".png"]).archive()

    def test_tidy_absolute(self):

        "Test archive creation using absolute paths and an internal directory."

        target_path = self.model_dir
        tar_file_path = join(self.output_dir, 'tidy_absolute.tar.gz')
        Archiver(tar_file_path, target_path, [".bed", ".fa", ".png"]).archive('ENCSR000BSA.HCT166.JUND')

    def test_flat_relative(self):

        "Test archive creation using relative paths with no internal directory."

        chdir(self.data_dir)
        target_path = self.record_dir_relative
        tar_file_path = join(self.output_dir, 'flat_relative.tar.gz')
        Archiver(tar_file_path, target_path, [".bed", ".fa", ".png"]).archive()
        chdir(self.curdir)

    def test_tidy_relative(self):

        "Test archive creation using relative paths and an internal directory."

        chdir(self.data_dir)
        target_path = self.model_dir_relative
        tar_file_path = join(self.output_dir, 'tidy_relative.tar.gz')
        Archiver(tar_file_path, target_path, [".bed", ".fa", ".png"]).archive('ENCSR000BSA.HCT166.JUND')
        chdir(self.curdir)

if __name__ == "__main__":
    main()

# vim: tabstop=4 expandtab shiftwidth=4
