#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest import main, TestCase
import requests, sys

# API access definitions.

base_url = "http://localhost:8000/api/v1"
json_format = "application/json"

# TFBS detail dictionary keys with optional/unpopulated keys commented out.

tfbs_keys = [
    #'biological_condition',
    'cell_line',
    'identifier',
    'jaspar_id',
    'prediction_models',
    'tf_id',
    'tf_name',
    'total_peaks',
    ]

model_tfbs_keys = [
    'adj_centrimo_pvalue',
    'distance_threshold',
    'jaspar_id',
    'jaspar_version',
    #'model_detail',
    'score_threshold',
    'total_tfbs',
    ]

# Utility functions.

def GET(url, params=None, format=json_format):
    r = requests.get(url, params=params, headers={"Accept" : format})
    if format == json_format:
        return r.json()
    else:
        return r.text

def get_dataset(folder, format=json_format):
    return GET("%s/datasets/%s/" % (base_url, folder), format=format)

# Convenience classes.

class APITestCase(TestCase):
    '''
    Common base class providing querying conveniences.
    '''

    def _test_search(self, endpoint, params, success=True):
        '''
        Perform a search, testing the result with self.assertFalse or
        self.assertTrue.
        '''

        info = GET("%s/%s/" % (base_url, endpoint), params=params)

        fn = success and self.assertTrue or self.assertFalse
        fn(info["count"], "Count %s for search involving %s" % (info["count"], params))
        return info

# Test cases.

class DatasetCollectionTest(TestCase):
    '''
    Test collections of datasets/folders.
    '''

    def test_datasets(self):
        '''
        Test for the presence of collections.
        '''

        info = GET("%s/datasets" % base_url)
        self.assertTrue(info["count"], "Count %s for available datasets" % info["count"])

        results = info["results"]
        self.assertTrue(results, "Number of results %s for available datasets" % results)

class DatasetDetailTest(APITestCase):
    '''
    Test the matrix detail information.
    '''

    def test_basic_detail(self):
        '''
        Test the detail information for a particular dataset.
        '''

        datasets = ["GSE41820.mcf7.AHR", "GSE41820.MCF7_Invasive_ductal_breast_carcinoma.AHR"]

        # Find a dataset. The following would work, but changes in the cell
        # line/type representation make direct access to datasets unreliable.

        # detail = get_dataset(dataset)

        # Instead, a search for the dataset occurs.

        info = self._test_search("datasets", {"identifier" : "GSE41820", "tf_name" : "AHR"})
        self.assertTrue(info["count"], "Count %s for available datasets" % info["count"])

        results = info["results"]
        self.assertTrue(results, "Number of results %s for available datasets" % results)

        url = results[0]["url"]
        detail = GET(url)

        self._test_identifier(detail, datasets)

        for key in tfbs_keys:
            self.assertTrue(detail.get(key), "Key %s in detail for %s" % (key, datasets))

    def _test_identifier(self, detail, datasets):
        '''
        Test the reported identifier in the result details.
        '''
        tf_id_lower = detail["tf_id"].lower()
        datasets_lower = [dataset.lower() for dataset in datasets]

        self.assertIn(tf_id_lower, datasets_lower, "Dataset %s, specified %s" % (detail["tf_id"], datasets))

class DatasetSearchTest(APITestCase):
    '''
    Test searching for datasets.
    '''

    def test_dataset_jaspar_id(self):
        '''
        Test a search for a particular JASPAR identifier.
        '''

        jaspar_id = "MA0006.1"

        self._test_search("datasets", {"jaspar_id" : jaspar_id})

    def test_name_search(self):
        '''
        Test a search for transcription factors using a name.
        '''

        self._test_search("datasets", {"tf_name" : "TFAP2A"})
        self._test_search("datasets", {"tf_name" : "BOFS"}, False)
        self._test_search("datasets", {"tf_name" : "testicular feminization"}, False)

    def test_general_search(self):
        '''
        Test a search for transcription factors using a name, matching
        generally, including synonyms.
        '''

        self._test_search("datasets", {"search" : "TFAP2A"})
        self._test_search("datasets", {"search" : "testicular feminization"})

    def test_species_search(self):
        '''
        Test a search for transcription factors using a species name.
        '''

        self._test_search("datasets", {"species" : "Homo sapiens"})

class SpeciesSearchTest(APITestCase):
    '''
    Test searching for species.
    '''

    def test_species(self):
        '''
        Test searches for species.
        '''

        all_species = self._test_search("species", {})
        only_human = self._test_search("species", {"search" : "Homo sapiens"})

        self.assertGreater(all_species["count"], 0, "Expect non-zero number of species")
        self.assertEqual(only_human["count"], 1, "Expect a single human species")

    def test_species_links(self):
        '''
        Test links provided by species records.
        '''

        all_species = self._test_search("species", {})

        for record in all_species["results"]:
            response = requests.get(record["url"])
            self.assertTrue(response.ok, "dataset page status at %s: %s" % (record["url"], response.status_code))
            break



# Some additional help.

help_text = """\
To test a remote site, specify the --url option as follows:

%s --url http://test-unibind.uio.no/api/v1

In order to function correctly, the configuration must allow cookies to be
transmitted over the protocol in use. The following settings must be verified:

 * CSRF_COOKIE_SECURE
 * SESSION_COOKIE_SECURE
""" % (sys.argv[0])



# Main program, calling the unit test runner.

if __name__ == '__main__':
    args = sys.argv

    # Handle command arguments before the test runner sees them.

    if len(args) > 1:

        # Show some additional help.

        if args[1] == "--test-help":
            print(help_text, file=sys.stderr)
            sys.exit(1)

        # Obtain any indicated URL for the Web site.

        elif len(args) > 2 and args[1] == "--url":
            base_url = args[2].rstrip("/")
            del args[1:3]

    main()
