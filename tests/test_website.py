#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from collections import defaultdict
from html.parser import HTMLParser
from unittest import main, TestCase
import requests, urllib.parse, sys

# Program details.

progname = sys.argv[0]

# Web site access definitions.

base_url = "http://localhost:8000"
html_format = "text/html"
json_format = "application/json"

# Utility classes and functions.

class Session:
    '''
    A Web site session abstraction, maintaining a referrer and offering a few
    conveniences.
    '''

    def __init__(self, site_url=None):
        self.site_url = site_url or base_url
        self.session = requests.Session()
        self.referrer = None

    def close(self):
        if self.session:
            self.session.close()
            self.session = None

    def make_url(self, path):
        return "%s%s" % (self.site_url, path)

    def set_referrer(self, headers):
        if self.referrer:
            headers["Referer"] = self.referrer

    def send(self, fn, path, params=None, data=None, format=html_format):
        url = self.make_url(path)
        headers = {"Accept" : format}
        self.set_referrer(headers)
        self.referrer = url
        return fn(url, params=params, headers=headers, data=data)

    def GET(self, path, params=None, format=html_format):
        return self.send(self.session.get, path, params=params, format=format)

    def HEAD(self, path, params=None, format=html_format):
        return self.send(self.session.head, path, params=params, format=format)

    def POST(self, path, params=None, format=html_format):
        return self.send(self.session.post, path, data=params, format=format)

def make_url(path):
    return urllib.parse.urljoin(base_url, path)

def GET(url, params=None, format=html_format):
    headers = format and {"Accept" : format} or {}
    return requests.get(url, params=params, headers=headers)

def HEAD(url, params=None, format=html_format):
    headers = format and {"Accept" : format} or {}
    return requests.head(url, params=params, headers=headers)

def POST(url, params=None, format=html_format):
    headers = format and {"Accept" : format} or {}
    return requests.post(url, data=params, headers=headers)

def get_factor(factor_id, format=html_format):
    url = get_factor_url(factor_id)
    return GET(url, format=format)

def get_factor_url(factor_id):
    return make_url("/factor/%s/" % factor_id)

def get_forms(r):
    p = FormParser()
    if r.ok:
        p.feed(r.text)
    return p

def get_links(r):
    p = LinkParser()
    if r.ok:
        p.feed(r.text)
    return p

def get_filtered_forms(response, action):
    '''
    Get forms having actions with a given URL prefix.
    '''

    parser = get_forms(response)
    l = []

    for form in parser.forms:
        if (form.get_action() or "").startswith(action):
            l.append(form)

    return l

def get_filtered_links(response, path="/static/data", tag="a", params=None):
    '''
    Get local site links from the response.
    '''

    parser = get_links(response)
    l = []

    for links in parser.links.values():
        for link in links:
            if (not path or link.parsed.path.startswith(path)) and \
               (not tag or link.tag == tag) and \
               (not params or set(params).intersection(link.params.keys())):
                l.append(link)

    return l

def get_search_results(r):
    if r.ok:
        p = SearchTableParser()
        p.feed(r.text)
        return p.results
    else:
        return []

def get_lines(s):
    return s.split("\n")

# Dataset discovery using the API.

def get_dataset_id(identifier, tf_name):
    '''
    Return the identifier for the dataset involving the given details.
    '''

    api_url = "%s/api/v1" % base_url
    datasets_url = "%s/datasets/" % api_url

    # Query using the details, obtaining additional summary information
    # providing the folder or dataset identifier.

    response = GET(datasets_url, {
        "identifier" : identifier,
        "tf_name" : tf_name,
        "summary" : "true"},
        json_format)

    info = response.json()
    if not info:
        return None
    results = info["results"]
    if not results:
        return None
    return results[0]["folder"]

# Utility classes.

class CommonParser(HTMLParser):
    '''
    Common parsing routines.
    '''

    def _attrs(self, attrs):
        d = {}
        for name, value in attrs:
            d[name] = value
        return d

class Field:
    '''
    A field representation.
    '''

    def __init__(self, selected=None, choices=None):
        self.selected = selected or []
        self.choices = choices or []

    def add_choice(self, value):
        self.choices.append(value)

    def select_value(self, value):
        self.selected.append(value)

    def select_values(self, values):
        self.selected = values

class Form:
    '''
    A form representation.
    '''

    def __init__(self, attrs):
        self.attrs = attrs
        self.fields = defaultdict(Field)

    def get_action(self):
        return self.attrs.get("action")

    def get_fields(self):
        params = []
        for key, field in self.fields.items():
            for selected in field.selected:
                params.append((key, selected))
        return params

class FormParser(CommonParser):
    '''
    Obtain form field details for the forms in a document.
    '''

    def __init__(self):
        super().__init__()
        self.forms = []
        self.form = None
        self.field_name = None

    def handle_starttag(self, tag, attrs):
        d = self._attrs(attrs)

        # Enter the form.

        if not self.form and tag == "form":
            self.form = Form(d)
            self.forms.append(self.form)

        # Collect different fields.

        elif self.form:
            field_name = None
            select_value = False

            if tag == "select" and "name" in d:
                self.field_name = d["name"]

            elif tag == "input" and "name" in d:
                field_name = d["name"]
                select_value = d.get("type") not in ("checkbox", "radio") or "selected" in d

            elif tag == "option" and self.field_name:
                field_name = self.field_name
                select_value = "selected" in d

            # Process any value element.

            if field_name:
                self.form.fields[field_name].add_choice(d.get("value"))

                # Record selected values.

                if select_value:
                    self.form.fields[field_name].select_value(d.get("value"))

    def handle_endtag(self, tag):
        if not self.form:
            return

        # Leave the form or element.

        if tag == "form":
            self.form = None
        elif tag == "select":
            self.field_name = None

class Link:
    '''
    A link representation.
    '''

    def __init__(self, link, tag="a"):
        self.link = link
        self.tag = tag
        self.parsed = urllib.parse.urlparse(link)
        self.params = urllib.parse.parse_qs(self.parsed.query)

    def __repr__(self):
        return "Link(%r, %r)" % (self.link, self.tag)

    def __str__(self):
        return self.link

    def site_url(self):
        return urllib.parse.urljoin(base_url, self.link)

class LinkParser(CommonParser):
    '''
    Parse HTML documents, obtaining links to other resources.
    '''

    targets = {"a" : "href", "img" : "src"}

    def __init__(self):
        super().__init__()
        self.links = defaultdict(set)
        self.target = None
        self.label = None

    def handle_starttag(self, tag, attrs):
        if tag in ("a", "img"):
            d = self._attrs(attrs)
            target = d.get(self.targets[tag])
            if tag == "img":
                self.links[d.get("alt")].add(Link(target.strip(), tag))
            else:
                self.target = target
                self.label = ""

    def handle_data(self, data):
        if self.target:
            self.label += data

    def handle_endtag(self, tag):
        if tag == "a":
            self.links[self.label.strip()].add(Link(self.target.strip(), tag))
            self.target = None

class SearchTableParser(CommonParser):
    '''
    Parse HTML documents, obtaining search results from a table.
    '''

    def __init__(self):
        super().__init__()
        self.results = []
        self.in_table = False

    def handle_starttag(self, tag, attrs):
        d = self._attrs(attrs)

        # Enter the table.

        if not self.in_table and tag == "table" and d.get("id") == "search_table":
            self.in_table = True

        # Enter a result row.

        elif tag == "tr":
            self.matrix_id = None

        # Obtain a matrix name.

        elif tag == "a" and d.get("href", "").startswith("/factor/"):
            self.matrix_id = d.get("href")[len("/factor/"):]

    def handle_endtag(self, tag):
        if not self.in_table:
            return

        # Leave the table.

        if tag == "table":
            self.in_table = False

        # Leave a result row.

        elif tag == "tr" and self.matrix_id:
            self.results.append(self.matrix_id)

# Common test functionality.

class WebTestCase(TestCase):
    '''
    Provide some common test functionality.
    '''

    def _test_page(self, path, name, content=False):
        url = make_url(path)
        method = content and GET or HEAD
        response = method(url)
        self._test_page_result(response, "%s page status at %s: %s" % (name, path, response.status_code))

    def _test_page_result(self, response, message):
        self.assertTrue(response.ok, message)

# Test cases.

class PagesTest(WebTestCase):
    '''
    Test the different pages.
    '''

    def test_principal_pages(self):
        '''
        Test the principal pages.
        '''

        self._test_page("/", "Main")
        self._test_page("/about", "About")
        self._test_page("/api", "API")
        self._test_page("/changelog", "Changelog")
        self._test_page("/docs", "Documentation")
        self._test_page("/downloads", "Downloads")
        self._test_page("/enrichment", "Enrichment analysis")
        self._test_page("/genome-tracks", "Genome tracks")
        self._test_page("/search", "Search")

class DownloadsTest(WebTestCase):
    '''
    Test download links.
    '''

    def test_download_links(self):
        '''
        Obtain the downloads page and test each of the links.
        '''

        url = make_url("/downloads")
        response = GET(url)
        self._test_page_result(response, "Downloads page status: %s" % response.status_code)

        links = get_filtered_links(response)
        self.assertTrue(links, "Downloads page links")

        for link in links:
            response = HEAD(link.site_url())
            self._test_page_result(response, "Download at %s: %s" % (link, response.status_code))

class FactorDetailTest(WebTestCase):
    '''
    Test transcription factor detail pages.
    '''

    factor_details = [
        ("ENCSR000EVB", "ELK4"),
        ("ENCSR000AHD", "CTCF"),
        ]

    def _get_factor_ids(self):
        '''
        Return the dataset identifiers for the above details.
        '''

        l = []
        for identifier, tf_name in self.factor_details:
            folder = get_dataset_id(identifier, tf_name)
            self.assertTrue(folder, "Dataset identifier for source identifier %s and name %s" % (identifier, tf_name))
            l.append(folder)
        return l

    def test_factor_detail_images(self):
        '''
        Test the availability of images in transcription factor pages.
        '''

        for factor_id in self._get_factor_ids():
            self._test_factor_detail_images(factor_id)

    def _test_factor_detail_images(self, factor_id):
        '''
        Test the availability of images in a transcription factor page.
        '''

        response = self._test_factor_detail(factor_id)

        # Obtain image links from the page.

        links = get_filtered_links(response, tag="img")
        self.assertTrue(links, "Detail page images for %s" % factor_id)

        # Check each image. Some will be navigational, whereas resource-specific
        # images will contain the factor identifier in the URL.

        for link in links:
            if factor_id in link.parsed.path:
                response = GET(link.site_url())
                self._test_page_result(response, "Detail page image for %s at %s: %s" % (factor_id, link, response.status_code))

    def test_factor_detail_downloads(self):
        '''
        Test the availability of downloads in transcription factor pages.
        '''

        for factor_id in self._get_factor_ids():
            self._test_factor_detail_downloads(factor_id)

    def _test_factor_detail_downloads(self, factor_id):
        '''
        Test the availability of downloads in a transcription factor page.
        '''

        response = self._test_factor_detail(factor_id)

        # Obtain image links from the page.

        links = get_filtered_links(response)
        self.assertTrue(links, "Detail page links for %s" % factor_id)

        for link in links:
            response = GET(link.site_url())
            self._test_page_result(response, "Detail page download for %s at %s: %s" % (factor_id, link, response.status_code))

    def test_factor_detail_trained_models(self):
        '''
        Test the availability of trained models in transcription factor pages.
        '''

        map(self._test_factor_detail_trained_models, self._get_factor_ids())

    def _test_factor_detail_trained_models(self, factor_id):
        '''
        Test the availability of trained models in a transcription factor page.
        '''

        response = self._test_factor_detail(factor_id)

        # Find "mtrain" action links.

        links = get_filtered_links(response, "/factor", params=["mtrain"])
        self.assertTrue(links, "Detail page links for %s" % factor_id)

        for link in links:
            response = GET(link.site_url())
            self._test_page_result(response, "Detail page download for %s at %s: %s" % (factor_id, link, response.status_code))

    def _test_factor_detail(self, factor_id):
        '''
        Test the availability of a transcription factor page.
        '''

        response = get_factor(factor_id)
        self._test_page_result(response, "Detail page status for %s: %s" % (factor_id, response.status_code))
        return response

class FactorSearchTest(WebTestCase):
    '''
    Test transcription factor searching.
    '''

    def test_factor_search(self):
        '''
        Test searching for transcription factors.
        '''

        self._test_search("MA0492.1")

    def test_name_search(self):
        '''
        Test searching for transcription factors by name.
        '''

        self._test_search("TFAP2A")

    def test_synonym_search(self):
        '''
        Test searching for transcription factors by synonym.
        '''

        self._test_search("BOFS")
        self._test_search("testicular feminization")

    def test_search_table_downloads(self):
        '''
        Test searching and table downloads from the search results page.
        '''

        self._test_search_download_links("MA0465.2")
        self._test_search_download_links("CDX2")

    def test_search_form_fields(self):
        '''
        Test the availability of form fields, specifically those providing cell
        line choices.
        '''

        # Indicate a species to get access to more search options.

        query = {"species" : "9606"}
        response = GET(make_url("/search"), query)
        self._test_page_result(response, "Search page status for %s: %s" % (query, response.status_code))

        # Obtain the search form.

        form = self._get_form(response, "quick_search")
        self.assertTrue(form, "Search form availability given query %s" % query)

        # Check for cell line fields.

        self.assertTrue(form.fields["cell_line_id"].choices, "Cell line field choices in search form")

    def test_search_table_pagination(self):
        '''
        Test searching and the pagination support.
        '''

        session = Session()
        try:
            # Perform an initial search to get access to more search options.

            query = {"q" : "", "species" : "9606"}
            response = session.GET("/search", query)
            self._test_page_result(response, "Search page status for %s: %s" % (query, response.status_code))

            # Check for results.

            results = get_search_results(response)
            self.assertTrue(results, "Search results for query %s" % query)

            # Obtain the search form and check for cell line fields.

            form = self._get_form(response, "quick_search")
            self.assertTrue(form, "Search form availability given query %s" % query)
            self.assertTrue(form.fields["cell_line_id"].choices, "Cell line field choices in search form")

            # Select all cell lines and resubmit.

            all_cell_lines = form.fields["cell_line_id"].choices
            form.fields["cell_line_id"].select_values(all_cell_lines)

            response = session.POST("/search", form.get_fields())
            self._test_page_result(response, "Search page status for %s: %s" % (query, response.status_code))

            # Check for results again.

            results_all = get_search_results(response)
            self.assertTrue(results_all, "Search results for updated query %s" % query)

            # Obtain the pagination form.

            form = self._get_form(response, "page_size_form")
            self.assertTrue(form, "Pagination form availability given updated query %s" % query)

            # Select 25 results per page and resubmit.

            form.fields["page_size"].select_values(["25"])

            response = session.POST("/search", form.get_fields())
            self._test_page_result(response, "Search page status for %s (with pagination): %s" % (query, response.status_code))

            # Check for results again.

            results_paged = get_search_results(response)
            self.assertEqual(len(results_paged), 25, "Search results for updated query %s (with pagination)" % query)

        finally:
            session.close()

    # Helper methods.

    def _get_form(self, response, identifier):
        '''
        Obtain the form with the given identifier.
        '''

        forms = get_forms(response).forms

        for form in forms:
            if form.attrs.get("id") == identifier:
                return form

        return None

    def _test_search(self, query, success=True):
        '''
        Perform a search and test the response.
        '''

        url = make_url("/search")
        response = GET(url, params={"q" : query})
        self._test_page_result(response, "Search page status for %s: %s" % (query, response.status_code))
        results = get_search_results(response)

        fn = success and self.assertTrue or self.assertFalse
        fn(results, "Results for search %s: %s" % (query, results))

    def _test_search_download_links(self, query):
        '''
        '''

        url = make_url("/search")
        response = GET(url, params={"q" : query, "collection" : "Permissive"})
        self._test_page_result(response, "Search page status for %s: %s" % (query, response.status_code))

        forms = get_filtered_forms(response, "/api/v1/datasets")
        self.assertTrue(forms, "Search result page links for %s" % query)

        # Obtain tabular data downloads.

        for form in forms:
            if "format" in form.fields and "tsv" in form.fields["format"].selected:
                response = POST(form.get_action(), form.get_fields(), format="text/tab-separated-values")
                self._test_page_result(response,
                                       "Search result download for %s at %s: %s" %
                                       (query, form.get_action(), response.status_code))

                lines = get_lines(response.text)
                headers = lines and lines[0].split("\t") or []
                self.assertTrue(headers and "synonyms" in headers,
                                "Require synonyms in headers for %s, download %s: %s" %
                                (query, form.get_action(), headers))



# Some additional help.

example_hostname = "test-unibind.uio.no"

help_text = """\
To test a remote site, specify the --url option as follows:

%s --url http://%s

In order to function correctly, the configuration must allow cookies to be
transmitted over the protocol in use. The following settings must be verified:

 * CSRF_COOKIE_SECURE
 * SESSION_COOKIE_SECURE

It is easier to use a secure site if this has been deployed. For example:

%s --url https://%s
""" % (progname, example_hostname, progname, example_hostname)



# Main program, calling the unit test runner.

if __name__ == '__main__':
    args = sys.argv

    # Handle command arguments before the test runner sees them.

    if len(args) > 1:

        # Show some additional help.

        if args[1] == "--test-help":
            print(help_text, file=sys.stderr)
            sys.exit(1)

        # Obtain any indicated URL for the Web site.

        elif len(args) > 2 and args[1] == "--url":
            base_url = args[2].rstrip("/")
            del args[1:3]

    main()
