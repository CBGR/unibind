# SPDX-FileCopyrightText: 2020-2021 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.http.request import QueryDict
from os import listdir
from os.path import commonprefix, isdir, join
import tarfile

# Utility class for constructing archives using the tarfile module.

class Archiver(object):
    '''
    Create an archive from a directory, excluding files if appropriate.
    '''

    def __init__(self, output_path, base, excluded=None):
        '''
        Initialise an archive with an output path, base directory, and
        an optional sequence of excluded file extensions.
        '''

        self.output_path = output_path
        self.base = base
        self.excluded = excluded

    def filter(self, tarinfo):
        '''
        Given the tarinfo for a file, return the modified tarinfo
        indicating the archive filename, or return None if the file is
        to be excluded.
        '''

        base = join(self.base, "").lstrip("/")

        # Remove the base directory from each filename to get the
        # path within the directory.

        if commonprefix([tarinfo.name, base]) == base:
            filename = tarinfo.name[len(base):]
        else:
            filename = tarinfo.name

        # Exclude certain files.

        if self.excluded:
            for suffix in self.excluded:
                if filename.endswith(suffix):
                    return None

        tarinfo.name = filename
        return tarinfo

    def archive(self, name=None):
        '''
        Make the archive from the base directory or from a specific
        object, having 'name', within the directory.
        '''

        payload = name and join(self.base, name) or self.base

        # Add the contents of the given payload to a compressed archive
        # at the indicated location.

        tf = tarfile.open(self.output_path, "w:gz")

        try:
            # Add files within a directory manually without any
            # selected name because an attempt is otherwise made to
            # use "/" as the directory name within the archive.

            if isdir(payload) and not name:
                for filename in listdir(payload):
                    tf.add(join(payload, filename), filter=self.filter)
            else:
                tf.add(payload, filter=self.filter)
        finally:
            tf.close()

# Common utilities.

def get_data_source(request):
    '''
    Choose the source of parameter data from the request method.
    '''

    if request:
        if request.method == 'GET':
            return request.GET
        elif request.method == 'POST':
            return request.POST

    # Return an empty dictionary without any request or appropriate payload.

    return QueryDict(mutable=True)

# vim: tabstop=4 expandtab shiftwidth=4
