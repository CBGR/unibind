# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.core.mail import BadHeaderError, EmailMessage
import smtplib

def send_email_notification(recipient, job_status, job_url):
    '''
    Send an e-mail notification.
    '''

    if not recipient:
        return True

    # The sender address may be subject to restrictions and should be updated
    # with caution.

    from_email = 'unibindmail@uio.no'
    reply_to = 'no-reply@unibind.uio.no'
    subject = 'UniBind Job %s' % job_status
    from_name = "UniBind"
    message = "Thanks for using <b>UniBind</b>!<br><br>"

    if job_status == 'Completed':
        message += """\
Your results are ready to view and download using this link:<br>
<br>
%s""" % job_url

    elif job_status == 'Running':
        message += """\
Your job is submitted and results will be shortly available to view and download
using this link:<br>
<br>
%s""" % job_url

    elif job_status == 'Stopped':
        message += """\
Your job was stopped due to an error. Please make sure your input file is in BED format.
For more information check the error log:<br>
<br>
%s""" % job_url

    message += "<br><br>Best regards,<br>UniBind Team<br>https://unibind.uio.no"

    email = EmailMessage(
        subject,
        message,
        from_email,
        [recipient],
        reply_to=[reply_to],
        )
    try:
        email.content_subtype = "html"
        email.send()
        return True
    except (BadHeaderError, smtplib.SMTPException):
        return False

# vim: tabstop=4 expandtab shiftwidth=4
