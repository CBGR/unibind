# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
#from django.contrib import admin
from django.contrib.sitemaps.views import sitemap
from django.template.loader import get_template

from rest_framework.documentation import include_docs_urls

from .sitemaps import StaticViewSitemap

# Documentation view attributes.
# The documentation application must be defined at the top level.

# Obtain API documentation strings from templates.
# The REST framework templates should really permit customisation using
# templates, but instead they access attributes on a document object that need
# to be set here.

API_TITLE = 'UniBind API'
API_DESCRIPTION = get_template("restapi/description.html").render()

# Resource definitions.

docs = include_docs_urls(title=API_TITLE, description=API_DESCRIPTION)
sitemaps = {'sitemaps': {'static': StaticViewSitemap}}

#admin.site.site_header = 'UniBind Admin'

# Application details and structure.

urlpatterns = [
    url(r'^', include('portal.urls')),
    #url(r'^admin/', admin.site.urls),
    url(r'^api/', include('restapi.v1.urls', namespace='v1')),
    url(r'^api/v1/docs/', docs),
    url(r'^api/auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    url(r'^sitemap\.xml$', sitemap, sitemaps,
                           name='django.contrib.sitemaps.views.sitemap'),
    ]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [url(r'^__debug__/', include(debug_toolbar.urls))] + urlpatterns
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
