# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2017-2020 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

"""
Django settings for unibind project.

For more information on this file, see
https://docs.djangoproject.com/en/2.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.2/ref/settings/
"""

from django.core.management.utils import get_random_secret_key
from os.path import abspath, dirname, exists, join
import json, os

# Application administrators.

ADMINS = [('Paul Boddie', 'paul.boddie@ncmm.uio.no'),]

# Version information. This should be updated when new data versions are
# produced so that sites providing older versions can link to the latest site.

LATEST_DATA_VERSION = '2021'

# The base directory should be the root of this software distribution.

BASE_DIR = dirname(dirname(abspath(__file__)))

# Miscellaneous values.

_HOUR_IN_SECONDS = 60 * 60

# Production versus test configuration.
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# A production configuration is enabled by having a dedicated secret file
# present in the filesystem which should be initialised when deploying the
# software.

_SECRET_FILE = join(BASE_DIR, "secret.txt")
_PRODUCTION = exists(_SECRET_FILE)

# Without the secret file, a random secret is generated which will most likely
# expire sessions if the server is restarted.

if not _PRODUCTION:
    SECRET_KEY = get_random_secret_key()
    SESSION_COOKIE_DOMAIN = 'localhost'
else:
    SECRET_KEY = open(_SECRET_FILE).read().strip()

# Debug is disabled in production. Since the testing server is not secure, CSRF
# cookies are also not secure when testing; otherwise, they must be served over
# secure HTTP connections. This is configured in the Web server environment.

DEBUG = not _PRODUCTION
CSRF_COOKIE_SECURE = _PRODUCTION
SESSION_COOKIE_SECURE = _PRODUCTION

SECURE_BROWSER_XSS_FILTER = True
SECURE_CONTENT_TYPE_NOSNIFF = True
X_FRAME_OPTIONS = 'ALLOW'

ALLOWED_HOSTS = [
    '*'
    #'localhost',
    #'127.0.0.1',
    #'http://unibind.uio.no'
    ]

#INTERNAL_IPS = ('127.0.0.1',)

# Application definition

INSTALLED_APPS = [
    'bootstrap_admin',
    #'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'portal.apps.PortalConfig',
    'debug_toolbar',
    'rest_framework',
    'django_filters',
    'bootstrapform',
    'restapi.v1',
    #'rest_framework_docs',
    #'compressor',
    'rest_framework_swagger',
    'corsheaders',
    ]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    ]

ROOT_URLCONF = 'unibind.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'DIRS': [BASE_DIR, join(BASE_DIR, 'templates')],
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                ],
            },
        },
    ]

BOOTSTRAP_ADMIN_SIDEBAR_MENU = True

WSGI_APPLICATION = 'unibind.wsgi.application'

# Database connection details.
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'UniBind20220909.sqlite3'),
        ##For MySQL we only need to add name string. For example
        #'ENGINE': 'django.db.backends.mysql',
        #'NAME': 'unibind',
        ##If you are using MySQL engine, please fill the follwoing details. sqlite doesn't need these
        #'USER': 'root',
        #'PASSWORD': 'root',
        #'HOST': '/Applications/MAMP/tmp/mysql/mysql.sock',
        #'PORT': '3306',
        }
    }

# Password validation.
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# SMTP settings configuration.
# This is provided as a JSON format file with various keys employed below.

_SMTP_SETTINGS_FILE = join(BASE_DIR, "smtp_settings.txt")

_SMTP_SETTINGS = exists(_SMTP_SETTINGS_FILE) and json.load(open(_SMTP_SETTINGS_FILE)) or {}
_get = _SMTP_SETTINGS.get

if _SMTP_SETTINGS:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST          = _get("host")
    EMAIL_HOST_USER     = _get("user")
    EMAIL_HOST_PASSWORD = _get("password")
    EMAIL_PORT          = _get("port", 25)
    EMAIL_USE_SSL       = _get("ssl", False)
else:
    EMAIL_BACKEND = 'django.core.mail.backends.dummy.EmailBackend'

SEND_TO_EMAIL = ['anthony.mathelier@ncmm.uio.no']

# Internationalization.
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'

USE_I18N = True
USE_L10N = True
USE_TZ = True

# Limits.

DATA_UPLOAD_MAX_NUMBER_FIELDS = 2000
SEARCH_MAX_CELL_LINES = 100

# Results pagination.

PAGINATION_DEFAULT = 10
MAX_PAGINATION_LIMIT = 250

# Static files (CSS, JavaScript, images).

STATIC_ROOT = join(BASE_DIR, 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Temporary file location and lifespan (in days).

TEMP_DIR = join(BASE_DIR, "temp")
TEMP_LIFE = 10

DOWNLOAD_DIR = join(BASE_DIR, "download")

# Absolute path for enrichment tools.

ENRICHMENT_DIR = join(dirname(BASE_DIR), "unibind_enrichment")
TFBS_EXTRACTION_DIR = join(dirname(BASE_DIR), "unibind_tfbs_extraction")

STATICFILES_DIRS = [
    join(BASE_DIR, "portal", "static"),
    join(BASE_DIR, "restapi", "static"),
    ]

# STATICFILES_FINDERS = (
#     'django.contrib.staticfiles.finders.FileSystemFinder',
#     'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#     # other finders..
#     'compressor.finders.CompressorFinder',
# )

# Django REST Framework settings.

REST_FRAMEWORK = {
    #'UNICODE_JSON': False

    # Read only access.
    'DEFAULT_PERMISSION_CLASSES': [
        #'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
        ],

    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning',

    'DEFAULT_MODEL_SERIALIZER_CLASS':
        'rest_framework.serializers.HyperlinkedModelSerializer',

    'ORDERING_PARAM': 'order',

    # Results pagination.
    'PAGINATE_BY': 10,
    'PAGINATE_BY_PARAM': 'page_size',
    'MAX_PAGINATE_BY': 1000,

    'DEFAULT_PARSER_CLASSES': (
        'rest_framework_yaml.parsers.YAMLParser',
        ),

    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework_jsonp.renderers.JSONPRenderer',
        'rest_framework_yaml.renderers.YAMLRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer',
        ),

    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',

    'DEFAULT_THROTTLE_CLASSES': (
        'rest_framework.throttling.AnonRateThrottle',
        'rest_framework.throttling.UserRateThrottle'
        ),

    'DEFAULT_THROTTLE_RATES': {
        'anon': '25/second',
        'user': '100/second'
        },

    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',)
    }

SWAGGER_SETTINGS = {
    'OPERATIONS_SORTER': 'alpha',
    'APIS_SORTER': 'alpha',
    'SHOW_REQUEST_HEADERS': True,
    'VALIDATOR_URL': None,
    'SUPPORTED_SUBMIT_METHODS': ['get'],
    'DOC_EXPANSION': 'list',
    'SECURITY_DEFINITIONS': {},
    }

# CORS settings.

#CORS_ALLOW_METHODS = ('GET','OPTIONS',)
CORS_URLS_REGEX = r'^/api/v1/.*$'
#CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
    'https://google.com',
    )

# Django - Memcached.

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
        #'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        #'LOCATION': '127.0.0.1:11211',
        }
    }

# Calls to QuerySet.count() can be cached, configured here.

CACHE_COUNT_TIMEOUT = 24 * _HOUR_IN_SECONDS

# Empty querysets.
# By default cache machine will not cache empty querysets. To cache them set this to True.

CACHE_EMPTY_QUERYSETS = True

# Logging.
# See: https://docs.djangoproject.com/en/3.0/topics/logging/#configuring-logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': 'ERROR',
        },
    },
}
