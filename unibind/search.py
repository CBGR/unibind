# SPDX-FileCopyrightText: 2017-2021 University of Oslo
# SPDX-FileContributor: Aziz Khan <azez.khan__AT__gmail.com>
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

import coreapi
import coreschema

from django.db.models import Q
from portal.models import Cell, Collection, Factor, FactorCollection, \
                          FactorData, FactorRef, FactorSample, Profile, \
                          RefSource, Species, Synonym
from unibind.settings import SEARCH_MAX_CELL_LINES
import re

# Search metadata. Since this needs to be consistent with the search operation,
# it is maintained here.

def get_schema_fields():
    '''
    Return filtering metadata for the API.
    '''

    return [
        coreapi.Field(
            name='biological_condition',
            location='query',
            schema=coreschema.String(
                description='Biological condition or source (e.g. dht).',
                title='Biological condition',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='cell_line',
            location='query',
            schema=coreschema.String(
                description='Cell line or tissue name. For example: MCF7',
                title='Cell line',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='collection',
            location='query',
            schema=coreschema.String(
                description='Collection membership of the data, such as Permissive or Robust.',
                title='Collection',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='data_source',
            location='query',
            schema=coreschema.String(
                description='Source of the data, such as ENCODE, GEO, AE(Array Express)',
                title='Data source',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='has_pvalue',
            location='query',
            schema=coreschema.Boolean(
                description='Select datasets meeting a p-value threshold.',
                title='Has significant centrality',
                ),
            required=False,
            type='boolean'),
        coreapi.Field(
            name='identifier',
            location='query',
            schema=coreschema.String(
                description='The dataset identifier, such ENCODE or GEO dataset ID. For example: GSE60130',
                title='Identifier',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='jaspar_id',
            location='query',
            schema=coreschema.String(
                description='JASPAR database profile matrix ID. For example: MA0492.1',
                title='JASPAR ID',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='model',
            location='query',
            schema=coreschema.String(
                description='Prediction model involved in the production of datasets.',
                title='Prediction model',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='species',
            location='query',
            schema=coreschema.String(
                description='Species name associated with a TF',
                title='Species',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='summary',
            location='query',
            schema=coreschema.String(
                description='Obtain summary of TF and related data',
                title='Summary',
                ),
            required=False,
            type='boolean'),
        coreapi.Field(
            name='tf_name',
            location='query',
            schema=coreschema.String(
                description='Search by TF name (case-insensitive). For example: SMAD3',
                title='TF Name',
                ),
            required=False,
            type='string'),
        coreapi.Field(
            name='threshold_pvalue',
            location='query',
            schema=coreschema.Number(
                description='Threshold applying to p-value filtering: log(p-value); maximum 0.',
                title='p-value Threshold',
                maximum=0,
                ),
            required=False,
            type='number'),
        ]

# Search functionality.

def get_search_queryset(source, queryset=None):
    '''
    Return a queryset for a search involving the given data source.
    Filter any given queryset.
    '''

    get = source.get

    # Filter out empty values in lists of parameters.

    getlist = lambda name: list(filter(None, source.getlist(name)))

    biological_condition = get('biological_condition')
    cell_line = getlist('cell_line')
    collection = get('collection')
    has_pvalue = get('has_pvalue')
    threshold_pvalue = get('threshold_pvalue', 0)
    identifier = get('identifier', None)
    jaspar_id = get('jaspar_id', None)
    model = get('model')
    query_string = get('q') or get('search')
    ref_db = get('source') or get('data_source')
    species = get('species')
    tf_name = get('tf_name')

    # Support for cell line selection in the regular search interface.

    cell_line_ids = getlist('cell_line_id')

    # Construct a query set for the results.

    if queryset is None:
        queryset = Factor.objects.all().order_by('tf_name')

    # Filter based on query_string.

    query_string_result = queryset.none()

    if query_string:
        for term in _get_query_terms(query_string):

            # Obtain the base identifier from any matrix term.

            term = _get_matrix_term(term)

            # Attempt to match the query string to various fields, looking
            # for a case-insensitive substring match in each one.

            field, value = term

            if field in (None, 'tf'):
                name_factors = queryset.filter(tf_name__icontains=value)
            else:
                name_factors = queryset.none()

            # Obtain biological conditions and cell lines matching the query string.

            sample_factors = queryset.none()

            if field in (None, 'condition'):
                sample_factors |= queryset.filter(folder__in=
                        FactorSample.objects.filter(
                            biological_condition__icontains=value
                            ).values("folder"))

            if field in (None, 'title'):
                sample_factors |= queryset.filter(folder__in=
                        FactorSample.objects.filter(
                            cell__in=Cell.objects.filter(title__icontains=value)
                            ).values("folder"))

            # Obtain database references matching the query string.

            referenced_factors = queryset.none()

            if field in (None, 'id', 'ref'):
                referenced_factors |= queryset.filter(folder__in=
                        FactorRef.objects.filter(
                            ref_id__icontains=value
                            ).values("folder"))

            if field in (None, 'db', 'source'):
                referenced_factors |= queryset.filter(folder__in=
                        FactorRef.objects.filter(ref_db__in=
                            RefSource.objects.filter(
                                source_name__icontains=value
                                )).values("folder"))

            # Obtain profiles matching the query string.

            if field in (None, 'jaspar'):
                profile_factors = queryset.filter(folder__in=
                        FactorData.objects.filter(
                            Q(jaspar_id__icontains=value)
                            ).values("folder"))
            else:
                profile_factors = queryset.none()

            # Obtain synonyms matching the query string.

            if field in (None, 'synonym'):
                synonym_factors = queryset.filter(folder__in=
                        FactorData.objects.filter(profile__in=
                            Profile.objects.filter(
                                Q(genes__in=Synonym.objects.filter(synonym__icontains=value).values_list("gene_id")) |
                                Q(name__icontains=value)))
                        .values("folder"))
            else:
                synonym_factors = queryset.none()

            query_string_result |= name_factors | sample_factors | referenced_factors | profile_factors | synonym_factors

        queryset = query_string_result

    # Filter based on various criteria.

    if biological_condition:
        queryset = queryset.filter(folder__in=
                    FactorSample.objects.filter(biological_condition__iexact=biological_condition)
                    .values("folder"))

    # Searching using cell line identifiers.

    if cell_line_ids and len(cell_line_ids) <= SEARCH_MAX_CELL_LINES:
        q = Q(cell__in=cell_line_ids)
        queryset = queryset.filter(folder__in=
                                   FactorSample.objects.filter(q)
                                   .values("folder"))

    # Searching using cell line titles.

    if cell_line:
        q = Q(cell__in=Cell.objects.filter(title__in=cell_line))
        queryset = queryset.filter(folder__in=
                                   FactorSample.objects.filter(q)
                                   .values("folder"))

    # Centrality filtering.
    # Coerce the threshold to a number.

    if has_pvalue and has_pvalue.lower() != 'false':
        try:
            threshold_pvalue = float(threshold_pvalue)
        except ValueError:
            threshold_pvalue = 0

        folders = queryset.values_list('folder', flat=True).distinct()
        folders = FactorData.objects.values_list('folder', flat=True) \
                                    .filter(folder__in=folders) \
                                    .exclude(adj_centrimo_pvalue__gte=threshold_pvalue) \
                                    .distinct()
        queryset = queryset.filter(folder__in=folders)

    # External database identifier filtering.

    if identifier:
        queryset = queryset.filter(folder__in=
                                   FactorRef.objects.filter(ref_id__iexact=identifier)
                                   .values("folder"))

    # JASPAR identifier (excluding version) filtering.

    if jaspar_id:
        jaspar_id = str(jaspar_id).split('.')[0]
        queryset = queryset.filter(folder__in=
                                   FactorData.objects.filter(jaspar_id__iexact=jaspar_id)
                                   .values("folder"))

    # Prediction model filtering.

    if model:
        queryset = queryset.filter(folder__in=
                                   FactorData.objects.filter(prediction_model=model)
                                   .values("folder"))

    # External database filtering.

    if ref_db:
        queryset = queryset.filter(folder__in=
                                   FactorRef.objects.filter(ref_db=ref_db)
                                   .values("folder"))

    # Species filtering.
    # Permit taxonomy identifiers and names.

    if species:
        if species.isdigit():
           q = Q(species=species)
        else:
           q = Q(species__in=Species.objects.filter(name__iexact=species))
        queryset = queryset.filter(q)

    # Exact, case-insensitive name filtering.

    if tf_name:
        queryset = queryset.filter(tf_name__iexact=tf_name)

    # Collection filtering.

    if collection:
        queryset = queryset.filter(folder__in=
                       FactorCollection.objects.filter(collection__in=
                           Collection.objects.filter(name=collection)).values('folder'))

    return queryset

# Search query processing.

def _get_matrix_term(term):
    '''
    Obtain the base identifier from any matrix identifier of the
    form "<base>.<version>", where <base> is of the form "MA<num>".
    '''

    field, value = term

    if field not in (None, 'tf'):
        return term

    t = value.split('.', 1)
    if len(t) == 2 and t[0][0:2] == 'MA':
        return field, t[0]
    else:
        return term

def _get_query_terms(query_string):
    '''
    Return a list of (field, value) terms.
    '''

    pattern = re.compile("(?<!:):(?!:)")
    terms = []

    for token in _get_query_tokens(query_string):
        t = pattern.split(token)
        field = len(t) > 1 and t[0] or None
        value = len(t) < 2 and t[0] or ":".join(t[1:])
        terms.append((field, value))

    return terms

def _get_query_tokens(query_string):
    '''
    Return a list of tokens, with quotation marks being used to group
    characters into single tokens.
    '''

    pattern = re.compile('"(.*?)"')
    unquoted = True
    tokens = []

    # Split the query using quoted blocks as separators. These blocks will
    # be single tokens, with the surrounding text being split on whitespace
    # to produce individual tokens.

    for value in pattern.split(query_string):
        if unquoted:
            tokens += value.split()
        else:
            tokens.append(value)
        unquoted = not unquoted

    return tokens

# vim: tabstop=4 expandtab shiftwidth=4
